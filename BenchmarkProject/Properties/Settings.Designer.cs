﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BenchmarkProject.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Benchmarks")]
        public string InitialDir {
            get {
                return ((string)(this["InitialDir"]));
            }
            set {
                this["InitialDir"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public decimal InitialTime {
            get {
                return ((decimal)(this["InitialTime"]));
            }
            set {
                this["InitialTime"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool DrawEveryImage {
            get {
                return ((bool)(this["DrawEveryImage"]));
            }
            set {
                this["DrawEveryImage"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("3")]
        public int DOD {
            get {
                return ((int)(this["DOD"]));
            }
            set {
                this["DOD"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0.5")]
        public double NeighbourhoodProb1 {
            get {
                return ((double)(this["NeighbourhoodProb1"]));
            }
            set {
                this["NeighbourhoodProb1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0.5")]
        public double NeighbourhoodProb2 {
            get {
                return ((double)(this["NeighbourhoodProb2"]));
            }
            set {
                this["NeighbourhoodProb2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public double InventorsRatio1 {
            get {
                return ((double)(this["InventorsRatio1"]));
            }
            set {
                this["InventorsRatio1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public double InventorsRatio2 {
            get {
                return ((double)(this["InventorsRatio2"]));
            }
            set {
                this["InventorsRatio2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool TraceDetailedInfo {
            get {
                return ((bool)(this["TraceDetailedInfo"]));
            }
            set {
                this["TraceDetailedInfo"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public decimal InitialCapacityPart {
            get {
                return ((decimal)(this["InitialCapacityPart"]));
            }
            set {
                this["InitialCapacityPart"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public decimal CapacitySlope {
            get {
                return ((decimal)(this["CapacitySlope"]));
            }
            set {
                this["CapacitySlope"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public double DistanceWeightPower {
            get {
                return ((double)(this["DistanceWeightPower"]));
            }
            set {
                this["DistanceWeightPower"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool UsePreviousSolution {
            get {
                return ((bool)(this["UsePreviousSolution"]));
            }
            set {
                this["UsePreviousSolution"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool Use2Opt {
            get {
                return ((bool)(this["Use2Opt"]));
            }
            set {
                this["Use2Opt"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool AdditionalTreeOptimizer {
            get {
                return ((bool)(this["AdditionalTreeOptimizer"]));
            }
            set {
                this["AdditionalTreeOptimizer"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public int BetweenResampleMonteCarloIterations {
            get {
                return ((int)(this["BetweenResampleMonteCarloIterations"]));
            }
            set {
                this["BetweenResampleMonteCarloIterations"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int TotalMonteCarloIterations {
            get {
                return ((int)(this["TotalMonteCarloIterations"]));
            }
            set {
                this["TotalMonteCarloIterations"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool ComputeOnlyWhenChange {
            get {
                return ((bool)(this["ComputeOnlyWhenChange"]));
            }
            set {
                this["ComputeOnlyWhenChange"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0.0")]
        public decimal InitialMonteCaroloPartOfTheDay {
            get {
                return ((decimal)(this["InitialMonteCaroloPartOfTheDay"]));
            }
            set {
                this["InitialMonteCaroloPartOfTheDay"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("4")]
        public int ReservoirVehiclesCount {
            get {
                return ((int)(this["ReservoirVehiclesCount"]));
            }
            set {
                this["ReservoirVehiclesCount"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool GenerateDataFromModel {
            get {
                return ((bool)(this["GenerateDataFromModel"]));
            }
            set {
                this["GenerateDataFromModel"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool UseUCB {
            get {
                return ((bool)(this["UseUCB"]));
            }
            set {
                this["UseUCB"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool RememberBandits {
            get {
                return ((bool)(this["RememberBandits"]));
            }
            set {
                this["RememberBandits"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SeparateImagesForRoutes {
            get {
                return ((bool)(this["SeparateImagesForRoutes"]));
            }
            set {
                this["SeparateImagesForRoutes"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool UseGreedyInsteadOfRandom {
            get {
                return ((bool)(this["UseGreedyInsteadOfRandom"]));
            }
            set {
                this["UseGreedyInsteadOfRandom"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool UseProcessesNotThreads {
            get {
                return ((bool)(this["UseProcessesNotThreads"]));
            }
            set {
                this["UseProcessesNotThreads"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public decimal BufferSize {
            get {
                return ((decimal)(this["BufferSize"]));
            }
            set {
                this["BufferSize"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool FullMulticlustering {
            get {
                return ((bool)(this["FullMulticlustering"]));
            }
            set {
                this["FullMulticlustering"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool RandomOrderOfInstances {
            get {
                return ((bool)(this["RandomOrderOfInstances"]));
            }
            set {
                this["RandomOrderOfInstances"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0.025")]
        public decimal TimeStep {
            get {
                return ((decimal)(this["TimeStep"]));
            }
            set {
                this["TimeStep"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int RouteSwarmIterations {
            get {
                return ((int)(this["RouteSwarmIterations"]));
            }
            set {
                this["RouteSwarmIterations"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("22")]
        public int SwarmSize {
            get {
                return ((int)(this["SwarmSize"]));
            }
            set {
                this["SwarmSize"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public int ThreadCount {
            get {
                return ((int)(this["ThreadCount"]));
            }
            set {
                this["ThreadCount"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("8")]
        public int ParallelInstances {
            get {
                return ((int)(this["ParallelInstances"]));
            }
            set {
                this["ParallelInstances"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SendEmail {
            get {
                return ((bool)(this["SendEmail"]));
            }
            set {
                this["SendEmail"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("22")]
        public int MaxPSOSwarmNeighbourhoodSize {
            get {
                return ((int)(this["MaxPSOSwarmNeighbourhoodSize"]));
            }
            set {
                this["MaxPSOSwarmNeighbourhoodSize"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool RetrievePreviousSolutionThroughDiscretization {
            get {
                return ((bool)(this["RetrievePreviousSolutionThroughDiscretization"]));
            }
            set {
                this["RetrievePreviousSolutionThroughDiscretization"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool ApplyDistanceHeuristicInMST {
            get {
                return ((bool)(this["ApplyDistanceHeuristicInMST"]));
            }
            set {
                this["ApplyDistanceHeuristicInMST"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool TakeOnlyVehicleNumberEstimation {
            get {
                return ((bool)(this["TakeOnlyVehicleNumberEstimation"]));
            }
            set {
                this["TakeOnlyVehicleNumberEstimation"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool UseDiscretizedPreviousSolutio {
            get {
                return ((bool)(this["UseDiscretizedPreviousSolutio"]));
            }
            set {
                this["UseDiscretizedPreviousSolutio"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool ShareTopResultsBetweenProcesses {
            get {
                return ((bool)(this["ShareTopResultsBetweenProcesses"]));
            }
            set {
                this["ShareTopResultsBetweenProcesses"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("c*D.vrp;f*D.vrp;t*D.vrp")]
        public string FileFilter {
            get {
                return ((string)(this["FileFilter"]));
            }
            set {
                this["FileFilter"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("20")]
        public int Optimizer {
            get {
                return ((int)(this["Optimizer"]));
            }
            set {
                this["Optimizer"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("RC10")]
        public string Comment {
            get {
                return ((string)(this["Comment"]));
            }
            set {
                this["Comment"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("2")]
        public int ClassesForVehicle {
            get {
                return ((int)(this["ClassesForVehicle"]));
            }
            set {
                this["ClassesForVehicle"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1200")]
        public double TimeSliceTimeLimit {
            get {
                return ((double)(this["TimeSliceTimeLimit"]));
            }
            set {
                this["TimeSliceTimeLimit"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("140")]
        public int SwarmIterations {
            get {
                return ((int)(this["SwarmIterations"]));
            }
            set {
                this["SwarmIterations"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public int ExperimentsForFile {
            get {
                return ((int)(this["ExperimentsForFile"]));
            }
            set {
                this["ExperimentsForFile"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool PenalizeInsufficentVehicles {
            get {
                return ((bool)(this["PenalizeInsufficentVehicles"]));
            }
            set {
                this["PenalizeInsufficentVehicles"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("16")]
        public int Metaheuristic {
            get {
                return ((int)(this["Metaheuristic"]));
            }
            set {
                this["Metaheuristic"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool LogSubsequentStates {
            get {
                return ((bool)(this["LogSubsequentStates"]));
            }
            set {
                this["LogSubsequentStates"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0.5")]
        public decimal CutoffTime {
            get {
                return ((decimal)(this["CutoffTime"]));
            }
            set {
                this["CutoffTime"] = value;
            }
        }
    }
}
