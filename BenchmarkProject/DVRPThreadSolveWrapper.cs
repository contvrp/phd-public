﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using BenchmarkProjectComputingService;
using DynamicVehicleRouting;

namespace BenchmarkProject
{
    class DVRPThreadSolveWrapper
    {
        public DVRPInstance function;
        public double result;
        public int port;
        public Exception exception;
        static object lockCount = new object();
        static int countCurrent = 0;
        public DateTime startTime;
        public bool processesNotThreads;

        public DVRPThreadSolveWrapper()
        {
            startTime = DateTime.Now;
        }

        [ServiceContract(Namespace="http://DVRP2MPSO/")]
        public interface IFunctionSolverService
        {
            [OperationContract]
            FunctionWithResult Solve(DVRPInstance function);
        }

        public void Solve()
        {
            lock (lockCount)
            {
                Thread.CurrentThread.Name = (++countCurrent).ToString();
            }
            try
            {
                if (processesNotThreads)
                {
                    NetTcpBinding binding = new NetTcpBinding();
                    binding.CloseTimeout = new TimeSpan(1, 0, 0);
                    binding.OpenTimeout = new TimeSpan(1, 0, 0);
                    binding.ReceiveTimeout = new TimeSpan(1, 0, 0);
                    binding.SendTimeout = new TimeSpan(1, 0, 0);
                    binding.MaxReceivedMessageSize = int.MaxValue;
                    binding.MaxBufferSize = int.MaxValue;
                    binding.TransferMode = TransferMode.Buffered;
                    ChannelFactory<IFunctionSolverService> pipeFactory =
                        new ChannelFactory<IFunctionSolverService>(
                            binding,
                            new EndpointAddress(
                                string.Format("net.tcp://localhost:{0}/DVRPSolverService/", port)));
                    IFunctionSolverService pipeProxy =
                    pipeFactory.CreateChannel();
                    //here I should call a distant method
                    FunctionWithResult resultFunction = pipeProxy.Solve(function);
                    result = resultFunction.result;
                    function = resultFunction.function;
                    function.CacheDistances();
                }
                else
                {
                    result = function.Solve();
                }
                exception = null;
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            finally
            {
                lock (Benchmark.serviceAccess)
                {
                    if (Benchmark.busyServices.Any())
                    {
                        Benchmark.busyServices[port] = false;
                    }
                }
                Benchmark.serviceAccess.Release();
            }
            lock (lockCount)
            {
                --countCurrent;
            }
        }
    }
}
