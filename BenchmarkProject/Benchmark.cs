﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using DynamicVehicleRouting;
using ParticleSwarmOptimization;
using System.Drawing;
using System.Net;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace BenchmarkProject
{
    class Benchmark
    {
        static List<Process> computingServices = new List<Process>();
        public static Dictionary<int, bool> busyServices;
        static bool breakKiller = false;
        static Dictionary<Thread, DateTime> threadsCalls = new Dictionary<Thread, DateTime>();
        static readonly object threadlock = new object();
        static Semaphore semaphore = new Semaphore(
            Properties.Settings.Default.ThreadCount == -1 ? Environment.ProcessorCount : Properties.Settings.Default.ThreadCount,
            Properties.Settings.Default.ThreadCount == -1 ? Environment.ProcessorCount : Properties.Settings.Default.ThreadCount);
        public static Semaphore serviceAccess = new Semaphore(Environment.ProcessorCount, Environment.ProcessorCount);
        private static string outputFolderName = 
            string.Format("{0}-{1}",
            Path.GetInvalidFileNameChars().Aggregate(GetExperimentTypeForDB(), (current, c) => current.Replace(c, '-')),
            DateTime.Now.Ticks
            );

        static string path = Properties.Settings.Default.InitialDir;
        static readonly object resultslock = new object();
        private static int expCount = 1;

        static bool ConsoleEventCallback(int eventType)
        {
            TrySendMailAndInsertResultToDB(true);
            return false;
        }

        static ConsoleEventDelegate handler;   // Keeps it from getting garbage collected
        // Pinvoke
        private delegate bool ConsoleEventDelegate(int eventType);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);

        static void Main(string[] args)
        {
            Directory.CreateDirectory(outputFolderName);
            handler = new ConsoleEventDelegate(ConsoleEventCallback);
            SetConsoleCtrlHandler(handler, true);
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);
            Thread killer = new Thread(ThreadKiller);
            killer.Start();
            SortedSet<string> benchmarks = new SortedSet<string>();
            foreach (string filter in Properties.Settings.Default.FileFilter.Split(';'))
            {
                DirectoryInfo info = new DirectoryInfo(path);
                foreach (string file in info.GetFiles(filter, SearchOption.AllDirectories).Select(file => file.FullName))
                    benchmarks.Add(file);
            }
            StartServices();
            for (int i = 0; i < Properties.Settings.Default.ExperimentsForFile; i++)
            {
                List<string> benchmarksList = benchmarks.ToList();
                if (Properties.Settings.Default.RandomOrderOfInstances)
                    benchmarksList = benchmarks.OrderBy(bnch => Utils.Instance.random.Next()).ToList();
                foreach (string filepath in benchmarksList)
                {
                    Thread t = new Thread(RunBenchmark);
                    semaphore.WaitOne();
                    t.Start(filepath);
                    TrySendMailAndInsertResultToDB(false);
                }
            }
            for (int i = 0; i < (Properties.Settings.Default.ThreadCount == -1 ? Environment.ProcessorCount : Properties.Settings.Default.ThreadCount); i++)
                semaphore.WaitOne();
            TrySendMailAndInsertResultToDB(true);
            StopServices();

            Console.WriteLine("Press key to end application and synchronize with DB (check for I-net connection).");
            breakKiller = true;
            killer.Interrupt();
            killer.Join();
            Console.ReadKey();
            MockDBConnector.ExecuteInsertResults();
        }

        public static void StopServices()
        {
            foreach (var p in computingServices)
            {
                try
                {
                    p.Exited += p_Exited;
                    p.Kill();
                }
                catch
                {
                    //Does not metter
                }
            }
        }

        static void p_Exited(object sender, EventArgs e)
        {
            computingServices.Remove((Process)sender);
        }

        public static void StartServices()
        {
            while (computingServices.Count > 0)
            {
                if (computingServices[0].HasExited)
                    computingServices.RemoveAt(0);
                Thread.Sleep(500);
            }
            busyServices = new Dictionary<int, bool>();
            int port = 6540;
            int tries = 2;
            while (tries > 0)
            {
                try
                {
                    for (int services = 0; services < Environment.ProcessorCount; ++services)
                    {
#if DEBUG
#else
                        if (Properties.Settings.Default.UseProcessesNotThreads)
                        {
                            Process p = Process.Start(new ProcessStartInfo("BenchmarkProjectComputingService.exe", port.ToString())
                            {
                                CreateNoWindow = true,
                                UseShellExecute = false,
                                WindowStyle = ProcessWindowStyle.Hidden
                            });
                            computingServices.Add(p);
                            busyServices.Add(port, false);
                        }
#endif
                        port++;
                    }
                    break;
                }
                catch
                {
                    Thread.Sleep(tries * 1000 * 60 * 5);
                    tries--;
                    if (tries == 0)
                    {
                        StopServices();
                        Properties.Settings.Default.UseProcessesNotThreads = false;
                    }
                }
            }
        }

        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            StopServices();
            TrySendMailAndInsertResultToDB(true);
            }

        private static void TrySendMailAndInsertResultToDB(bool force)
        {
            MockMailSender.TrySendMessage(force);
            MockDBConnector.ExecuteInsertResults();
        }

        private static void RunBenchmark(object filepath)
        {
            if (Properties.Settings.Default.ExperimentsForFile <=
                MockDBConnector.CheckNumberOfExperiments(
                    (new FileInfo(filepath.ToString())).Name,
                    GetExperimentTypeForDB()
                ))
            {
                semaphore.Release();
                return;
            }

        REPEAT:
            try
            {
                lock (threadlock)
                {
                    threadsCalls.Add(Thread.CurrentThread, DateTime.Now);
                }
                int expNo = 0;
                lock (semaphore)
                {
                    expNo = expCount++;
                }
                List<DVRPThreadSolveWrapper> functions = new List<DVRPThreadSolveWrapper>();
                for (int i = 0; i < Properties.Settings.Default.ParallelInstances; i++)
                {
                    functions.Add(new DVRPThreadSolveWrapper()
                    {
                        function = new DVRPInstance(
                        (string)filepath,
                        Properties.Settings.Default.TimeStep,
                        Properties.Settings.Default.SwarmSize,
                        Properties.Settings.Default.MaxPSOSwarmNeighbourhoodSize,
                        Properties.Settings.Default.SwarmIterations,
                        Properties.Settings.Default.RouteSwarmIterations,
                        Properties.Settings.Default.NeighbourhoodProb1,
                        Properties.Settings.Default.InventorsRatio1,
                        Properties.Settings.Default.NeighbourhoodProb1,
                        Properties.Settings.Default.NeighbourhoodProb2,
                        Properties.Settings.Default.InitialTime,
                        (DVRPInstance.DegreeOfDynamicity)Properties.Settings.Default.DOD,
                        Properties.Settings.Default.CutoffTime,
                        Properties.Settings.Default.Optimizer,
                        Properties.Settings.Default.Metaheuristic,
                        Properties.Settings.Default.InitialCapacityPart,
                        Properties.Settings.Default.CapacitySlope,
                        Properties.Settings.Default.TraceDetailedInfo,
                        Properties.Settings.Default.DistanceWeightPower,
                        Properties.Settings.Default.UsePreviousSolution,
                        Properties.Settings.Default.RetrievePreviousSolutionThroughDiscretization,
                        Properties.Settings.Default.ShareTopResultsBetweenProcesses,
                        Properties.Settings.Default.UseDiscretizedPreviousSolutio,
                        Properties.Settings.Default.Use2Opt,
                        Properties.Settings.Default.ApplyDistanceHeuristicInMST,
                        Properties.Settings.Default.TakeOnlyVehicleNumberEstimation,
                        Properties.Settings.Default.PenalizeInsufficentVehicles,
                        Properties.Settings.Default.ClassesForVehicle,
                        Properties.Settings.Default.ReservoirVehiclesCount,
                        Properties.Settings.Default.InitialMonteCaroloPartOfTheDay,
                        Properties.Settings.Default.BetweenResampleMonteCarloIterations,
                        Properties.Settings.Default.TotalMonteCarloIterations,
                        Properties.Settings.Default.GenerateDataFromModel,
                        Properties.Settings.Default.UseUCB,
                        Properties.Settings.Default.RememberBandits,
                        Properties.Settings.Default.UseGreedyInsteadOfRandom,
                        Properties.Settings.Default.BufferSize,
                        Properties.Settings.Default.FullMulticlustering,
                        Properties.Settings.Default.TimeSliceTimeLimit,
                        Properties.DE.Default.MaxF,
                        Properties.DE.Default.MinF,
                        Properties.DE.Default.CrossProbability,
                        Properties.DE.Default.SeperateFForDim,
                        Properties.Problem.Default.AdvancedCommitmentTimeSteps,
                        Properties.Problem.Default.ScaleAdvancedCommitimentToPartOfTheDay
                        ),
#if DEBUG
                        processesNotThreads = false
#else
                        processesNotThreads = Properties.Settings.Default.UseProcessesNotThreads
#endif
                    });
                }
                DVRPInstance function = functions[0].function;
                /*
                string name = new FileInfo(filepath.ToString()).Name;
                File.WriteAllLines(
                    name.Split('.')[0] + ".txt",
                    function.Clients.Select(clnt => String.Format("{0:0} {1:0} {2:0} {3:0} {4:0} {5:0} {6:0}" ,clnt.Id, clnt.X, clnt.Y, clnt.StartAvailable, clnt.Need, function.Vehicles[0].Capacity, function.Depots[0].EndAvailable )));
                return;
                 * */
                /*
                List<double[]> points = function.Clients.Select(clnt => new double[] { clnt.X, clnt.Y, clnt.StartAvailable }).ToList();
                name = new FileInfo(filepath.ToString()).Name;
                double[,,] matrix1 = new double[5,5,5], matrix2 = new double[5,5,5];
                double minX = points.Min(pnt => pnt[0]);
                double minY = points.Min(pnt => pnt[1]);
                double minT = points.Min(pnt => pnt[2]);
                double maxX = points.Max(pnt => pnt[0]) + 1.0;
                double maxY = points.Max(pnt => pnt[1]) + 1.0;
                double maxT = points.Max(pnt => pnt[2]) + 1.0;
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        for (int k = 0; k < 5; k++)
                        {
                            matrix1[i, j, k] = 1.0 + ((double)points.Count(pnt => (pnt[2] - minT) >= (maxT - minT) * ((double)k) / 5.0 && (pnt[2] - minT) < (maxT - minT) * ((double)k + 1.0) / 5.0)) / 25.0;
                            matrix2[i, j, k] = 1.0 + points.Count(pnt =>
                                (pnt[0] - minX) >= (maxX - minX) * ((double)i) / 5.0 &&
                                (pnt[1] - minY) >= (maxY - minY) * ((double)j) / 5.0 &&
                                (pnt[2] - minT) >= (maxT - minT) * ((double)k) / 5.0 &&
                                (pnt[0] - minX) < (maxX - minX) * ((double)i + 1.0) / 5.0 &&
                                (pnt[1] - minY) < (maxY - minY) * ((double)j + 1.0) / 5.0 &&
                                (pnt[2] - minT) < (maxT - minT) * ((double)k + 1.0) / 5.0
                                );
                        }
                    }
                }
                File.AppendAllText("data.csv", name+Environment.NewLine);
                for (int k = 0; k < 5; k++)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        for (int j = 0; j < 5; j++)
                        {
                            File.AppendAllText("data.csv", matrix1[i, j, k] + " ");
                        }
                        for (int j = 0; j < 5; j++)
                        {
                            File.AppendAllText("data.csv", matrix2[i, j, k] + " ");
                        }
                        File.AppendAllText("data.csv", Environment.NewLine);
                    }
                }
                return;
                */
                /*
                string name = new FileInfo(filepath.ToString()).Name;
                int[] capacities = function.Vehicles.Select(vhcl => vhcl.Capacity).Distinct().ToArray();
                int[] needs = function.Clients.Select(clnt => clnt.Need).ToArray();

                File.AppendAllText("capacities.csv", name + ";" + Math.Ceiling(((double)needs.Sum()) / -capacities.Sum()) + Environment.NewLine);

                File.AppendAllText("capacities.csv", "Capacities;" + capacities.Sum() + Environment.NewLine);
                foreach (int capacity in capacities)
                {
                    File.AppendAllText("capacities.csv", capacity + Environment.NewLine);
                }
                File.AppendAllText("capacities.csv", "Needs;" + needs.Sum() + Environment.NewLine);
                foreach (int need in needs)
                {
                    File.AppendAllText("capacities.csv", need + Environment.NewLine);
                }
                return;
                */
                double routeLength = double.MaxValue;
                string lastImage = "";
                while (function.CurrentTime <= 1.0M)
                {
                    List<Thread> threads = new List<Thread>();
                    foreach (DVRPThreadSolveWrapper wrapper in functions)
                    {
                        if (wrapper.function.exportedSolutions != null)
                            function.importedSolutions.Add(wrapper.function.exportedSolutions);
                        if (wrapper.function.exportedVehicleSolutions != null)
                            function.importedVehicleSolutions.Add(wrapper.function.exportedVehicleSolutions);
                    }
                    foreach (DVRPThreadSolveWrapper wrapper in functions)
                    {
                        if (wrapper.function.bandits != null)
                            function.listOfBandits.Add(wrapper.function.bandits);
                        if (wrapper.function != function)
                        {
                            wrapper.function = function.Clone();
                            wrapper.function.SetSwarms(Properties.Settings.Default.SwarmSize,
                                Properties.Settings.Default.RouteSwarmIterations,
                                Properties.Settings.Default.SwarmIterations);

                        }
                        //Jeżeli jest więcej optymalizatorów to najlepszy jest zamieniany w szybkie MST z małą poprawką
                        else if (functions.Count > 1 && Properties.Settings.Default.AdditionalTreeOptimizer)
                        {
                            wrapper.function.SetSwarms(10,
                                10,
                                10);
                        }
                    }
                    foreach (DVRPThreadSolveWrapper wrapper in functions)
                    {
                        serviceAccess.WaitOne();
                        lock (serviceAccess)
                        {
                            if (busyServices.Any())
                            {
                                wrapper.port = busyServices.Where(pair => !pair.Value).Min(pair => pair.Key);
                                busyServices[wrapper.port] = true;
                            }
                        }
                        threads.Add(new Thread(wrapper.Solve));
                        threads.Last().Start();
                    }
                    Exception exception = new Exception();
                    foreach (Thread thread in threads)
                    {
                        thread.Join();
                        if (functions[threads.IndexOf(thread)].exception != null)
                        {
                            functions[threads.IndexOf(thread)].result = double.MaxValue;
                            if (exception != null)
                                exception = functions[threads.IndexOf(thread)].exception;
                        }
                        else
                        {
                            exception = null;
                        }
                        Console.WriteLine("{0}\t{1}\t{2:0.00}\t{3:0.00}\t{4}", DateTime.Now, new FileInfo(filepath.ToString()).Name, functions[threads.IndexOf(thread)].function.CurrentTime, functions[threads.IndexOf(thread)].result, string.Join(";", functions[threads.IndexOf(thread)].function.Vehicles.Where(vhcl1 => vhcl1.clientsToAssign.Count + vhcl1.assignedClients.Count > 0).Select(vhcl => vhcl.Id.ToString() + ":" + "(" + string.Join(",", (vhcl.assignedClients.Union(vhcl.clientsToAssign)).Select(clnt => clnt.Id.ToString())) + ")")));
                    }
                    if (exception != null)
                        throw exception;
                    routeLength = functions.Min(fctn2 => fctn2.result);
                    function = functions.Where(fctn => fctn.result == routeLength).Select(fctn => fctn.function).First();
                    foreach (DVRPThreadSolveWrapper wrapper in functions)
                    {
                        if (function.Results == null)
                        {
                            function.Results = new Dictionary<DVRPInstance.ContinuousMetaheuristic, List<double>>();
                        }
                        if (!function.Results.ContainsKey(wrapper.function.ChosenAlgorithm))
                        {
                            function.Results.Add(wrapper.function.ChosenAlgorithm, new List<double>());
                        }
                        function.Results[wrapper.function.ChosenAlgorithm].Add(wrapper.result / routeLength);

                    }
                    lock (threadlock)
                    {
                        threadsCalls[Thread.CurrentThread] = DateTime.Now;
                    }
                    if (Properties.Settings.Default.DrawEveryImage)
                    {
                        lastImage = DrawImageAndSaveState(filepath, expNo, lastImage, function);
                    }
                    //Utils.LogInfo("log2.txt", "{0}\t{1}\t{2:0.00}\t{3:0.00}", DateTime.Now, new FileInfo(filepath.ToString()).Name, function.CurrentTime, routeLength);
                    if (function.ClientRequestsCount != function.ClientsInVehiclesCount)
                        throw new Exception("Not all clients have been assigned!");
                    function.UpdateTime();
                    if (Properties.Settings.Default.LogSubsequentStates)
                    {
                        LogSubsequentStates(filepath, expNo, function);
                    }
                }
                function.CacheDistances();
                double computationsTime = (DateTime.Now - functions.Where(fctn => fctn.result == routeLength).First().startTime).TotalMilliseconds;
                MockDBConnector.InsertNewResult(DateTime.Now,
                    (new FileInfo(filepath.ToString())).Name,
                    routeLength,
                    function.DOD,
                    function.EvaluationCount,
                    function.bestKnown,
                    routeLength / function.bestKnown,
                    GetExperimentTypeForDB(),
                    computationsTime);
                Utils.Instance.LogInfo("results.txt", "{10}\t{0}\t{1}\t{2:0.00}\t{3:0.00}\t{4:0.00}\t{5}\t{6}\t{7}\t{8}\t{9}",
                    DateTime.Now,
                    (new FileInfo(filepath.ToString())).Name,
                    routeLength,
                    function.DOD,
                    function.Completness,
                    function.EvaluationCount,
                    function.bestKnown,
                    routeLength / function.bestKnown,
                    computationsTime,
                    function.Vehicles.Count(vhcl => vhcl.clientsToAssign.Count + vhcl.assignedClients.Count > 0),
                    GetExperimentTypeForDB()
                    );
                /*
                Utils.LogInfo("log2.txt",
                                "{9} Parametry: (DODType:{0} Swarm:{1} Iter:{2} Neighb1:{3} Inv1:{4} Neighb2:{5} Inv2:{6} InitCap:{7:0.00} Slope:{8:0.00} Optimizer:{9})",
                                Properties.Settings.Default.DOD,
                                Properties.Settings.Default.SwarmSize,
                                Properties.Settings.Default.SwarmIterations,
                                Properties.Settings.Default.NeighbourhoodProb1,
                                Properties.Settings.Default.InventorsRatio1,
                                Properties.Settings.Default.NeighbourhoodProb2,
                                Properties.Settings.Default.InventorsRatio2,
                                Properties.Settings.Default.InitialCapacityPart,
                                Properties.Settings.Default.CapacitySlope,
                                Properties.Settings.Default.Optimizer,
                                DateTime.Now
                );
                */
                Console.WriteLine("{0}\t{1}\t{2:0.00}\t{3:0.00}\t{4:0.00}\t{5}\t{6}\t{7}", DateTime.Now, (new FileInfo(filepath.ToString())).Name, routeLength, function.DOD, function.Completness, function.EvaluationCount, function.bestKnown, routeLength / function.bestKnown);
                if (!Properties.Settings.Default.DrawEveryImage)
                {
                    lastImage = DrawImageAndSaveState(filepath, expNo, lastImage, function);
                }
                if (lastImage != null && (Properties.Settings.Default.SendEmail || routeLength <= MockDBConnector.GetMinValueForExperiment((new FileInfo(filepath.ToString())).Name) + MockDBConnector.EPS))
                {
                    MockMailSender.AddToMessage(filepath, function, routeLength, lastImage);
                }
            }
            catch (Exception ex)
            {
                Utils.Instance.LogInfo("log2.txt", "{0}", ex.Message);
                Utils.Instance.LogInfo("exceptions.txt", ex.GetType().ToString());
                Utils.Instance.LogInfo("exceptions.txt", ex.Message);
                Utils.Instance.LogInfo("exceptions.txt", ex.StackTrace);
                string prefix = "";
                while (ex.InnerException != null)
                {
                    prefix += ">";
                    Utils.Instance.LogInfo("exceptions.txt", prefix + ex.InnerException.GetType().ToString());
                    ex = ex.InnerException;
                }
                expCount--;
                RestartServices();
                goto REPEAT;
            }
            finally
            {
                lock (threadlock)
                {
                    threadsCalls.Remove(Thread.CurrentThread);
                }
            }
            RestartServices();

            semaphore.Release();
        }

        private static void LogSubsequentStates(object filepath, int expNo, DVRPInstance function)
        {
            if (function.exportedSolutions != null)
            {
                File.AppendAllText(
                    Path.GetInvalidFileNameChars().Aggregate(GetExperimentTypeForDB(), (current, c) => current.Replace(c, '-'))
                    + "-" + new FileInfo(filepath.ToString()).Name
                    + "-" + expNo
                    + ".solutions.series",
                    string.Join(";", function.exportedSolutions) + "\n");
            }
            File.AppendAllText(
                Path.GetInvalidFileNameChars().Aggregate(GetExperimentTypeForDB(), (current, c) => current.Replace(c, '-'))
                + "-" + new FileInfo(filepath.ToString()).Name
                + "-" + expNo
                + ".assignments.series",
                string.Join(",",
                function.Clients.Select(clnt => clnt.FakeVehicleId)
                    ) + "\n");
            File.AppendAllText(
                Path.GetInvalidFileNameChars().Aggregate(GetExperimentTypeForDB(), (current, c) => current.Replace(c, '-'))
                + "-" + new FileInfo(filepath.ToString()).Name
                + "-" + expNo
                + ".assignments.series",
                string.Join(",",
                function.Clients.Select(clnt => function.Vehicles.Any(vhcl => vhcl.assignedClients.Any(clnt2 => clnt2.Id == clnt.Id) || vhcl.clientsToAssign.Any(clnt2 => clnt2.Id == clnt.Id)) ? 1 : 0)
                    ) + "\n");
            File.AppendAllText(
                Path.GetInvalidFileNameChars().Aggregate(GetExperimentTypeForDB(), (current, c) => current.Replace(c, '-'))
                + "-" + new FileInfo(filepath.ToString()).Name
                + "-" + expNo
                + ".assignments.series",
                string.Join(",",
                function.Clients.Select(clnt => function.Vehicles.Any(vhcl => vhcl.clientsToAssign.Any(clnt2 => clnt2.Id == clnt.Id)) ? 1 : 0)
                    ) + "\n");
        }

        private static void RestartServices()
        {
            if (Properties.Settings.Default.UseProcessesNotThreads)
            {
                StopServices();
                StartServices();
            }
        }

        private static string GetExperimentTypeForDB()
        {
            string template = "{0} {20}{21} {22} {9} {18}";
            if ((Properties.Settings.Default.Optimizer & ((int)DVRPInstance.FirstPhaseOptimizer.TreesMonteCarlo | (int)DVRPInstance.FirstPhaseOptimizer.PSOMonteCarlo)) > 0)
            {
                template += " {1}x{7}+{8}";
            }
            if ((Properties.Settings.Default.Optimizer & ((int)DVRPInstance.FirstPhaseOptimizer.Dapso
                | (int)DVRPInstance.FirstPhaseOptimizer.AiKachitvichyanukul
                | (int)DVRPInstance.FirstPhaseOptimizer.HansharGA
                | (int)DVRPInstance.FirstPhaseOptimizer.PSOMonteCarlo)) > 0)
            {
                template += "{13}{14}{15}{16}{19}{17}x{11}+{12} {1}/{2:000}/";
                if (Properties.Settings.Default.TimeSliceTimeLimit < 1000)
                {
                    template += "{6:0.00sec}";
                }
                else
                {
                    template += "{3:000}/{4}";
                }
                if ((Properties.Settings.Default.Optimizer & (int)DVRPInstance.ContinuousMetaheuristic.DE) > 0)
                {
                    template += "C{10:0.00}";
                }
            }
            if (Properties.Settings.Default.Optimizer == (int)DVRPInstance.FirstPhaseOptimizer.TreeClustering)
            {
                template += " {1}";
            }
            template += "({5})";
            string type = string.Format(template,
                                Properties.Settings.Default.Comment,
                                Properties.Settings.Default.ParallelInstances,
                                Properties.Settings.Default.SwarmSize,
                                Properties.Settings.Default.SwarmIterations,
                                Properties.Settings.Default.RouteSwarmIterations,
                                Properties.Settings.Default.TimeStep,
                                Properties.Settings.Default.TimeSliceTimeLimit,
                                Properties.Settings.Default.BetweenResampleMonteCarloIterations,
                                Properties.Settings.Default.TotalMonteCarloIterations,
                                Properties.Settings.Default.Optimizer,
                                Properties.DE.Default.CrossProbability,
                                Properties.Settings.Default.ClassesForVehicle,
                                Properties.Settings.Default.ReservoirVehiclesCount,
                                (Properties.Settings.Default.FullMulticlustering ? "v3" : "v2"),
                                (Properties.Settings.Default.RetrievePreviousSolutionThroughDiscretization && Properties.Settings.Default.UsePreviousSolution ? "Dscrt" : "Orgnl"),
                                (Properties.Settings.Default.UseDiscretizedPreviousSolutio && Properties.Settings.Default.UsePreviousSolution ? "D" : ""),
                                (Properties.Settings.Default.ShareTopResultsBetweenProcesses && Properties.Settings.Default.UsePreviousSolution ? "Share" : ""),
                                (Properties.Settings.Default.PenalizeInsufficentVehicles ? "P" : ""),
                                Properties.Settings.Default.Metaheuristic,
                                (Properties.Settings.Default.UsePreviousSolution ? "" : "NoHist"),
                                Properties.Problem.Default.AdvancedCommitmentTimeSteps * Properties.Settings.Default.TimeStep,
                                (Properties.Problem.Default.ScaleAdvancedCommitimentToPartOfTheDay ? "S" : ""),
                                (Properties.Settings.Default.CutoffTime)
                                )
                                .Replace(',', '.');
            //type = type.Substring(0, Math.Min(50, type.Length));
            return type;

        }

        private static string DrawImageAndSaveState(object filepath, int expNo, string lastImage, DVRPInstance function)
        {
            try
            {
                if (!Directory.Exists(outputFolderName))
                    Directory.CreateDirectory(outputFolderName);
                Image img = new Bitmap(1024, 768);
                DynamicVehicleRouting.Visualize.Visualizer.DrawVehicleAssignment(function, Graphics.FromImage(img));
                img.Save(lastImage = string.Format("{4}//{0}.{3:000}-{1}.{2}", new FileInfo(filepath.ToString()).Name, function.CurrentTime, "png", expNo, outputFolderName), System.Drawing.Imaging.ImageFormat.Png);

                //Generate instances with only one vehicle
                if (Properties.Settings.Default.SeparateImagesForRoutes)
                {
                    foreach (Vehicle vehicle in function.Vehicles)
                    {
                        if (vehicle.clientsToAssign.Count() + vehicle.assignedClients.Count() > 0)
                        {
                            DVRPInstance oneVehicleFunction = function.Clone();
                            foreach (Vehicle vehicle2 in oneVehicleFunction.Vehicles)
                            {
                                if (vehicle2.Id != vehicle.Id)
                                {
                                    vehicle2.assignedClients.Clear();
                                    vehicle2.clientsToAssign.Clear();
                                }
                            }
                            img = new Bitmap(1024, 768);
                            DynamicVehicleRouting.Visualize.Visualizer.DrawVehicleAssignment(oneVehicleFunction, Graphics.FromImage(img));
                            img.Save(lastImage = string.Format("{5}//{0}.{3:000}-{1}-{4:00}.{2}", new FileInfo(filepath.ToString()).Name, function.CurrentTime, "png", expNo, vehicle.Id, outputFolderName), System.Drawing.Imaging.ImageFormat.Png);
                        }
                    }
                }

                string csvFile = string.Format("{4}//{0}.{3:000}-{1}.{2}", new FileInfo(filepath.ToString()).Name, function.CurrentTime, "txt", expNo, outputFolderName);
                if (File.Exists(csvFile))
                    File.Delete(csvFile);
                File.AppendAllText(csvFile, new FileInfo(filepath.ToString()).Name + "\t" + function.bestKnown + "\t" + function.CurrentTime + "\t" + function.capacity + Environment.NewLine);
                File.AppendAllText(csvFile, string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t", "VehicleId", "ClientId", "X", "Y", "StartTime", "EndTime", "ArriveTime", "Need", "Assigned", "Unload") + Environment.NewLine);

                foreach (Vehicle v in function.Vehicles)
                {
                    foreach (Client c in v.assignedClients)
                    {
                        File.AppendAllText(csvFile, string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t", v.Id, c.Id, c.X, c.Y, c.StartAvailable, c.EndAvailable, c.VisitTime, -c.Need, true, c.TimeToUnload) + Environment.NewLine);
                    }
                    foreach (Client c in v.clientsToAssign)
                    {
                        File.AppendAllText(csvFile, string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t", v.Id, c.Id, c.X, c.Y, c.StartAvailable, c.EndAvailable, c.VisitTime, -c.Need, false, c.TimeToUnload) + Environment.NewLine);
                    }
                }
                return lastImage;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        static void ThreadKiller()
        {
            while (true)
            {
                try
                {
                    try
                    {
                        Thread.Sleep(DVRPInstance.timeout);
                    }
                    catch (ThreadInterruptedException)
                    {

                    }
                    if (breakKiller)
                        return;
                    lock (threadlock)
                    {
                        foreach (Thread t in threadsCalls.Keys)
                            if (DateTime.Now - threadsCalls[t] > DVRPInstance.timeout)
                                t.Abort();
                    }
                }
                catch (ThreadAbortException)
                {
                    break;
                }
            }
        }

    }
}
