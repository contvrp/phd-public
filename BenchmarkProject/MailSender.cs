﻿using DynamicVehicleRouting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace BenchmarkProject
{
    public class MockMailSender
    {
        static SmtpClient client = new SmtpClient();
        static MailMessage message = null;
        static DateTime lastMessage = DateTime.Now;

        static MockMailSender()
        {
            //TODO: setup client here
        }

        public static void AddToMessage(object filepath, DVRPInstance function, double routeLength, string lastImage)
        {
            lock (client)
            {
                if (message == null)
                {
                    message = new MailMessage(
                        "from@example.com",
                        "to@example.com",
                        string.Format(Properties.Settings.Default.Comment + "({10}) Parametry: (DODType:{0} Swarm:{1} Iter:{2} Neighb1:{3} Inv1:{4} Neighb2:{5} Inv2:{6} InitCap:{7:0.00} Slope:{8:0.00} Optimizer:{9} History: {11} 2Opt: {12})",
                            Properties.Settings.Default.DOD,
                            Properties.Settings.Default.SwarmSize,
                            Properties.Settings.Default.SwarmIterations + "/" + Properties.Settings.Default.RouteSwarmIterations,
                            Properties.Settings.Default.NeighbourhoodProb1,
                            Properties.Settings.Default.InventorsRatio1,
                            Properties.Settings.Default.NeighbourhoodProb2,
                            Properties.Settings.Default.InventorsRatio2,
                            Properties.Settings.Default.InitialCapacityPart,
                            Properties.Settings.Default.CapacitySlope,
                            Properties.Settings.Default.Optimizer,
                            Properties.Settings.Default.ParallelInstances,
                            Properties.Settings.Default.UsePreviousSolution,
                            Properties.Settings.Default.Use2Opt),
                        string.Format("{0} {1} {2}", Environment.MachineName, Environment.OSVersion, Environment.ProcessorCount) +
                        Environment.NewLine + string.Format("{0}\t{1}\t{2:0.00}\t{3:0.00}\t{4:0.00}\t{5}\t{6}\t{7}", "Godzina", "Test", "Trasa", "dod", "Ukończono", "Obliczeń", "Znany", "Jakość"));
                    message.Bcc.Add("optymalizacja.rojowa@gmail.com");
                }
                message.Body += Environment.NewLine;
                message.Body += string.Format("\"{0}\"\t{1}\t{2:0.00}\t{3:0.00}\t{5}\t{6}\t{7}", DateTime.Now, (new FileInfo(filepath.ToString())).Name, routeLength, function.DOD, function.Completness, function.EvaluationCount, function.bestKnown, routeLength / function.bestKnown);
                message.Attachments.Add(new Attachment(lastImage));
                message.Attachments.Add(new Attachment(lastImage.Replace(".png", ".txt")));
            }
        }

        public static void TrySendMessage(bool force)
        {
            lock (client)
            {
                if (message != null && (force || (((DateTime.Now - lastMessage).TotalMinutes > (new Random()).Next(10) + 25) || message.Body.Count(ch => ch == '\n') > 24)))
                {
                    try
                    {
                        //TODO: define sending mail here
                    }
                    catch
                    {
                        //Wysłanie maila nie jest super ważne
                    }
                }
            }
        }

    }
}
