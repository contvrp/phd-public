﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace BenchmarkProject
{
    class MockDBConnector
    {
        public const double EPS = 0.05;

        public static void InsertNewResult(DateTime timestamp, string name, double result, decimal dod, int iterations, double bestKnownResult, double resultToBest, string type, double computationsTime)
        {
        }

        public static void ExecuteInsertResults()
        {
        }

        public static int CheckNumberOfExperiments(string name, string type)
        {
            return 0;
        }

        public static double GetMinValueForExperiment(string name)
        {
            return double.PositiveInfinity;
        }


    }
}
