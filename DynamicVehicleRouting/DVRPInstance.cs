﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using ParticleSwarmOptimization;

namespace DynamicVehicleRouting
{
    [DataContract]
    [KnownType(typeof(double[]))]
    public class DVRPInstance
    {
        public static TimeSpan timeout = new TimeSpan(0, 10, 0);
        [ThreadStatic]
        public static DateTime solvingStartTime;

        [DataMember]
        public double bestKnown;

        [DataMember]
        public int capacity;

        [DataMember]
        public Client[] Clients;

        [DataMember]
        public Depot[] Depots;

        [DataMember]
        public int EvaluationCount = 0;

        [DataMember]
        public Vehicle[] Vehicles;

        private static readonly object loggerLocker = new object();

        [DataMember]
        private decimal capacitySlope;

        [DataMember]
        private int previousStepClientsCount = 0;

        [DataMember]
        private List<Client> clientsToAssign;

        [DataMember]
        private decimal currentTime;

        [DataMember]
        private bool discrete = true;

        internal double[,] distances;

        [DataMember]
        public Dictionary<ContinuousMetaheuristic, List<double>> Results { get; set; }

        [DataMember]
        public ContinuousMetaheuristic ChosenAlgorithm { get; set; }

        [DataMember]
        private DegreeOfDynamicity dod;

        [DataMember]
        internal string filename;

        [DataMember]
        private decimal initialCapacityPart;

        // Pentium 4 @2.8 4.31 Gflop/s
        //Intel i7/2nd@3.4 55 Gflop/s
        //Intel i7/4rth@2.4 36 Gflop/s
        //Intel i5/4rth@2.5 13 Gflop/s (use like 26)
        [DataMember]
        private double timeLimit;

        #region PSO properties
        [DataMember]
        private int internalSwarmIterations;

        [DataMember]
        private int internalSwarmSize;

        [DataMember]
        private int maxPSOSwarmNeighbourhoodSize;

        [DataMember]
        private double inventorsRatio1;

        [DataMember]
        private double inventorsRatio2;

        [DataMember]
        private double neighbourProbability1;

        [DataMember]
        private double neighbourProbability2;
        #endregion

        [DataMember]
        private int optimizer;

        [DataMember]
        private int metaheuristic;

        [DataMember]
        private double previousBestValue = double.MaxValue;

        [DataMember]
        private int routeSwarmIterations;

        [DataMember]
        internal Vehicle[] StartVehicles;

        [DataMember]
        private decimal cutoffTime;

        [DataMember]
        private decimal timeStep;

        [DataMember]
        private decimal advancedCommitmentTimeSteps;

        [DataMember]
        private bool scaleAdvancedCommitimentToPartOfTheDay;

        [DataMember]
        private bool traceInfo;

        [DataMember]
        private bool use2Opt;

        #region Solutions management
        [DataMember]
        private bool usePreviousSolutions;
        [DataMember]
        private bool retrievePreviousSolutionThroughDiscretization;
        [DataMember]
        private bool shareServicesBestSolutions;
        [DataMember]
        private bool useDiscretizedPreviousSolution;
        #endregion 

        [DataMember]
        private double weightPowerParameter;

        [DataMember]
        private int classesForVehicle;
        [DataMember]
        private int reservoirVehiclesCount;
        [DataMember]
        private decimal initialPartOfTheDay;
        [DataMember]
        private int initialIterations;
        [DataMember]
        private int partialIterations;
        [DataMember]
        private bool generateDataFromModel;
        [DataMember]
        public object bandits;
        [DataMember]
        public List<object> listOfBandits = new List<object>();
        [DataMember]
        private bool useUCB;
        [DataMember]
        private bool rememberBandits;
        [DataMember]
        private bool useGreedyInMC;
        [DataMember]
        private decimal bufferSize;
        [DataMember]
        private bool fullMulticlustering;
        [DataMember]
        private bool applyDistanceHeuristic;
        [DataMember]
        private bool takeOnlyVehicleNumberEstimation;
        [DataMember]
        private bool penalizeInsufficentVehicles;
        [DataMember]
        private int previousClientsToAssign;
        [DataMember]
        private int previousEstimatedVehicles;
        [DataMember]
        public List<double[]> importedSolutions = new List<double[]>();
        [DataMember]
        public double[] exportedSolutions;
        [DataMember]
        public List<int[]> importedVehicleSolutions = new List<int[]>();
        [DataMember]
        public int[] exportedVehicleSolutions;
        [DataMember]
        private List<int> lastClientsIds = new List<int>();


        #region DE properties
        [DataMember]
        private double de_maxF;
        [DataMember]
        private double de_minF;
        [DataMember]
        private double de_crossProb;
        [DataMember]
        private bool de_separateFForEachDim;
        #endregion

        public DVRPInstance(
            string filename,
            decimal timeStep,
            int internalSwarmSize,
            int maxPSOSwarmNeighbourhoodSize,
            int internalSwarmIterations,
            int routeSwarmIterations,
            double neighbourProbability1,
            double inventorsRatio1,
            double neighbourProbability2,
            double inventorsRatio2,
            decimal currentTime,
            DegreeOfDynamicity dod,
            decimal cutoffTime,
            int optimizer,
            int metaheuristic,
            decimal initialCapacityPart,
            decimal capacitySlope,
            bool traceInfo,
            double weightPowerParameter,
            bool usePreviousSolutions,
            bool retrievePreviousSolutionThroughDiscretization,
            bool shareServicesBestSolutions,
            bool useDiscretizedPreviousSolution,
            bool use2Opt,
            bool applyDistanceHeuristic,
            bool takeOnlyVehicleNumberEstimation,
            bool penalizeInsufficentVehicles,
            int classesForVehicle,
            int reservoirVehiclesCount,
            decimal initialPartOfTheDay,
            int initialIterations,
            int partialIterations,
            bool generateDataFromModel,
            bool useUCB,
            bool remeberBandits,
            bool useGreedyInMC,
            decimal bufferSize,
            bool fullMulticlustering,
            double timeSliceTimeLimit,
            double deMaxF,
            double deMinF,
            double deCrossover,
            bool deSeparateFForDim,
            decimal advancedCommitmentTimeSteps = 2.0M,
            bool scaleAdvancedCommitimentToPartOfTheDay = false
            )
        {
            this.filename = filename;
            this.timeStep = timeStep;
            this.internalSwarmSize = internalSwarmSize;
            this.internalSwarmIterations = internalSwarmIterations;
            this.routeSwarmIterations = routeSwarmIterations;
            this.optimizer = optimizer;
            this.metaheuristic = metaheuristic;
            this.dod = dod;
            this.neighbourProbability1 = neighbourProbability1;
            this.inventorsRatio1 = inventorsRatio1;
            this.neighbourProbability2 = neighbourProbability2;
            this.inventorsRatio2 = inventorsRatio2;
            this.traceInfo = traceInfo;
            this.initialCapacityPart = initialCapacityPart;
            this.capacitySlope = capacitySlope;
            this.weightPowerParameter = weightPowerParameter;
            this.usePreviousSolutions = usePreviousSolutions;
            this.retrievePreviousSolutionThroughDiscretization = retrievePreviousSolutionThroughDiscretization;
            this.useDiscretizedPreviousSolution = useDiscretizedPreviousSolution;
            this.shareServicesBestSolutions = shareServicesBestSolutions;
            this.use2Opt = use2Opt;
            this.applyDistanceHeuristic = applyDistanceHeuristic;
            this.takeOnlyVehicleNumberEstimation = takeOnlyVehicleNumberEstimation;
            this.penalizeInsufficentVehicles = penalizeInsufficentVehicles;
            this.classesForVehicle = classesForVehicle;
            this.reservoirVehiclesCount = reservoirVehiclesCount;
            this.initialIterations = initialIterations;
            this.initialPartOfTheDay = initialPartOfTheDay;
            this.partialIterations = partialIterations;
            this.generateDataFromModel = generateDataFromModel;
            this.useUCB = useUCB;
            this.rememberBandits = remeberBandits;
            this.useGreedyInMC = useGreedyInMC;
            this.bufferSize = bufferSize;
            this.fullMulticlustering = fullMulticlustering;
            this.timeLimit = timeSliceTimeLimit;
            this.de_maxF = deMaxF;
            this.de_minF = deMinF;
            this.de_crossProb = deCrossover;
            this.de_separateFForEachDim = deSeparateFForDim;
            this.advancedCommitmentTimeSteps = advancedCommitmentTimeSteps;
            this.scaleAdvancedCommitimentToPartOfTheDay = scaleAdvancedCommitimentToPartOfTheDay;
            this.maxPSOSwarmNeighbourhoodSize = maxPSOSwarmNeighbourhoodSize;
            this.cutoffTime = cutoffTime;
            StreamReader reader = new StreamReader(filename);
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (line.StartsWith("COMMENT: Best known objective:"))
                {
                    line = line.Replace("COMMENT: ", "");
                    this.bestKnown = GetDoubleNumberFromLine(line);
                }
                else if (line.StartsWith("NUM_DEPOTS"))
                {
                    Depots = new Depot[GetNumberFromLine(line)];
                }
                else if (line.StartsWith("NUM_CAPACITIES"))
                {
                    if (GetNumberFromLine(line) != 1)
                        throw new Exception("Nie obsługuję różnych typów towarów");
                }
                else if (line.StartsWith("NUM_VISITS"))
                {
                    Clients = new Client[GetNumberFromLine(line)];
                }
                else if (line.StartsWith("NUM_LOCATIONS"))
                {
                    Depots = new Depot[GetNumberFromLine(line) - Clients.Length];
                }
                else if (line.StartsWith("NUM_VEHICLES"))
                {
                    Vehicles = new Vehicle[GetNumberFromLine(line)];
                }
                else if (line.StartsWith("CAPACITIES"))
                {
                    int capacity = GetNumberFromLine(line);
                    this.capacity = capacity;
                    for (int i = 0; i < Vehicles.Length; ++i)
                    {
                        Vehicles[i] = new Vehicle();
                        Vehicles[i].Id = i + Clients.Length;
                        Vehicles[i].clientsToAssign = new List<Client>();
                        Vehicles[i].assignedClients = new List<Client>();
                        Vehicles[i].Available = true;
                        Vehicles[i].Capacity = capacity;
                    }
                }
                else if (line.StartsWith("DEPOTS"))
                {
                    for (int i = 0; i < Depots.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Depots[i] = new Depot();
                        Depots[i].Id = numbers[0];
                    }
                }
                else if (line.StartsWith("DEMAND_SECTION"))
                {
                    for (int i = 0; i < Clients.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Clients[i] = new Client();
                        Clients[i].Id = numbers[0];
                        Clients[i].Need = numbers[1];
                        Clients[i].Rank = (decimal)(2.0 * (Utils.Instance.random.NextDouble() - 0.5));
                        Clients[i].FakeVehicleId = Utils.Instance.random.Next(Vehicles.Length) + Vehicles.Min(vhcl => vhcl.Id);
                    }
                }
                else if (line.StartsWith("LOCATION_COORD_SECTION"))
                {
                    for (int i = 0; i < Clients.Length + Depots.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Client client = null;
                        if (Clients.Count(clnt => clnt.Id == numbers[0]) > 0)
                        {
                            client = Clients.First(clnt => clnt.Id == numbers[0]);
                        }
                        else
                        {
                            client = Depots.First(dpt => dpt.Id == numbers[0]);
                            for (int j = 0; j < Vehicles.Length; j++)
                            {
                                Vehicles[j].X = new double[classesForVehicle];
                                Vehicles[j].Y = new double[classesForVehicle];
                                for (int m = 0; m < classesForVehicle; ++m)
                                {
                                    Vehicles[j].X[m] = numbers[1] + (2.0 * (Utils.Instance.random.NextDouble() - 0.5));
                                    Vehicles[j].Y[m] = numbers[2] + (2.0 * (Utils.Instance.random.NextDouble() - 0.5));
                                }
                            }
                        }
                        client.X = numbers[1];
                        client.Y = numbers[2];
                    }
                }
                else if (line.StartsWith("DURATION_SECTION"))
                {
                    for (int i = 0; i < Clients.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Client client = Clients.First(clnt => clnt.Id == numbers[0]);
                        if (client != null)
                            client.TimeToUnload = numbers[1];
                    }
                }
                else if (line.StartsWith("DEPOT_TIME_WINDOW_SECTION"))
                {
                    for (int i = 0; i < Depots.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Depot depot = Depots.First(dpt => dpt.Id == numbers[0]);
                        if (depot != null)
                        {
                            depot.StartAvailable = numbers[1];
                            depot.EndAvailable = numbers[2] * (this.dod == DegreeOfDynamicity.None ? 100 : 1);
                        }
                    }
                }
                else if (line.StartsWith("TIME_AVAIL_SECTION"))
                {
                    for (int i = 0; i < Clients.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Client client = Clients.First(clnt => clnt.Id == numbers[0]);
                        if (client != null)
                        {
                            client.StartAvailable = numbers[1];
                            if (dod == DegreeOfDynamicity.Automatic && client.StartAvailable + client.TimeToUnload +
                                (decimal)(2 * distances[client.Id, Depots.First().Id]) > (decimal)MaxTime)
                            {
                                client.StartAvailable = 0;
                            }
                            else if (dod == DegreeOfDynamicity.Manual && client.StartAvailable > cutoffTime * (decimal)MaxTime)
                            {
                                client.StartAvailable = 0;
                            }
                            else if (dod == DegreeOfDynamicity.Static)
                            {
                                client.StartAvailable = 0;
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < Vehicles.Length; ++i)
            {
                Vehicles[i].SetDepot((Depot)Depots[0].Clone());
            }
            reader.Close();
            reader.Dispose();
            CacheDistances();
            /*
            NumericVector needs = engine.CreateNumericVector(Clients.Where(clnt => clnt.StartAvailable == 0).Select(clnt => (double)-clnt.Need).ToArray());
            engine.SetSymbol("needs", needs);
            double moment = engine.Evaluate("moment((needs-mean(needs))/sd(needs), order=3) ").AsNumeric()[0];

            NumericVector x = engine.CreateNumericVector(Clients.Where(clnt => clnt.StartAvailable == 0).Select(clnt => (double)clnt.X).ToArray());
            NumericVector y = engine.CreateNumericVector(Clients.Where(clnt => clnt.StartAvailable == 0).Select(clnt => (double)clnt.Y).ToArray());
            engine.SetSymbol("x", x);
            engine.SetSymbol("y", y);
            engine.Evaluate("xy = cbind(x,y)");
            engine.Evaluate("gap = clusGap(xy, FUN = kmeans, nstart = 20, K.max = nrow(xy)/2, B = 50)");
            engine.Evaluate("idx = which.max(ifelse(gap$Tab[,3]==Inf,0,gap$Tab[,3]))");
            double gap = engine.Evaluate("nrow(xy)/(min(which(gap$Tab[idx,3]-gap$Tab[idx,4] < gap$Tab[,3])))").AsNumeric()[0];

            Console.WriteLine(moment);
            Console.WriteLine(gap);
            */
            this.CurrentTime = currentTime;
        }

        static DVRPInstance()
        {
        }

        public void CacheDistances()
        {
            if (distances == null)
                distances = new double[Clients.Length + Depots.Length, Clients.Length + Depots.Length];
            else if (distances.GetLength(0) < Clients.Length + Depots.Length || distances.GetLength(1) < Clients.Length + Depots.Length)
                distances = new double[Clients.Length + Depots.Length, Clients.Length + Depots.Length];
            foreach (Depot d1 in Depots)
            {
                foreach (Depot d2 in Depots)
                {
                    distances[d1.Id, d2.Id] = Utils.Instance.EuclideanDistance(new double[] { d1.X, d1.Y }, new double[] { d2.X, d2.Y });
                }
                foreach (Client c in Clients)
                {
                    distances[d1.Id, c.Id] = Utils.Instance.EuclideanDistance(new double[] { d1.X, d1.Y }, new double[] { c.X, c.Y });
                }
            }
            foreach (Client c1 in Clients)
            {
                foreach (Depot d in Depots)
                {
                    distances[c1.Id, d.Id] = Utils.Instance.EuclideanDistance(new double[] { c1.X, c1.Y }, new double[] { d.X, d.Y });
                }
                foreach (Client c2 in Clients)
                {
                    distances[c1.Id, c2.Id] = Utils.Instance.EuclideanDistance(new double[] { c1.X, c1.Y }, new double[] { c2.X, c2.Y });
                }
            }
            foreach (Vehicle vehicle in Vehicles)
            {
                vehicle.Distances = distances;
            }
        }

        private DVRPInstance()
        {
            listOfBandits = new List<object>();
        }

        public delegate void UpdateBestEvent(DVRPInstance function);

        public event UpdateBestEvent UpdatedBest;
        internal const bool DebugMode = false;

        [DataContract]
        public enum DegreeOfDynamicity
        {
            [EnumMember(Value = "1")]
            Full = 1,
            [EnumMember(Value = "2")]
            Automatic = 2,
            [EnumMember(Value = "3")]
            Manual = 3,
            [EnumMember(Value = "4")]
            None = 4,
            [EnumMember(Value = "5")]
            Static = 5
        }

        [DataContract]
        public enum FirstPhaseOptimizer
        {
            [EnumMember(Value = "0")]
            Empty = 0,
            [EnumMember(Value = "1")]
            VehicleClustering = 1,
            [EnumMember(Value = "2")]
            ClientAssignment = 2,
            [EnumMember(Value = "4")]
            TreeClustering = 4,
            [EnumMember(Value = "8")]
            Partitioning = 8,
            [EnumMember(Value = "16")]
            Dapso = 16,
            [EnumMember(Value = "32")]
            MonteCarlo = 32,
            [EnumMember(Value = "64")]
            TreesMonteCarlo = 64,
            [EnumMember(Value = "128")]
            PSOMonteCarlo = 128,
            [EnumMember(Value = "4096")]
            AiKachitvichyanukul = 4096,
            [EnumMember(Value = "8192")]
            HansharGA = 8192,
            [EnumMember(Value = "16384")]
            KhouadjiaEncoding = 16384,
        }


        [DataContract]
        public enum ContinuousMetaheuristic
        {
            [EnumMember(Value = "0")]
            Empty = 0,
            [EnumMember(Value = "16")]
            PSO = 16,
            [EnumMember(Value = "256")]
            DE = 256,
            [EnumMember(Value = "512")]
            RandomWalk = 512,
            [EnumMember(Value = "1024")]
            RandomClustering = 1024,
            [EnumMember(Value = "2048")]
            GridLog = 2048,
        }

        public List<Vehicle> AvailableVehicles
        {
            get
            {
                return Vehicles.Where(vhcl => vhcl.Available).ToList();
            }
        }

        public int ClientRequestsCount
        {
            get
            {
                return Clients.Where(clnt => clnt.StartAvailable <= currentTime * Depots.Max(dpt => dpt.EndAvailable)).Count();
            }
        }

        public int ClientsInVehiclesCount
        {
            get
            {
                return Vehicles.Sum(vhcl => vhcl.clientsToAssign.Where(clnt => !(clnt is Depot)).Count() + vhcl.assignedClients.Where(clnt => !(clnt is Depot)).Count());
            }
        }

        public int AssignedClientsInVehiclesCount
        {
            get
            {
                return Vehicles.Sum(vhcl => vhcl.assignedClients.Where(clnt => !(clnt is Depot)).Count());
            }
        }

        public List<Client> ClientsToAssign
        {
            get
            {
                return clientsToAssign;
            }
        }

        public decimal Completness
        {
            get
            {
                return (decimal)Clients.Count(clnt => clnt.VisitTime + clnt.TimeToUnload +
                    (decimal)Depots.Min(dpt => distances[
                        dpt.Id,
                        clnt.Id
                        ]) <= (decimal)MaxTime
                    ) / Clients.Length;
            }
        }

        public decimal CurrentTime
        {
            get
            {
                return currentTime;
            }
            set
            {
                currentTime = value;
                SetClientsToAssign();
                ResetVehicles();
            }
        }

        public decimal DOD
        {
            get
            {
                return (decimal)Clients.Count(clnt => clnt.StartAvailable > 0) / Clients.Length;
            }
        }
        /// <summary>
        /// Czas zamknięcia zajezdni w jednostkach problemu
        /// </summary>
        public double MaxTime
        {
            get { return Depots.Max(depot => depot.EndAvailable); }
        }

        public decimal NextTime
        {
            get
            {
                return currentTime + timeStep;
            }
        }

        /// <summary>
        /// Poprzedni czas (w częściach całości)
        /// </summary>
        public decimal PreviousTime
        {
            get
            {
                return currentTime - timeStep;
            }
        }

        /// <summary>
        /// Kopiuje instancję problemu - trzeba pamiętać o aktualizacji tej funkcji po istotniejszych zmianach
        /// </summary>
        /// <returns></returns>
        public DVRPInstance Clone()
        {
            DVRPInstance cloneFunction = new DVRPInstance();
            cloneFunction.bestKnown = this.bestKnown;
            cloneFunction.capacity = this.capacity;
            cloneFunction.capacitySlope = this.capacitySlope;
            cloneFunction.Clients = new Client[this.Clients.Length];
            for (int i = 0; i < Clients.Length; i++)
            {
                cloneFunction.Clients[i] = (Client)this.Clients[i].Clone();
            }
            cloneFunction.previousStepClientsCount = this.previousStepClientsCount;
            cloneFunction.Depots = new Depot[this.Depots.Length];
            for (int i = 0; i < Depots.Length; i++)
            {
                cloneFunction.Depots[i] = (Depot)this.Depots[i].Clone();
            }
            cloneFunction.Vehicles = new Vehicle[this.Vehicles.Length];
            for (int i = 0; i < Vehicles.Length; i++)
            {
                cloneFunction.Vehicles[i] = (Vehicle)this.Vehicles[i].Clone();
            }
            if (StartVehicles != null)
            {
                cloneFunction.StartVehicles = new Vehicle[this.StartVehicles.Length];
                for (int i = 0; i < StartVehicles.Length; i++)
                {
                    cloneFunction.StartVehicles[i] = (Vehicle)this.StartVehicles[i].Clone();
                }
            }
            //TODO: bandits should be merged
            cloneFunction.bandits = this.bandits;
            cloneFunction.dod = this.dod;
            cloneFunction.EvaluationCount = this.EvaluationCount;
            cloneFunction.filename = this.filename;
            cloneFunction.initialCapacityPart = this.initialCapacityPart;
            cloneFunction.internalSwarmIterations = this.internalSwarmIterations;
            cloneFunction.internalSwarmSize = this.internalSwarmSize;
            cloneFunction.inventorsRatio1 = this.inventorsRatio1;
            cloneFunction.inventorsRatio2 = this.inventorsRatio2;
            cloneFunction.neighbourProbability1 = this.neighbourProbability1;
            cloneFunction.neighbourProbability2 = this.neighbourProbability2;
            cloneFunction.optimizer = this.optimizer;
            cloneFunction.metaheuristic = this.metaheuristic;
            cloneFunction.routeSwarmIterations = this.routeSwarmIterations;
            cloneFunction.timeStep = this.timeStep;
            cloneFunction.traceInfo = this.traceInfo;
            cloneFunction.CurrentTime = this.currentTime;
            cloneFunction.weightPowerParameter = this.weightPowerParameter;
            cloneFunction.usePreviousSolutions = this.usePreviousSolutions;
            cloneFunction.retrievePreviousSolutionThroughDiscretization = this.retrievePreviousSolutionThroughDiscretization;
            cloneFunction.useDiscretizedPreviousSolution = this.useDiscretizedPreviousSolution;
            cloneFunction.shareServicesBestSolutions = this.shareServicesBestSolutions;
            cloneFunction.use2Opt = this.use2Opt;
            cloneFunction.discrete = this.discrete;
            cloneFunction.classesForVehicle = this.classesForVehicle;
            cloneFunction.reservoirVehiclesCount = this.reservoirVehiclesCount;
            cloneFunction.initialIterations = this.initialIterations;
            cloneFunction.initialPartOfTheDay = this.initialPartOfTheDay;
            cloneFunction.partialIterations = this.partialIterations;
            cloneFunction.generateDataFromModel = this.generateDataFromModel;
            cloneFunction.useUCB = this.useUCB;
            cloneFunction.rememberBandits = this.rememberBandits;
            cloneFunction.useGreedyInMC = this.useGreedyInMC;
            cloneFunction.distances = this.distances;
            cloneFunction.previousBestValue = this.previousBestValue;
            cloneFunction.bufferSize = this.bufferSize;
            cloneFunction.fullMulticlustering = this.fullMulticlustering;
            cloneFunction.timeLimit = this.timeLimit;
            cloneFunction.de_separateFForEachDim = this.de_separateFForEachDim;
            cloneFunction.de_minF = this.de_minF;
            cloneFunction.de_maxF = this.de_maxF;
            cloneFunction.de_crossProb = this.de_crossProb;
            cloneFunction.advancedCommitmentTimeSteps = this.advancedCommitmentTimeSteps;
            cloneFunction.scaleAdvancedCommitimentToPartOfTheDay = this.scaleAdvancedCommitimentToPartOfTheDay;
            cloneFunction.maxPSOSwarmNeighbourhoodSize = this.maxPSOSwarmNeighbourhoodSize;
            cloneFunction.applyDistanceHeuristic = this.applyDistanceHeuristic;
            cloneFunction.takeOnlyVehicleNumberEstimation = this.takeOnlyVehicleNumberEstimation;
            cloneFunction.penalizeInsufficentVehicles = this.penalizeInsufficentVehicles;
            cloneFunction.cutoffTime = this.cutoffTime;

            cloneFunction.previousEstimatedVehicles = this.previousEstimatedVehicles;
            cloneFunction.previousClientsToAssign = this.previousClientsToAssign;
            cloneFunction.importedSolutions.AddRange(this.importedSolutions);
            cloneFunction.exportedSolutions = this.exportedSolutions;

            cloneFunction.importedVehicleSolutions.AddRange(this.importedVehicleSolutions);
            cloneFunction.exportedVehicleSolutions = this.exportedVehicleSolutions;
            cloneFunction.lastClientsIds.AddRange(this.lastClientsIds);

            if (Results != null)
            {
                cloneFunction.Results = new Dictionary<ContinuousMetaheuristic, List<double>>();
                foreach (ContinuousMetaheuristic fpo in Results.Keys)
                {
                    cloneFunction.Results.Add(fpo, new List<double>());
                    cloneFunction.Results[fpo].AddRange(this.Results[fpo]);
                }
            }
            /*
            foreach (Vehicle vehicle in cloneFunction.Vehicles.Where(vhcl => vhcl.clientsToAssign.Count + vhcl.assignedClients.Count > 0))
            {
                vehicle.depot = cloneFunction.Depots[0];
                vehicle.earliestLeaveTime = (CurrentTime + timeStep) * (decimal)MaxTime;
                vehicle.distances = distances;
                vehicle.ignoreTimeBounds = (AvailableVehicles.Count == 0);
                double[] center = vehicle.Encode();
                vehicle.CommitVehicles(center, advancedCommitmentTimeSteps, timeStep, scaleAdvancedCommitimentToPartOfTheDay);
            }
            */
            cloneFunction.CurrentTime = this.currentTime;
            return cloneFunction;
        }

        public void SetSwarms(int size, int routeIterations, int clusterIterations)
        {
            this.internalSwarmIterations = clusterIterations;
            this.routeSwarmIterations = routeIterations;
            this.internalSwarmSize = size;
        }

        /// <summary>
        /// Zerowanie licznika czasu
        /// </summary>
        public void RestartTime()
        {
            currentTime = 0;
            foreach (Vehicle vehicle in Vehicles)
            {
                vehicle.assignedClients.Clear();
            }
        }

        public double Solve()
        {
            solvingStartTime = DateTime.Now;
            StartVehicles = new Vehicle[this.Vehicles.Length];
            //decimal rank = 0;
            for (int i = 0; i < this.Vehicles.Length; i++)
            {
                StartVehicles[i] = (Vehicle)Vehicles[i].Clone();
                //Try: attempt to reset ranks - maybe this will help restoring?
                /*   
                   foreach (Client client in StartVehicles[i].clientsToAssign)
                   {
                       if (!(client is Depot))
                       {
                           client.Rank = rank;
                           client.VisitTime = 0.0M;
                       }
                       rank += 1.0M / clientsToAssign.Count;
                   }
                   //Result: ranks did not help - the clients still get removed for some reason...
                   //Another try - resetting visit time?
                 * //Reason: start vehicles already missed some clients - reason uncertain - but removed commiting from cloning
                 */
            }

            double value = 0.0;
            bool firstRun = true;
            PrepareVehicles();
            DateTime start = DateTime.Now;

            while ((firstRun || (ClientRequestsCount != ClientsInVehiclesCount && AvailableVehicles.Count > 0)) && (DateTime.Now - start) < timeout)
            {
                if (!firstRun)
                    Console.WriteLine("Another attempt. Function: {3}, Time: {0}, Clients: {1}, Vehicles: {2}", currentTime, ClientsToAssign.Count, AvailableVehicles.Count, filename);
                OptimizeOperatingAreas();
                firstRun = false;
                value = OptimizeRoutes(false);
                //Commit();
                UpdateClientsToAssign();
                if (ClientsInVehiclesCount + clientsToAssign.Count != ClientRequestsCount)
                    throw new Exception("There is a leak in clients requests");
                var neighbourhood = 1;
                while (clientsToAssign.Count > 0 && currentTime <= cutoffTime)
                {
                    foreach (Client newClient in clientsToAssign.OrderBy(clnt => Utils.Instance.random.Next()))
                    {
                        List<Vehicle> consideredVehicles = AvailableVehicles.Where(
                            vhcl => vhcl.CountAssignedClients + vhcl.CountClientsToAssign > 0 && -vhcl.Need - newClient.Need <= vhcl.Capacity).ToList();
                        consideredVehicles.AddRange(AvailableVehicles.Where(
                            vhcl => vhcl.CountAssignedClients + vhcl.CountClientsToAssign == 0).Take(1));
                        int bestVehicleId = -1;
                        decimal bestRank = 0;
                        int bestVehicleNeed = int.MinValue;
                        double smallestIncrease = double.PositiveInfinity;
                        foreach (Vehicle vehicle in consideredVehicles)
                        {
                            int localBestVehicleId = -1;
                            double localSmallestIncrease = double.PositiveInfinity;
                            decimal localBestRank = 0.0M;
                            ((Vehicle)vehicle.Clone()).CheckPossibilityAndCostOfAssigningClientToVehicle(
                                newClient,
                                ref localBestVehicleId,
                                ref localBestRank,
                                ref localSmallestIncrease,
                                Depots,
                                (CurrentTime + timeStep) * (decimal)MaxTime,
                                distances,
                                ref bestVehicleNeed);
                            if (localSmallestIncrease < smallestIncrease)
                            {
                                smallestIncrease = localSmallestIncrease;
                                bestVehicleId = localBestVehicleId;
                                bestRank = localBestRank;
                            }
                        }
                        if (bestVehicleId != -1)
                        {
                            Vehicle chosenVehicle = Vehicles.First(vhcl => vhcl.Id == bestVehicleId);
                            chosenVehicle.clientsToAssign.Add(newClient);
                            newClient.Rank = bestRank;
                            newClient.FakeVehicleId = bestVehicleId;
                            value += smallestIncrease;
                            //TODO: should I commit here?
                        }
                        else
                        {
                            value = double.MaxValue;
                            break;
                        }
                    }
                    foreach (Vehicle vehicle in Vehicles)
                    {
                        vehicle.clientsToAssign.RemoveAll(clnt => clnt is Depot);
                    }
                    value = OptimizeRoutes(false);

                    //Commit();
                    UpdateClientsToAssign();
                    if (clientsToAssign.Any())
                    {
                        var vehiclesWithRemovableClients = Vehicles.Where(vhcl => vhcl.clientsToAssign.Any(clnt => !(clnt is Depot))).ToList();
                        if (vehiclesWithRemovableClients.Any())
                        {
                            var selectedVehicle = Utils.Instance.random.Next(vehiclesWithRemovableClients.Count);
                            var clientsInVehicle = vehiclesWithRemovableClients[selectedVehicle].clientsToAssign.Count(clnt => !(clnt is Depot));
                            for (int i = 0; i < neighbourhood / 10 && i < clientsInVehicle; ++i)
                            {
                                var clientToRemove = vehiclesWithRemovableClients[selectedVehicle].clientsToAssign.Last(clnt => !(clnt is Depot));
                                vehiclesWithRemovableClients[selectedVehicle].clientsToAssign.Remove(clientToRemove);
                            }
                            UpdateClientsToAssign();
                            ++neighbourhood;
                        }
                        else
                        {
                            break;
                        }
                        if (neighbourhood > 200)
                        {
                            break;
                        }
                    }

                }
                if (this.Clients.Count(clnt => (double)clnt.StartAvailable / MaxTime <= (double)currentTime) == previousStepClientsCount)
                {
                    //jezeli mamy tyle samo klientow co wczesniej to koniec obliczen
                    break;
                }
                break;
            }
            string additionalInfo = "";
            if (this.Clients.Count(clnt => (double)clnt.StartAvailable / MaxTime <= (double)currentTime) == previousStepClientsCount &&
                (value > previousBestValue || ClientsToAssign.Count > 0))
            {
                value = RestoreBetterSolution(StartVehicles);
                additionalInfo = " while restoring " +
                    +
                                        StartVehicles.Sum(vhcl => vhcl.assignedClients.Where(clnt => !(clnt is Depot)).Count() +
                        vhcl.clientsToAssign.Where(clnt => !(clnt is Depot)).Count()) +
                        " " +
                    Vehicles.Sum(vhcl => vhcl.assignedClients.Where(clnt => !(clnt is Depot)).Count() +
                        vhcl.clientsToAssign.Where(clnt => !(clnt is Depot)).Count()) +
                    " " + value
                    + " " + string.Join(",", clientsToAssign.Select(clnt => clnt.Id));
                Vehicle currentFaultyRoute = Vehicles.FirstOrDefault(vhcl2 => vhcl2.Unassigned != null && vhcl2.Unassigned.Any());
                if (currentFaultyRoute != null)
                {
                    Vehicle faultyRoute = StartVehicles.FirstOrDefault(vhcl => vhcl.Id == currentFaultyRoute.Id);
                    additionalInfo = string.Format(" while restoring {0}",
                        faultyRoute.ToString());
                    /*
                    //temporary repairment procedure
                    currentTime -= timeStep;
                    value = RestoreBetterSolution(StartVehicles);
                    currentTime += timeStep;
                    */
                }
            }
            else
            {
                previousBestValue = value;
            }
            if (ClientsToAssign.Count > 0)
            {
                throw new Exception("Not all the clients have been assigned" + additionalInfo);
            }
            previousStepClientsCount = this.Clients.Count(clnt => (double)clnt.StartAvailable / MaxTime <= (double)currentTime);
            return value;
        }

        private void UpdateClientsToAssign()
        {
            clientsToAssign = Clients.Where(
                clnt => clnt.StartAvailable <= CurrentTime * Depots.Max(dpt => dpt.EndAvailable) && !Vehicles.Any(vhcl => vhcl.assignedClients.Any(clnt2 => clnt2.Id == clnt.Id) || vhcl.clientsToAssign.Any(clnt2 => clnt2.Id == clnt.Id))
                ).ToList();
        }

        private void Commit()
        {
            foreach (Vehicle vehicle in Vehicles.Where(vhcl =>
                vhcl.assignedClients.Any() ||
                vhcl.clientsToAssign.Any()
                )
            )
            {
                /*
                Vehicle offendingVehicle = Vehicles.FirstOrDefault(vhcl =>
                vhcl.assignedClients.Any() &&
                vhcl.clientsToAssign.Any() &&
                vhcl.assignedClients.Last().VisitTime + vhcl.assignedClients.Last().TimeToUnload < (currentTime + 2 * timeStep) * (decimal)(int)MaxTime &&
                vhcl.FinishTime > (1 - timeStep * (1 + advancedCommitmentTimeSteps)) * (decimal)(int)MaxTime
                );
                */
                //throw new Exception("Not commited properly " + offendingVehicle);
                vehicle.ignoreTimeBounds = false;
                vehicle.Distances = distances;
                vehicle.Depot.VisitTime = vehicle.EarliestLeaveTime;
                vehicle.CommitVehicles(vehicle.Encode(), advancedCommitmentTimeSteps, timeStep, scaleAdvancedCommitimentToPartOfTheDay);
            }
        }

        private double RestoreBetterSolution(Vehicle[] StartVehicles)
        {
            double value = 0.0;
            for (int i = 0; i < Vehicles.Length; i++)
                this.Vehicles[i] = (Vehicle)StartVehicles[i].Clone();
            foreach (Vehicle vehicle in this.Vehicles.Where(vhcl => vhcl.clientsToAssign.Count + vhcl.assignedClients.Count > 0))
            {
                vehicle.Depot.VisitTime = vehicle.EarliestLeaveTime;
                vehicle.Distances = distances;
                vehicle.ignoreTimeBounds = (AvailableVehicles.Count == 0);

                double[] center = vehicle.Encode();
                /*
                if (use2Opt)
                    center = EnhanceWith2Opt(vehicle, center, Depots, distances);
                */
                value += vehicle.CommitVehicles(center, advancedCommitmentTimeSteps, timeStep, scaleAdvancedCommitimentToPartOfTheDay);
            }
            clientsToAssign = Clients.Where(
    clnt => clnt.StartAvailable <= CurrentTime * Depots.Max(dpt => dpt.EndAvailable) && Vehicles.Sum(vhcl => vhcl.assignedClients.Count(clnt2 => clnt2.Id == clnt.Id) + vhcl.clientsToAssign.Count(clnt2 => clnt2.Id == clnt.Id)) == 0
    ).ToList();
            return value;
        }

        public override string ToString()
        {
            return base.ToString() + new FileInfo(filename).Name;
        }

        /// <summary>
        /// Aktualizuje czas
        /// </summary>
        public void UpdateTime()
        {
            CurrentTime += timeStep;
        }
        private static double GetDoubleNumberFromLine(string line)
        {
            try
            {
                return double.Parse(line.Trim().Split(':')[1].Trim().Replace(".", ","));
            }
            catch
            {
                return double.Parse(line.Trim().Split(':')[1].Trim());
            }
        }

        private static int GetNumberFromLine(string line)
        {
            return int.Parse(line.Trim().Split(':')[1].Trim());
        }

        private static int[] GetNumbersFromLine(string line)
        {
            string[] data = line.Trim().Split(' ');
            int[] numbers = new int[data.Length];
            for (int i = 0; i < data.Length; ++i)
                numbers[i] = int.Parse(data[i].Trim());
            return numbers;
        }

        /// <summary>
        /// Poszukiwanie trasy dla pojedynczego pojazdu
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        private double GetOptimumRouteForVehicle(Vehicle vehicle, bool randomizeRoute)
        {
            vehicle.Distances = distances;
            vehicle.ignoreTimeBounds = (AvailableVehicles.Count == 0);

            double[] center = vehicle.Encode();
            //without historic knowledge start from a random permutation
            if (randomizeRoute)
            {
                center = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Select(clnt => Utils.Instance.random.NextDouble()).ToArray();
            }
            if (vehicle.Available || AvailableVehicles.Count == 0)
            {
                double radius = center.Length > 0 ? ((vehicle.clientsToAssign.Max(clnt => decimal.ToDouble(clnt.Rank)) - vehicle.clientsToAssign.Min(clnt => decimal.ToDouble(clnt.Rank)) + 0.5) * Math.Sqrt(center.Length)) : 1;
                if (!usePreviousSolutions)
                    radius = 2 * (center.Length > 0 ? Math.Sqrt(center.Length) : 1);
                var seedsDictionary = Optimizer.CreatePopulationSeedsFromLastAndHeuristic(internalSwarmSize, center, null, null);
                IOptimizer optimizer = null;
                if ((metaheuristic & (int)ContinuousMetaheuristic.PSO) > 0)
                {
                    optimizer = new PSOAlgorithm(
                        vehicle,
                        (int)(internalSwarmSize),
                        radius,
                        seedsDictionary,
                        neighbourProbability2,
                        maxPSOSwarmNeighbourhoodSize,
                        inventorsRatio2,
                        false);
                }
                else if ((metaheuristic & (int)ContinuousMetaheuristic.DE) > 0)
                {
                    optimizer = new DifferentialEvolution(
                        (int)(internalSwarmSize),
                        vehicle,
                        radius,
                        seedsDictionary);
                }
                else if ((metaheuristic & (int)ContinuousMetaheuristic.RandomClustering) > 0)
                {
                    optimizer = new RandomClustererVNS(
                        (int)(internalSwarmSize),
                        vehicle,
                        radius,
                        seedsDictionary);
                }
                else if ((metaheuristic & (int)ContinuousMetaheuristic.RandomWalk) > 0)
                {
                    optimizer = new RandomWalkVNS(
                        (int)(internalSwarmSize),
                        vehicle,
                        radius,
                        seedsDictionary);
                }
                for (int i = 0; i < routeSwarmIterations && center.Length > 0 && optimizer != null; i++)
                {
                    optimizer.Step();
                    EvaluationCount += internalSwarmSize;
                    if (traceInfo)
                    {
                        Utils.Instance.LogInfo("wydajnosc.csv", "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}", "vehicle" + vehicle.Id, currentTime, i, optimizer.BestValue, EvaluationCount, AvailableVehicles.Count, ClientsToAssign.Count);
                    }
                    if ((DateTime.Now - solvingStartTime).TotalSeconds > timeLimit)
                        break;

                    center = optimizer.Best;
                }
            }
            if (use2Opt)
                center = vehicle.EnhanceWith2Opt(center, Depots, distances);
            double best = vehicle.CommitVehicles(center, advancedCommitmentTimeSteps, timeStep, scaleAdvancedCommitimentToPartOfTheDay);
            return best;
        }

        private void OptimizeOperatingAreas()
        {
            var currentClientsIds = clientsToAssign.OrderBy(clnt => clnt.Id).Select(clnt => clnt.Id).ToList();
            IFunction fitnessFunction = null;
            IEnumerable<int> vehicleIds = null;
            double[] center = null, roughHeuristicSolution = null, discretizedCenter = null;
            int[] hansharSolution = null, khouadjiaSolution = null;
            int[] discreteCenter = null;
            double radius = 1;
            int discreteRadius = AvailableVehicles.Count / 2 + 1;
            if ((optimizer & (int)FirstPhaseOptimizer.TreeClustering) > 0 ||
                ((optimizer & (int)FirstPhaseOptimizer.TreesMonteCarlo) > 0 && currentTime >= cutoffTime))
            {
                InitializeBySpanningTree(out fitnessFunction, out vehicleIds, out roughHeuristicSolution, out hansharSolution, out khouadjiaSolution);
            }
            else
            {
                InitializeByARandomGreedy(out fitnessFunction, out vehicleIds, out roughHeuristicSolution, out hansharSolution, out khouadjiaSolution);
            }

            if (takeOnlyVehicleNumberEstimation)
            {
                roughHeuristicSolution = null;
                hansharSolution = null;
            }

            if ((optimizer & (int)FirstPhaseOptimizer.ClientAssignment) > 0)
            {
                InitializeForClientAssignment(out fitnessFunction, out center, out discreteCenter);
                if (discrete)
                {
                    PerformDiscreteClientAssignmentOptimization(fitnessFunction, discreteCenter, discreteRadius);
                }
                else
                {
                    PerformContinuousClientAssignmentOptimization(fitnessFunction, center, radius);
                }
            }
            else if (((optimizer & (int)FirstPhaseOptimizer.VehicleClustering) > 0))
            {
                InitializeForVehicleClustering(out fitnessFunction, out center, out radius, vehicleIds);
                PerformContinousClusteringOptimization(fitnessFunction, center, radius, discretizedCenter, roughHeuristicSolution);
            }
            else if ((optimizer & (int)FirstPhaseOptimizer.Partitioning) > 0)
            {
                InitializeForPartitioning(out fitnessFunction, out center, out radius);
                PerformContinousClusteringOptimization(fitnessFunction, center, radius, discretizedCenter, roughHeuristicSolution);
            }
            else if ((optimizer & (int)FirstPhaseOptimizer.HansharGA) > 0)
            {
                List<int[]> population;
                InitializePopulationForHansharAlgorithm(hansharSolution, out population);
                PerformHansharOptimization(out fitnessFunction, ref population);
            }
            else if ((optimizer & (int)FirstPhaseOptimizer.KhouadjiaEncoding) > 0)
            {
                List<int[]> greedySolutions = new List<int[]>();
                /*
                while (this.importedSolutions.Count + greedySolutions.Count + 1 < internalSwarmSize)
                {
                    int[] aaa;
                    InitializeByARandomGreedy(out fitnessFunction, out vehicleIds, out roughHeuristicSolution, out hansharSolution, out aaa);
                    greedySolutions.Add(aaa);
                }
                */
                var vehicleIdsList = vehicleIds.ToList();
                var function = new KhouadjiaFunction(
                    Vehicles, clientsToAssign.ToArray(), Depots, (CurrentTime + timeStep) * (decimal)MaxTime, distances, this.weightPowerParameter,
                    this.classesForVehicle, this.fullMulticlustering, this.penalizeInsufficentVehicles, this.cutoffTime, this.bufferSize);
                var filledInPreviousSolutions = KhouadjiaFunction.AdaptSolutions(this.lastClientsIds, clientsToAssign, this.importedVehicleSolutions);
                var adaptedSolutions = new List<int[]>();
                adaptedSolutions.Add(khouadjiaSolution);
                var bestValue = function.Value(khouadjiaSolution);
                var bestIndex = adaptedSolutions.Count - 1;
                foreach (var filledInSolution in filledInPreviousSolutions)
                {
                    var testValue = function.Value(filledInSolution);
                    int[] adaptedSolution = KhouadjiaFunction.GetKhouadjiaEncoding(function.ClientsToAssign);
                    adaptedSolutions.Add(adaptedSolution);
                    if (testValue < bestValue)
                    {
                        bestValue = testValue;
                        bestIndex = adaptedSolutions.Count - 1;
                    }
                }
                foreach (var greedy in greedySolutions)
                {
                    var testValue = function.Value(greedy);
                    adaptedSolutions.Add(greedy);
                    if (testValue < bestValue)
                    {
                        bestValue = testValue;
                        bestIndex = adaptedSolutions.Count - 1;
                    }
                }
                /*
                var save = adaptedSolutions[bestIndex];
                adaptedSolutions.Clear();
                adaptedSolutions.Add(save);
                bestIndex = 0;
                */
                if (khouadjiaSolution.Length > 0)
                {
                    int tries = 0;
                    int lambda = 1;
                    for (int i = 0; i < internalSwarmIterations * internalSwarmSize; i++)
                    {
                        var testPoint = new int[khouadjiaSolution.Length];
                        var chosen = Utils.Instance.random.Next(adaptedSolutions.Count);
                        Array.Copy(adaptedSolutions[chosen], testPoint, testPoint.Length);
                        for (int l = 0; l < lambda; l++)
                        {
                            var choice = Utils.Instance.random.Next(testPoint.Length);
                            int newVehicle = -1;
                            while ((newVehicle = vehicleIdsList[Utils.Instance.random.Next(vehicleIdsList.Count)]) == testPoint[choice]) ;
                            testPoint[choice] = newVehicle;
                        }
                        var testValue = function.Value(testPoint);
                        tries++;
                        if (testValue < bestValue)
                        {
                            tries = 0;
                            bestValue = testValue;
                            Array.Copy(testPoint, adaptedSolutions[chosen], testPoint.Length);
                            bestIndex = chosen;
                        }
                        if (tries > 100 * lambda)
                        {
                            tries = 0;
                            lambda++;
                        }
                    }
                    this.exportedVehicleSolutions = new int[adaptedSolutions[bestIndex].Length];
                    Array.Copy(adaptedSolutions[bestIndex], this.exportedVehicleSolutions, this.exportedVehicleSolutions.Length);
                    this.importedVehicleSolutions.Clear();

                }
            }
            else if (
                ((optimizer & (int)FirstPhaseOptimizer.PSOMonteCarlo) > 0 && currentTime >= cutoffTime)
                || ((optimizer & (int)FirstPhaseOptimizer.Dapso) > 0  
                 || (optimizer & (int)FirstPhaseOptimizer.AiKachitvichyanukul) > 0
                 )
                && ((optimizer & (int)FirstPhaseOptimizer.TreesMonteCarlo) == 0 || CurrentTime >= cutoffTime))
            {
                InitializeForDAPSOClustering(out fitnessFunction, out center, out discretizedCenter, out radius, vehicleIds);
                PerformContinousClusteringOptimization(fitnessFunction, center, radius, discretizedCenter, roughHeuristicSolution);
            }
            else if ((optimizer & (int)FirstPhaseOptimizer.MonteCarlo) > 0)
            {
                MonteCarloSearcher monteCarloSearcher = new MonteCarloSearcher(this, out fitnessFunction, initialPartOfTheDay, initialIterations, partialIterations, generateDataFromModel, useUCB, useGreedyInMC, (listOfBandits == null || listOfBandits.Count == 0) ? null : listOfBandits);
                bandits = (rememberBandits/* || CurrentTime > cutoffTime*/) ? monteCarloSearcher.States : null;
            }
            else if ((optimizer & (int)FirstPhaseOptimizer.TreesMonteCarlo) > 0 && CurrentTime < cutoffTime)
            {
                MonteCarloWithSolutionProposals monteCarlo = new MonteCarloWithTree2OPTSolutionProposals(this, initialIterations, this.partialIterations, this.applyDistanceHeuristic, false, cutoffTime, generateDataFromModel, bufferSize);
                fitnessFunction = monteCarlo.Solve();
            }
            else if ((optimizer & (int)FirstPhaseOptimizer.PSOMonteCarlo) > 0 && CurrentTime < cutoffTime)
            {
                MonteCarloWithSolutionProposals monteCarlo = new MonteCarloWithPSOSolutionProposals(this, initialIterations, this.partialIterations, this.internalSwarmSize, this.internalSwarmIterations, this.fullMulticlustering, false, cutoffTime, generateDataFromModel, bufferSize);
                fitnessFunction = monteCarlo.Solve();
            }

            //Utils.LogInfo("log.txt", "[{2:yyyy-MM-dd HH:mm:ss}];{3};{0:0.00};{1:0.00};estimation", currentTime, pso.BestValue, DateTime.Now, new FileInfo(filename).Name);
            for (int i = 0; i < Vehicles.Length; i++)
            {
                Vehicle vehicle = (fitnessFunction as ClusterFunction).Vehicles.FirstOrDefault(vhcl => vhcl.Id == this.Vehicles[i].Id);
                if (vehicle != null)
                    this.Vehicles[i] = (Vehicle)vehicle.Clone();
            }
            for (int i = 0; i < clientsToAssign.Count; i++)
                this.clientsToAssign[i] = (Client)(fitnessFunction as ClusterFunction).ClientsToAssign[i].Clone();
            this.lastClientsIds = currentClientsIds;
        }

        private void PerformHansharOptimization(out IFunction clusterFunction, ref List<int[]> population)
        {
            HansharFunction hansharFunction = new HansharFunction(
                Vehicles,
                clientsToAssign.ToArray(),
                Depots,
                (CurrentTime + timeStep) * (decimal)MaxTime,
                distances,
                this.weightPowerParameter,
                this.classesForVehicle,
                fullMulticlustering,
                this.penalizeInsufficentVehicles,
                this.cutoffTime,
                this.bufferSize
            );
            List<double> values = new List<double>();
            List<List<int[]>> vehicleStructures = new List<List<int[]>>();
            foreach (var specimen in population)
            {
                List<int[]> vehicleStructure;
                values.Add(hansharFunction.Value(specimen, out vehicleStructure));
                vehicleStructures.Add(vehicleStructure);
            }
            for (int step = 0; step < internalSwarmIterations; ++step)
            {

                OneStepOfHansharGA(ref population, hansharFunction, ref values, ref vehicleStructures);
                if ((DateTime.Now - solvingStartTime).TotalSeconds / internalSwarmIterations * (internalSwarmIterations + routeSwarmIterations) > timeLimit)
                    break;
                //Console.WriteLine(hansharFunction.Value(pass[0]));
            }
            clusterFunction = hansharFunction;
        }

        private void OneStepOfHansharGA(ref List<int[]> population, HansharFunction hansharFunction, ref List<double> values, ref List<List<int[]>> vehicleStructures)
        {
            var orderedValues = values.Select((x, i) => new KeyValuePair<int, double>(i, x))
                .OrderBy(x => x.Value)
                .ToList();
            var populationCount = population.Count;
            var pass = population
                .Where((pop, idx) => orderedValues.Take(Math.Max(1, populationCount / 10))
                    .Select(x => x.Key)
                    .Contains(idx))
                    .ToList();
            var passValues = orderedValues.Take(Math.Max(1, population.Count / 10))
                .Select(x => x.Value)
                .ToList();
            var passVehicleStructures = vehicleStructures.Where((strct,idx) => orderedValues.Take(Math.Max(1, populationCount / 10))
                    .Select(x => x.Key)
                    .Contains(idx))
                    .ToList();

            List<int[]> offspring = new List<int[]>();
            for (int i = 0; i < population.Count / 2; i++)
            {
                var par1choice = Utils.Instance.random.Next(population.Count);
                var par2choice = Utils.Instance.random.Next(population.Count);
                var parent1 = population[par1choice];
                var parent2 = population[par2choice];
                var choice1 = vehicleStructures[par1choice][Utils.Instance.random.Next(vehicleStructures[par1choice].Count)];
                var choice2 = vehicleStructures[par2choice][Utils.Instance.random.Next(vehicleStructures[par2choice].Count)];
                offspring.Add(HansharFunction.GetHansharEncoding(hansharFunction.ReassignClients(parent1, choice2)));
                offspring.Add(HansharFunction.GetHansharEncoding(hansharFunction.ReassignClients(parent2, choice1)));
            }
            for (int i = 0; i < offspring.Count; ++i)
            {
                if (Utils.Instance.random.NextDouble() < 0.15)
                {
                    var cut1 = Utils.Instance.random.Next(offspring[i].Length);
                    var cut2 = Utils.Instance.random.Next(offspring[i].Length);
                    var minCut = Math.Min(cut1, cut2);
                    var maxCut = Math.Min(cut1, cut2);
                    offspring[i] = offspring[i]
                        .Take(minCut)
                        .Concat(offspring[i].Skip(minCut).Take(maxCut - minCut).Reverse())
                        .Concat(offspring[i].Skip(maxCut))
                        .ToArray();
                }
            }
            values.Clear();
            vehicleStructures.Clear();
            foreach (var specimen in offspring)
            {
                List<int[]> vehicleStructure;
                values.Add(hansharFunction.Value(specimen, out vehicleStructure));
                vehicleStructures.Add(vehicleStructure);
            }

            while (pass.Count < internalSwarmSize)
            {
                var spec1 = Utils.Instance.random.Next(offspring.Count);
                var spec2 = Utils.Instance.random.Next(offspring.Count);
                var choice = Utils.Instance.random.NextDouble();
                if (choice < 0.8)
                {
                    if (values[spec1] < values[spec2]) { pass.Add(offspring[spec1]); passValues.Add(values[spec1]); passVehicleStructures.Add(vehicleStructures[spec1]); }
                    else { pass.Add(offspring[spec2]); passValues.Add(values[spec2]); passVehicleStructures.Add(vehicleStructures[spec2]); }
                }
                else
                {
                    if (values[spec1] < values[spec2]) { pass.Add(offspring[spec2]); passValues.Add(values[spec2]); passVehicleStructures.Add(vehicleStructures[spec2]); }
                    else { pass.Add(offspring[spec1]); passValues.Add(values[spec1]); passVehicleStructures.Add(vehicleStructures[spec1]); }
                }
            }
            population = pass;
            values = passValues;
            vehicleStructures = passVehicleStructures;
        }

        private void InitializePopulationForHansharAlgorithm(int[] hansharEncoded, out List<int[]> population)
        {
            int[] previous = HansharFunction.GetHansharEncoding(StartVehicles);
            int[] heuristic = (int[])previous.Clone();
            if (hansharEncoded != null)
            {
                heuristic = hansharEncoded;
            }
            population = new List<int[]> { previous, heuristic };
#warning maybe store the population and use greedy insertion of new tasks?
            for (int s = 0; s < internalSwarmSize - 2; s++)
            {
                population.Add((Vehicles
                .Where(vhcl => vhcl.assignedClients.Any())
                .Select(vhcl => -vhcl.Id)
                        .Concat(clientsToAssign
                        .Where(clnt => !(clnt is Depot))
                        .Select(clnt => clnt.Id)))
                        .OrderBy(clnt => Utils.Instance.random.Next())
                        .ToArray());
            }
        }

        /*
        internal static double GetRouteLength(Vehicle vehicle, Depot[] depots, decimal currentProblemTime, double[,] distances)
        {
            VehicleRouteFunction vehicleRouteFunction = new VehicleRouteFunction(vehicle, depots, currentProblemTime, false, distances);
            decimal[] xRank = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Select(clnt => clnt.Rank).ToArray();
            double newValue = vehicleRouteFunction.Value(xRank.Select(xx => decimal.ToDouble(xx)).ToArray());
            if (vehicleRouteFunction.Unassigned.Count > 0)
                return double.NaN;
            vehicle.clientsToAssign = vehicleRouteFunction.Route.Where(clnt => !(clnt is Depot)).ToList();
            return newValue;
        }
        */

        internal double PerformContinousClusteringOptimization(IFunction clusterFunction, double[] center, double radius, double[] discretizedCenter, double[] heuristicSolution)
        {
            ParticleSwarmOptimization.IOptimizer optimizationAlgorithm = null;
            Dictionary<ContinuousMetaheuristic, double> threshold = CreateAlgorithmsRoulette();
            double choice = Utils.Instance.random.NextDouble();
            var seedsList = new List<double[]>()
            {
                center,
                useDiscretizedPreviousSolution && usePreviousSolutions ?  discretizedCenter: null,
                heuristicSolution
            };
            if (shareServicesBestSolutions && usePreviousSolutions)
            {
                var maskedImported = IntegrateImports(center);
                seedsList.AddRange(maskedImported);
            }
            var seedsDictionary = Optimizer.CreatePopulationSeedsFromList(internalSwarmSize, seedsList);
            if ((metaheuristic & (int)ContinuousMetaheuristic.DE) > 0 && choice < threshold[ContinuousMetaheuristic.DE])
            {
                optimizationAlgorithm = new DifferentialEvolution(internalSwarmSize, clusterFunction, radius, seedsDictionary,
                    de_maxF, de_minF, de_crossProb, de_separateFForEachDim);
                ChosenAlgorithm = ContinuousMetaheuristic.DE;
            }
            else
            {
                optimizationAlgorithm = new PSOAlgorithm(
                    clusterFunction,
                    internalSwarmSize,
                    radius,
                    seedsDictionary,
                    neighbourProbability1,
                    maxPSOSwarmNeighbourhoodSize,
                    inventorsRatio1);
                ChosenAlgorithm = ContinuousMetaheuristic.PSO;
            }
            long signature = DateTime.Now.Ticks;
            for (int i = 0; clusterFunction.Dimension > 0 && i < internalSwarmIterations; i++)
            {
                optimizationAlgorithm.Step();
                EvaluationCount += internalSwarmSize;
                if (traceInfo)
                {
                    Utils.Instance.LogInfo("wydajnosc.csv", "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}", "estimation", currentTime, i, optimizationAlgorithm.BestValue, EvaluationCount, AvailableVehicles.Count, ClientsToAssign.Count);
                }
                //File.AppendAllLines(string.Format("{0}-{1}-{2}.csv", filename,
                //    System.Diagnostics.Process.GetCurrentProcess().Id,
                //    signature,
                //    System.Threading.Thread.CurrentThread.ManagedThreadId), new string[] {
                //        string.Format("{0}\t{1}\t{2}",currentTime,i, optimizationAlgorithm.BestValue)
                //});
                if ((DateTime.Now - solvingStartTime).TotalSeconds / internalSwarmIterations * (internalSwarmIterations + routeSwarmIterations) > timeLimit)
                    break;
            }
            this.exportedSolutions = new double[optimizationAlgorithm.Best.Length];
            Array.Copy(optimizationAlgorithm.Best, this.exportedSolutions, this.exportedSolutions.Length);
            this.importedSolutions.Clear();
            this.previousClientsToAssign = clientsToAssign.Count();
            return clusterFunction.Value(optimizationAlgorithm.Best);
        }

        private List<double[]> IntegrateImports(double[] center)
        {
            int maxCount = 0;
            double[] importedMatch = new double[0];
            foreach (var importedSolution in importedSolutions)
            {
                int count = center.Sum(val => importedSolution.Contains(val) ? 1 : 0);
                if (count > maxCount)
                {
                    maxCount = count;
                    importedMatch = importedSolution;
                    if (count == Math.Min(importedMatch.Length, center.Length))
                        break;
                }
            }
            int[] bestMatch = center.Select(val => importedMatch.ToList().IndexOf(val)).ToArray();
            var maskedImported = new List<double[]>();
            var currentClients = clientsToAssign.OrderBy(clnt => clnt.Id).ToList();
            foreach (var importedSolution in importedSolutions)
            {
                var masked = new double[center.Length];
                for (int i = 0; i < masked.Length; i++)
                {
                    if ((optimizer & (int)FirstPhaseOptimizer.AiKachitvichyanukul) > 0 && i < clientsToAssign.Count)
                    {
                        masked[i] = lastClientsIds.IndexOf(currentClients[i].Id) > -1 ? importedSolution[lastClientsIds.IndexOf(currentClients[i].Id)] : 10 * Utils.Instance.random.NextDouble() - 5.0;
                    }
                    else
                    {
                        masked[i] = bestMatch[i] > -1 && bestMatch[i] < importedSolution.Length ? importedSolution[bestMatch[i]] : (i % 2 == 0 ? Depots[0].X : Depots[0].Y + Utils.Instance.random.NextDouble() - 0.5);
                    }
                }
                maskedImported.Add(masked);
            }
            return maskedImported;
        }

        private Dictionary<ContinuousMetaheuristic, double> CreateAlgorithmsRoulette()
        {
            int[] algorithmsArray =
            new int[] {
                (int)ContinuousMetaheuristic.DE,
                (int)ContinuousMetaheuristic.RandomClustering,
                (int)ContinuousMetaheuristic.RandomWalk,
                (int)ContinuousMetaheuristic.GridLog,
                (int)ContinuousMetaheuristic.PSO,
                };
            Dictionary<ContinuousMetaheuristic, double> threshold = new Dictionary<ContinuousMetaheuristic, double>();
            if (Results == null)
            {
                double options = 0.0;
                foreach (int algorithm in algorithmsArray)
                {
                    if ((metaheuristic & algorithm) > 0)
                    {
                        ++options;
                    }
                }
                double accumulator = 0.0;
                foreach (int algorithm in algorithmsArray)
                {
                    if ((metaheuristic & algorithm) > 0)
                    {
                        accumulator += 1 / options;
                        threshold.Add((ContinuousMetaheuristic)algorithm, accumulator);
                    }
                }
            }
            else
            {
                foreach (int algorithm in algorithmsArray)
                {
                    if ((metaheuristic & algorithm) > 0)
                    {
                        threshold.Add((ContinuousMetaheuristic)algorithm, 0);
                    }
                }
                double accumulator = 0.0;
                foreach (ContinuousMetaheuristic fpo in threshold.Keys.ToList())
                {
                    double max = Results.Max(pair => pair.Value.Average()) * 1.01;
                    if (Results.ContainsKey(fpo))
                    {
                        accumulator += (max - Results[fpo].Average()) / Results.Sum(pair => (max - pair.Value.Average()));
                        threshold[fpo] = accumulator;
                    }
                }
            }
            return threshold;
        }

        internal double PerformContinuousClientAssignmentOptimization(IFunction clusterFunction, double[] center, double radius)
        {
            var seedsDictionary = Optimizer.CreatePopulationSeedsFromLastAndHeuristic(internalSwarmSize, center, null, null);
            ParticleSwarmOptimization.PSOAlgorithm pso = new ParticleSwarmOptimization.PSOAlgorithm(
                clusterFunction as ParticleSwarmOptimization.IFunction,
                optimizer != (int)FirstPhaseOptimizer.TreeClustering ? internalSwarmSize : 1,
                radius,
                seedsDictionary,
                neighbourProbability1,
                maxPSOSwarmNeighbourhoodSize,
                inventorsRatio1,
                true);
            for (int i = 0; clusterFunction.Dimension > 0 && i < internalSwarmIterations; i++)
            {
                pso.Step();
                EvaluationCount += internalSwarmSize;
            }
            (clusterFunction as ParticleSwarmOptimization.IFunction).Value(center);
            return (clusterFunction as ParticleSwarmOptimization.IFunction).Value(pso.Best);
            //Console.WriteLine(pso.BestValue);
        }

        internal double PerformDiscreteClientAssignmentOptimization(IFunction clusterFunction, int[] discreteCenter, int discreteRadius)
        {
            if (DebugMode)
            {
                Console.Title = "0";
            }
            DiscreteParticleSwarmOptimization.PSOAlgorithm pso = new DiscreteParticleSwarmOptimization.PSOAlgorithm(clusterFunction as DiscreteParticleSwarmOptimization.IFunction, optimizer != (int)FirstPhaseOptimizer.TreeClustering ? internalSwarmSize : 1, discreteCenter, discreteRadius, neighbourProbability1, inventorsRatio1);
            for (int i = 0; (clusterFunction as DiscreteParticleSwarmOptimization.IFunction).DiscreteDimension > 0 && i < internalSwarmIterations; i++)
            {
                pso.Step();
                EvaluationCount += internalSwarmSize;
                if (DebugMode)
                {
                    lock (loggerLocker)
                    {
                        Console.Title = Math.Max(i, int.Parse(Console.Title)).ToString();
                    }
                }
            }
            (clusterFunction as DiscreteParticleSwarmOptimization.IFunction).Value(discreteCenter);
            return (clusterFunction as DiscreteParticleSwarmOptimization.IFunction).Value(pso.Best);
            //Console.WriteLine(pso.BestValue);
        }

        internal void InitializeForDAPSOClustering(out IFunction clusterFunction, out double[] center, out double[] discretizedCenter, out double radius, IEnumerable<int> ids)
        {
            if ((optimizer & (int)FirstPhaseOptimizer.AiKachitvichyanukul) > 0)
            {
                clusterFunction = new AiKachitvichyanukulContinuousFunction(
                    Vehicles.Where(vhcl => ids.Contains(vhcl.Id)).ToArray(),
                    clientsToAssign.ToArray(),
                    Depots,
                    (CurrentTime + timeStep) * (decimal)MaxTime,
                    distances,
                    this.weightPowerParameter,
                    this.classesForVehicle,
                    fullMulticlustering,
                    this.penalizeInsufficentVehicles,
                    this.cutoffTime,
                    this.bufferSize
                );
            }
            else
            {
                clusterFunction = new DapsoClusterFunction(
                    Vehicles.Where(vhcl => ids.Contains(vhcl.Id)).ToArray(),
                    clientsToAssign.ToArray(),
                    Depots,
                    (CurrentTime + timeStep) * (decimal)MaxTime,
                    distances,
                    this.weightPowerParameter,
                    this.classesForVehicle,
                    fullMulticlustering,
                    this.penalizeInsufficentVehicles,
                    this.cutoffTime,
                    this.bufferSize
                );
            }
            radius = GetRadius(clientsToAssign, AvailableVehicles.Count);
            center = CreateCodingVectorFromModel((clusterFunction as ClusterFunction).Vehicles, clientsToAssign,!usePreviousSolutions, radius);
            discretizedCenter = CreateCodingVectorFromDiscretizedModel(
                StartVehicles.Where(vhcl => ids.Contains(vhcl.Id)),
                clientsToAssign);
        }

        private double[] CreateCodingVectorFromModel(IEnumerable<Vehicle> vehicles, IEnumerable<Client> clients, bool randomize = false, double radius = 0.0)
        {
            double[] center;
            if ((optimizer & (int)FirstPhaseOptimizer.AiKachitvichyanukul) > 0)
            {
                center = AiKachitvichyanukulContinuousFunction.GetClientRanks(clients);
            }
            else
            {
                center = new double[0];
            }
            center = center.Concat(ClusterFunction.GetFlatCentersVector(vehicles, this.classesForVehicle, randomize, radius)).ToArray();
            return center;
        }

        private double[] CreateCodingVectorFromDiscretizedModel(IEnumerable<Vehicle> vehicles, IEnumerable<Client> clients)
        {
            double[] center;
            if ((optimizer & (int)FirstPhaseOptimizer.AiKachitvichyanukul) > 0)
            {
                center = AiKachitvichyanukulContinuousFunction.GetClientRanks(clients);
            }
            else
            {
                center = new double[0];
            }
            center = center.Concat(ClusterFunction.GetFlatCentersVectorFromDiscrete(vehicles, this.classesForVehicle)).ToArray();
            return center;
        }

        internal void InitializeForPartitioning(out IFunction clusterFunction, out double[] center, out double radius)
        {
            clusterFunction = new PartitionFunction(Vehicles, clientsToAssign.ToArray(), Depots,
                (CurrentTime + timeStep) * (decimal)MaxTime, distances, this.weightPowerParameter, this.classesForVehicle,
                this.fullMulticlustering, this.penalizeInsufficentVehicles, this.cutoffTime, this.bufferSize);
            var vehicles = (clusterFunction as ClusterFunction).Vehicles;
            radius = GetRadius(clientsToAssign, AvailableVehicles.Count);
            center = ClusterFunction.GetFlatCentersVector(vehicles, this.classesForVehicle, !usePreviousSolutions, radius);
        }

        internal void InitializeForVehicleClustering(out IFunction clusterFunction, out double[] center, out double radius, IEnumerable<int> ids)
        {
            if (ids == null)
                ids = Vehicles.Select(vhcl => vhcl.Id);
            clusterFunction = new ClusterFunction(
                Vehicles.Where(vhcl => ids.Contains(vhcl.Id)).ToArray(),
                clientsToAssign.ToArray(),
                Depots,
                (CurrentTime + timeStep) * (decimal)MaxTime,
                distances,
                this.weightPowerParameter,
                this.classesForVehicle,
                this.fullMulticlustering,
                this.penalizeInsufficentVehicles,
                this.cutoffTime,
                this.bufferSize);
            var vehicles = (clusterFunction as ClusterFunction).Vehicles;
            radius = GetRadius(clientsToAssign, AvailableVehicles.Count);
            center = ClusterFunction.GetFlatCentersVector(vehicles, this.classesForVehicle, !usePreviousSolutions, radius);
        }

        private static double GetRadius(List<Client> clientSet, int multiplier)
        {
            double radius;
            if (clientSet.Count > 1)
                radius = (Math.Max(clientSet.Max(clnt => clnt.X) - clientSet.Min(clnt => clnt.X),
            clientSet.Max(clnt => clnt.Y) - clientSet.Min(clnt => clnt.Y))) * Math.Sqrt(multiplier);
            else
                radius = 1.0;
            return radius;
        }

        internal void InitializeForClientAssignment(out IFunction clusterFunction, out double[] center, out int[] discreteCenter)
        {
            clusterFunction = new ClientAssignmentFunction(
                Vehicles,
                clientsToAssign.ToArray(),
                Depots,
                (CurrentTime + timeStep) * (decimal)MaxTime,
                distances,
                weightPowerParameter,
                classesForVehicle,
                fullMulticlustering,
                this.penalizeInsufficentVehicles,
                this.cutoffTime,
                this.bufferSize
                );
            center = ClientsToAssign.OrderBy(clnt => clnt.Id).SelectMany(clnt => clnt.positionForClientAssignment).ToArray();
            discreteCenter = ClientsToAssign.OrderBy(clnt => clnt.Id).Select(clnt => clnt.FakeVehicleId - Vehicles.Min(vhcl => vhcl.Id)).ToArray();
            //Console.WriteLine((clusterFunction as DiscreteParticleSwarmOptimization.IFunction).Value(discreteCenter));
            //                Console.WriteLine(string.Join(",", Clients.Select(clnt => string.Format("{0:000}:{1:00}",clnt.Id,clnt.FakeVehicleId-Vehicles.Length))));
        }

        internal void InitializeByInsertionChain(out int[] discreteCenter)
        {
            int counter = 1;
            if (AvailableVehicles.Count == Vehicles.Length)
            {
                for (int i = 0; i < Vehicles.Length; ++i)
                {
                    Vehicles[i] = (Vehicle)StartVehicles[i].Clone();
                }
            }
            foreach (Client newClient in clientsToAssign.Where(clnt => clnt.StartAvailable > PreviousTime * (decimal)MaxTime).OrderBy(a => Utils.Instance.random.NextDouble()))
            {
                int bestVehicleId = -1;
                decimal bestRank = 0;
                double smallestIncrease = double.PositiveInfinity;
                foreach (Vehicle vehicle in AvailableVehicles)
                {
                    if (-vehicle.Need - newClient.Need > vehicle.Capacity)
                        continue;
                    //Compute initial route length (before adding client)
                    double originalValue = vehicle.GetRouteLength(Depots, (CurrentTime + timeStep) * (decimal)MaxTime, distances);
                    decimal[] ranks = vehicle.GetSortedRanks();
                    vehicle.clientsToAssign.Add(newClient);
                    newClient.FakeVehicleId = vehicle.Id;
                    for (int i = 0; i < ranks.Length + 1; ++i)
                    {
                        if (ranks.Length == 0)
                        {
                            newClient.Rank = 0.5M;
                        }
                        else if (i == 0)
                        {
                            newClient.Rank = (decimal)ranks[0] - 1M;
                        }
                        else if (i == ranks.Length)
                        {
                            newClient.Rank = ranks[ranks.Length - 1] + 1M;
                        }
                        else
                        {
                            newClient.Rank = (ranks[i - 1] + ranks[i]) / 2M;
                        }
                        //TODO: This is very non-optimal
                        double newValue = vehicle.GetRouteLength(Depots, (CurrentTime + timeStep) * (decimal)MaxTime, distances);
                        if (newValue - originalValue < smallestIncrease || ((newValue - originalValue <= smallestIncrease) && -vehicle.Need < -Vehicles.First(vhcl => vhcl.Id == bestVehicleId).Need))
                        {
                            smallestIncrease = newValue - originalValue;
                            bestRank = newClient.Rank;
                            bestVehicleId = newClient.FakeVehicleId;
                        }
                    }
                    vehicle.clientsToAssign.Remove(newClient);
                }
                Vehicle chosenVehicle = Vehicles.First(vhcl => vhcl.Id == bestVehicleId);
                chosenVehicle.clientsToAssign.Add(newClient);
                newClient.Rank = bestRank;
                newClient.FakeVehicleId = bestVehicleId;
                chosenVehicle.GetRouteLength(Depots, (CurrentTime + timeStep) * (decimal)MaxTime, distances);
                if (DebugMode)
                {
                    lock (loggerLocker)
                    {
                        System.Drawing.Image img = new System.Drawing.Bitmap(1024, 768);
                        DynamicVehicleRouting.Visualize.Visualizer.DrawVehicleAssignment(this, System.Drawing.Graphics.FromImage(img));
                        img.Save(string.Format("{0}.{1}-{3:000}.{2}", new FileInfo(filename).Name, this.CurrentTime, "png", counter++), System.Drawing.Imaging.ImageFormat.Png);
                    }
                }
            }
            if (AvailableVehicles.Count == Vehicles.Length)
            {
                PrepareVehicles();
            }
            else
            {
                foreach (Vehicle vehicle in AvailableVehicles)
                {
                    foreach (Client client in ClientsToAssign)
                    {
                        vehicle.clientsToAssign.Remove(client);
                    }
                }
            }
            int minVehicleId = Vehicles.Min(vhcl => vhcl.Id);
            discreteCenter = clientsToAssign.Select(clnt => clnt.FakeVehicleId - minVehicleId).ToArray();
        }

        internal void InitializeByARandomGreedy(out ParticleSwarmOptimization.IFunction clusterFunction, out IEnumerable<int> ids, out double[] roughHeuristicSolution, out int[] hansharSolution, out int[] khouadjiaSolution)
        {
            hansharSolution = (Vehicles
                .Where(vhcl => vhcl.assignedClients.Any())
                .Select(vhcl => -vhcl.Id)
                        .Concat(clientsToAssign
                        .Where(clnt => !(clnt is Depot))
                        .Select(clnt => clnt.Id)))
                        .OrderBy(clnt => Utils.Instance.random.Next())
                        .ToArray();
            clusterFunction = new HansharFunction(
                Vehicles,
                clientsToAssign.ToArray(),
                Depots,
                (CurrentTime + timeStep) * (decimal)MaxTime,
                distances,
                this.weightPowerParameter,
                this.classesForVehicle,
                fullMulticlustering,
                this.penalizeInsufficentVehicles,
                this.cutoffTime,
                this.bufferSize);
            (clusterFunction as HansharFunction).Value(hansharSolution);
            var importantVehicles = (clusterFunction as HansharFunction).Vehicles.Where(vhcl => vhcl.assignedClients.Any(clnt => !(clnt is Depot)) || vhcl.clientsToAssign.Any(clnt => !(clnt is Depot)));
            ids = importantVehicles.Select(vhcl => vhcl.Id);
            roughHeuristicSolution = CreateCodingVectorFromModel(
                importantVehicles,
                (clusterFunction as HansharFunction).ClientsToAssign,
                false,
                0.0
                );
            khouadjiaSolution = KhouadjiaFunction.GetKhouadjiaEncoding((clusterFunction as HansharFunction).ClientsToAssign);
        }

        internal void InitializeBySpanningTree(out ParticleSwarmOptimization.IFunction clusterFunction, out IEnumerable<int> ids, out double[] roughHeuristicSolution, out int[] hansharSolution, out int[] khouadjiaSolution)
        {
            clusterFunction = new TreeClusterFunction(Vehicles.ToArray(), clientsToAssign.ToArray(), Depots, (CurrentTime + timeStep) * (decimal)MaxTime, distances, this.weightPowerParameter, this.classesForVehicle, reservoirVehiclesCount, this.applyDistanceHeuristic, this.penalizeInsufficentVehicles, this.cutoffTime, this.bufferSize, this.fullMulticlustering);
            clusterFunction.Value(new double[0]);
            var importantVehicles = (clusterFunction as TreeClusterFunction).Vehicles.Where(vhcl => vhcl.assignedClients.Any(clnt => !(clnt is Depot)) || vhcl.clientsToAssign.Any(clnt => !(clnt is Depot)));
            ids = importantVehicles.Select(vhcl => vhcl.Id);
            roughHeuristicSolution = CreateCodingVectorFromModel(
                importantVehicles,
                (clusterFunction as TreeClusterFunction).ClientsToAssign,
                false,
                0.0
                );
            hansharSolution = HansharFunction.GetHansharEncoding(importantVehicles);
            khouadjiaSolution = KhouadjiaFunction.GetKhouadjiaEncoding((clusterFunction as TreeClusterFunction).ClientsToAssign);
        }

        private double OptimizeRoutes(bool randomizeRoute)
        {
            double val = 0.0;
            foreach (Vehicle vehicle in Vehicles.Where(vhcl => vhcl.clientsToAssign.Count + vhcl.assignedClients.Count > 0))
            {
                val += GetOptimumRouteForVehicle(vehicle, randomizeRoute);
                foreach (var clnt in vehicle.assignedClients.Concat(vehicle.clientsToAssign))
                {
                    var client = Clients.FirstOrDefault(clnt2 => clnt2.Id == clnt.Id);
                    if (client != null)
                        client.FakeVehicleId = vehicle.Id;
                }
            }
            //Utils.LogInfo("log.txt","[{2:yyyy-MM-dd HH:mm:ss}];{3};{0:0.00};{1:0.00};precise", currentTime, val, DateTime.Now, new FileInfo(filename).Name);
            return val;
        }

        internal void ConditionallyCopyFromStartVehicles()
        {
            if (AvailableVehicles.Count != Vehicles.Length)
            {
                foreach (Vehicle vehicle in Vehicles)
                {
                    vehicle.Available = true;
                }
            }
            for (int i = 0; i < Vehicles.Length; ++i)
            {
                Vehicles[i] = (Vehicle)StartVehicles[i].Clone();
            }
        }


        internal void PrepareVehicles()
        {
            if (!usePreviousSolutions)
            {
                foreach (Client client in clientsToAssign)
                {
                    client.Rank = (decimal)Utils.Instance.random.NextDouble();
                }
            }
            if (AvailableVehicles.Count == Vehicles.Length)
            {
                for (int i = 0; i < Vehicles.Length; i++)
                {
                    if (retrievePreviousSolutionThroughDiscretization)
                    {
                        if (Vehicles[i].clientsToAssign.Any(clnt => !(clnt is Depot)) && ((metaheuristic & (int)ContinuousMetaheuristic.GridLog) == 0))
                        {
                            for (int j = 0; j < Vehicles[i].X.Length; ++j)
                            {
                                Vehicles[i].X[j] = Vehicles[i].clientsToAssign.Where(clnt => !(clnt is Depot)).Average(clnt => clnt.X);
                                Vehicles[i].Y[j] = Vehicles[i].clientsToAssign.Where(clnt => !(clnt is Depot)).Average(clnt => clnt.Y);
                            }
                        }
                    }
                    if (Vehicles[i].Available)
                        Vehicles[i].clientsToAssign.Clear();
                }
            }
            else
            {
                foreach (Vehicle vehicle in AvailableVehicles)
                {
                    if (retrievePreviousSolutionThroughDiscretization)
                    {
                        if (vehicle.clientsToAssign.Any(clnt => !(clnt is Depot)) && ((metaheuristic & (int)ContinuousMetaheuristic.GridLog) == 0))
                        {
                            for (int j = 0; j < vehicle.X.Length; ++j)
                            {
                                vehicle.X[j] = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Average(clnt => clnt.X);
                                vehicle.Y[j] = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Average(clnt => clnt.Y);
                            }
                        }
                    }
                    vehicle.clientsToAssign.Clear();
                }
                Vehicle[] unavailableVehicles = Vehicles.Where(vhcl => !vhcl.Available).ToArray();
                for (int i = 0; i < unavailableVehicles.Length; ++i)
                {
                    unavailableVehicles[i] = (Vehicle)StartVehicles.First(vhcl => vhcl.Id == unavailableVehicles[i].Id).Clone();
                }
            }
        }

        private void ResetVehicles()
        {
            foreach (Vehicle vehicle in Vehicles)
            {
                vehicle.Available = true;

                vehicle.Capacity =
                                    (int)Math.Min(
            Math.Max(ClientsToAssign.Count > 0 ? ClientsToAssign.Max(clnt => -clnt.Need) : 0,
            capacity * initialCapacityPart + Math.Ceiling((capacitySlope * capacity * currentTime))),
            capacity);
                vehicle.SetEarliestLeaveTime((currentTime + timeStep) * vehicle.Depot.EndAvailable);
            }
        }

        private void SetClientsToAssign()
        {
            if (Clients.Length > 0 && Vehicles.Length > 0 && Depots.Length > 0)
            {
                clientsToAssign = Clients.Where(
                    clnt => clnt.StartAvailable <= (decimal)this.MaxTime * this.CurrentTime &&
                        !Vehicles.Any(vhcl => vhcl.assignedClients.Any(clnt2 => clnt2.Id == clnt.Id))
                    ).ToList();
                /*
                                    availableCapacity =
                                        //        (decimal)Vehicles.Max(vhcl => vhcl.Capacity);

                                Math.Min(
                                Math.Max(ClientsToAssign.Count > 0 ? ClientsToAssign.Max(clnt => -clnt.Need) : 0.0M,
                                (decimal)Vehicles.Max(vhcl => vhcl.Capacity) * currentTime * 1.2M),
                                (decimal)Vehicles.Max(vhcl => vhcl.Capacity));
                 */
            }
            else
            {
                clientsToAssign = new List<Client>();
            }
        }

    }
}