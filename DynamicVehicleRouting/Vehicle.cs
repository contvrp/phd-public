﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParticleSwarmOptimization;
using System.Runtime.Serialization;

namespace DynamicVehicleRouting
{
    [DataContract]
    [KnownType(typeof(List<Client>))]
    [KnownType(typeof(Depot))]
    [KnownType(typeof(Client))]
    public class Vehicle : SpatialPoint, ICloneable, IFunction
    {
        public bool ignoreTimeBounds { get; set; }
        public List<Client> Unassigned { get; set; }
        public double[,] Distances;
        [DataMember]
        public Depot Depot { get; private set; }
        public void SetDepot(Depot depot)
        {
            Depot = depot;
        }
        public List<Client> Route { get; set; }
        [DataMember]
        public decimal EarliestLeaveTime { get; private set; }
        public void SetEarliestLeaveTime(decimal time)
        {
            EarliestLeaveTime = time;
        }

        [DataMember]
        public new double[] X, Y;

        [DataMember]
        public int Cargo;
        [DataMember]
        public int Capacity;
        [DataMember]
        public List<Client> clientsToAssign;
        [DataMember]
        public List<Client> assignedClients;
        [DataMember]
        public bool Available;
        public int GetId
        {
            get
            {
                return Id;
            }
        }

        public int CountClientsToAssign
        {
            get
            {
                return clientsToAssign.Count(clnt => !(clnt is Depot));
            }
        }

        public int CountAssignedClients
        {
            get
            {
                return assignedClients.Count(clnt => !(clnt is Depot));
            }
        }

        public int Need
        {
            get
            {
				return (clientsToAssign.Count > 0 ? clientsToAssign.Sum(clnt => clnt.Need) : 0) +
					   (assignedClients.Count > 0 ? assignedClients.Sum(clnt => clnt.Need) : 0);
            }
        }


        public decimal ClientsRouteTime
        {
            get
            {
                decimal time = 0.0M;
                for (int i = 0; i < clientsToAssign.Count; i++)
                {
                    int j = (i + 1) % clientsToAssign.Count;
                    decimal currentDist = (decimal)Utils.Instance.EuclideanDistance(new double[] { clientsToAssign[i].X, clientsToAssign[i].Y }, new double[] { clientsToAssign[j].X, clientsToAssign[j].Y }, clientsToAssign[i].Id, clientsToAssign[j].Id);
                    time += clientsToAssign[i].TimeToUnload + currentDist;
                }
                return time;

            }
        }

        public decimal FinishTime
        {
            get
            {
                    if (clientsToAssign.Count > 0)
                    {
                        Client start = assignedClients.Count > 0 ? assignedClients.First() : Depot;
                        Client finish = clientsToAssign.OrderBy(clnt => clnt.VisitTime).Last();
                        double dist = Utils.Instance.EuclideanDistance(
                            new double[] {start.X, start.Y},
                            new double[] { finish.X, finish.Y },
                            start.Id,
                            finish.Id
                            );
                        return finish.VisitTime + finish.TimeToUnload + (decimal)dist;
                    }
                    else if (assignedClients.Count > 0)
                    {
                        Client start = assignedClients.First();
                        Client finish = assignedClients.OrderBy(clnt => clnt.VisitTime).Last();
                        double dist = Utils.Instance.EuclideanDistance(
                            new double[] { start.X, start.Y },
                            new double[] { finish.X, finish.Y },
                            start.Id,
                            finish.Id
                            );
                        return finish.VisitTime + finish.TimeToUnload + (decimal)dist;
                    }
                    else
                        return 0.0M;
            }
        }

        public double GetDistanceFromVehicle(Client client, double normalizationFactor)
        {
            IEnumerable<Client> clients = this.assignedClients.Concat(this.clientsToAssign).Where(clnt => !(clnt is Depot));
            if (!clients.Any())
                return 0.0;
            double avgX = clients.Average(clnt => clnt.X);
            double avgY = clients.Average(clnt => clnt.Y);
            double distance = Utils.Instance.EuclideanDistance(new double[] { avgX, avgY }, new double[] { client.X, client.Y });
            distance /= normalizationFactor;
            return distance;
        }

        public object Clone()
        {
            Vehicle v = new Vehicle();
            v.Available = this.Available;
            v.Capacity = this.Capacity;
            v.Cargo = this.Cargo;
            v.clientsToAssign = new List<Client>();
            foreach (Client c in this.clientsToAssign)
            {
                if (!(c is Depot))
                    v.clientsToAssign.Add((Client)c.Clone());
            }
            v.assignedClients = new List<Client>();
            v.assignedClients.AddRange(this.assignedClients);
            v.Id = this.Id;
            v.X = this.X.ToArray();
            v.Y = this.Y.ToArray();
            v.EarliestLeaveTime = this.EarliestLeaveTime;
            if (this.Depot != null)
            {
                v.Depot = (Depot)this.Depot.Clone();
                v.Depot.VisitTime = this.EarliestLeaveTime;
            }
            v.Distances = this.Distances;
            return v;
        }

        /// <summary>
        /// Wysyłanie pojazdów do klientów (ustalanie trasy)
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="vehicleRouteFunction"></param>
        /// <param name="best"></param>
        /// <returns></returns>
        public double CommitVehicles(double[] best, decimal advancedCommitmentTimeSteps, decimal timeStep, bool scaleAdvancedCommitimentToPartOfTheDay)
        {
            decimal EPS = 0.02M;
            Unassigned = new List<Client>();
            double bestValue = this.Value(best);
            this.clientsToAssign = this.Route;
            /*
            if (this.Unassigned.Count > 0)
                this.Available = false;
            */
            decimal advancedCommitmentPart = (1 - (1 + advancedCommitmentTimeSteps) * timeStep);
            decimal timeOffset = 0.0M;
            if (scaleAdvancedCommitimentToPartOfTheDay)
                timeOffset = EarliestLeaveTime;
            if (this.FinishTime + EPS > Math.Min((Depot.EndAvailable - timeOffset) * advancedCommitmentPart + timeOffset, Depot.EndAvailable * (1 - timeStep)))
            {
                while (this.clientsToAssign.Count > 0
                    && (this.assignedClients.Count == 0 || this.assignedClients.Last().VisitTime + this.assignedClients.Last().TimeToUnload - EPS < EarliestLeaveTime + timeStep * Depot.EndAvailable))
                {
                    this.assignedClients.Add(this.clientsToAssign[0]);
                    this.clientsToAssign.RemoveAt(0);
                }
                if (this.assignedClients.Count == 1)
                {
                    this.clientsToAssign.Insert(0, this.assignedClients[0]);
                    this.assignedClients.RemoveAt(0);
                }
            }
            //Console.WriteLine("{0:0000} {1:0000} {2:00} {3:00}",vehicle.Id, vehicle.FinishTime, vehicle.CountAssignedClients, vehicle.CountClientsToAssign);
            return bestValue;
        }

        public double GetRouteLength(Depot[] depots, decimal earliestLeaveTime, double[,] distances)
        {
            this.Depot = depots[0];
            this.EarliestLeaveTime = earliestLeaveTime;
            this.ignoreTimeBounds = false;
            this.Distances = distances;
            this.clientsToAssign.RemoveAll(clnt => clnt is Depot);
            double newValue = this.Value(this.Encode());
            this.clientsToAssign = this.Route;
            if (this.Unassigned.Count > 0)
                return double.MaxValue;
            return newValue;
        }

        /// <summary>
        /// Poprawia znaleziony wynik przy użyciu 2Opta
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="vehicleRouteFunction"></param>
        /// <param name="center"></param>
        /// <returns></returns>
        public double[] EnhanceWith2Opt(double[] center, Depot[] Depots, double[,] distances)
        {
            //TODO: znaleźć przyczynę zapętlenia
            int counter = 0;
            bool change = true;
            int counterUpperBound = int.MaxValue;
            double length = this.ValueWithout2OPT(center);
            while (change && counter++ < counterUpperBound)
            {
                counterUpperBound = this.Route.Count * this.Route.Count * this.Route.Count * this.Route.Count;
                change = false;
                double gain = 0.0;
                int to1BestId = 0;
                int from2BestId = 0;
                for (int i = 0; i < this.Route.Count - 1; i++)
                {
                    if (this.Route[i] is Depot)
                        continue;
                    for (int j = i + 2; j < this.Route.Count + 1; j++)
                    {
                        Client from1 = null;
                        if (i == 0)
                        {
                            from1 = this.assignedClients.LastOrDefault();
                            if (from1 == null)
                            {
                                from1 = Depots.FirstOrDefault();
                            }
                        }
                        else
                        {
                            from1 = this.Route[i - 1];
                        }
                        Client from2 = this.Route[j - 1];
                        Client to1 = this.Route[i];
                        Client to2 = null;
                        if (j == this.Route.Count)
                        {
                            to2 = Depots.First();
                        }
                        else
                        {
                            if (this.Route[j - 1] is Depot)
                                break;
                            to2 = this.Route[j];
                        }
                        double tempGain =
                            distances[from1.Id, to1.Id] +
                            distances[from2.Id, to2.Id] -
                            distances[from1.Id, from2.Id] -
                            distances[to2.Id, to1.Id];
                        if (tempGain - gain > +1e-6)
                        {
                            gain = tempGain;
                            change = true;
                            to1BestId = i;
                            from2BestId = j - 1;
                        }
                    }
                }
                if (change)
                {
                    //Console.WriteLine(length);
                    //for (int i = 0; i < vehicleRouteFunction.Route.Count - 1; ++i )
                    //{
                    //    Console.Write("{0:0.00};", distances[vehicleRouteFunction.Route[i].Id,
                    //        vehicleRouteFunction.Route[i + 1].Id]);
                    //}
                    //Console.WriteLine();
                    //Console.WriteLine(string.Join(",", vehicleRouteFunction.Route.Select(clnt => clnt.Id).ToArray()));
                    this.Route = this.Route.Take(to1BestId)
                        .Concat(
                            this.Route.Take(from2BestId+1).Skip(to1BestId).Reverse())
                        .Concat(
                            this.Route.Skip(from2BestId+1)
                        )
                        .ToList();
                    /*
                    for (int i = to1BestId, j = from2BestId; i < j; ++i, --j)
                    {
                        while (this.Route[i].Rank == this.Route[j].Rank)
                        {
                            decimal minDist = this.Route.Min(clnt => (this.Route.Min(clnt2 => (clnt.Rank - clnt2.Rank == 0) ? decimal.MaxValue : Math.Abs(clnt.Rank - clnt2.Rank))));
                            this.Route[i].Rank -= minDist / 2;
                        }
                        decimal temp = this.Route[i].Rank;
                        this.Route[i].Rank = this.Route[j].Rank;
                        this.Route[j].Rank = temp;
                    }
                    */
                }
            }
            var ranks = this.Route.Where(clnt => !(clnt is Depot)).OrderBy(clnt => clnt.Rank).Select(clnt => clnt.Rank).ToList();
            var routeIterator = this.Route.Where(clnt => !(clnt is Depot)).GetEnumerator();
            int r = 0;
            while (routeIterator.MoveNext())
            {
                routeIterator.Current.Rank = ranks[r++];
            }
            center = this.Encode();
            return center;
        }

        /// <summary>
        /// Finds greedy optimum of inserting client into Vehicle, sets -1 as bestVehicleId if
        /// assignment is impossible
        /// </summary>
        /// <param name="newClient"></param>
        /// <param name="bestVehicleId"></param>
        /// <param name="bestRank"></param>
        /// <param name="smallestIncrease"></param>
        /// <param name="vehicle"></param>
        public void CheckPossibilityAndCostOfAssigningClientToVehicle(Client newClient, ref int bestVehicleId, ref decimal bestRank, ref double smallestIncrease,
            Depot[] depots, decimal currentProblemTime, double[,] distances, ref int bestVehicleNeed)
        {
            if (-this.Need - newClient.Need > this.Capacity)
            {
                if (this.clientsToAssign.Count > 0)
                    bestRank = this.clientsToAssign.Max(clnt => clnt.Rank) + 1;
                else
                    bestRank = 1;
                return;
            }
            //Compute initial route length (before adding client)
            double originalValue = this.GetRouteLength(depots, currentProblemTime, distances);
            decimal[] ranks = this.GetSortedRanks();
            this.clientsToAssign.Add(newClient);
            newClient.FakeVehicleId = this.Id;
            for (int i = 0; i < ranks.Length + 1; ++i)
            {
                if (ranks.Length == 0)
                {
                    newClient.Rank = 0.5M;
                }
                else if (i == 0)
                {
                    newClient.Rank = (decimal)ranks[0] - 1M;
                }
                else if (i == ranks.Length)
                {
                    newClient.Rank = ranks[ranks.Length - 1] + 1M;
                }
                else
                {
                    newClient.Rank = (ranks[i - 1] + ranks[i]) / 2M;
                }
                //TODO: This is very non-optimal
                double newValue = this.GetRouteLength(depots, currentProblemTime, distances);
                if (double.IsNaN(newValue) || this.Unassigned.Count > 0)
                {
                    this.clientsToAssign.AddRange(this.Unassigned);
                    this.Unassigned.Clear();
                    continue;
                }
                double localbestVehicleId = bestVehicleId;
                if (newValue - originalValue < smallestIncrease || ((newValue - originalValue <= smallestIncrease) && -this.Need < -bestVehicleNeed))
                {
                    smallestIncrease = newValue - originalValue;
                    bestRank = newClient.Rank;
                    bestVehicleId = newClient.FakeVehicleId;
                    bestVehicleNeed = this.Need;
                }
            }
            this.clientsToAssign.Remove(newClient);
        }




        public double Value(double[] x)
        {
            //x = DVRPInstance.EnhanceWith2Opt(vehicle, this, x, Depots, distances);
            return ValueWithout2OPT(x);
        }

        private Dictionary<string, double> distanceDict = new Dictionary<string, double>();
        private Dictionary<string, List<Client>> routesDict = new Dictionary<string, List<Client>>();
        private Dictionary<string, List<Client>> unassignedDict = new Dictionary<string, List<Client>>();

        public double ValueWithout2OPT(double[] x)
        {
            SetRanks(x);
            /*
            string key = string.Join(",",vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).OrderBy(clnt => clnt.Rank).Select(clnt => clnt.Id));
            if (!distanceDict.ContainsKey(key))
            {
            */
            double dist;
            List<Client> newRoute;
            ConstructRouteAndComputeDistance(out newRoute, out dist);
            //Dodać karę za spóźnienie jeżeli nie usuwamy pojazdów
            if (ignoreTimeBounds && newRoute.Count(clnt => !(clnt is Depot)) > 0)
                CheckAndAddPenalty(newRoute, ref dist);
            /*
                distanceDict.Add(key, dist);
                routesDict.Add(key, newRoute);
                unassignedDict.Add(key, Unassigned);
            }
            */
            this.Cargo = 0;
            /*
            Route = routesDict[key].Select(clnt => (Client)clnt.Clone()).ToList();

            Unassigned = unassignedDict[key].Select(clnt => (Client)clnt.Clone()).ToList();
            return distanceDict[key];
            */
            Route = newRoute;
            return dist;
        }

        private void CheckAndAddPenalty(List<Client> newRoute, ref double dist)
        {
            var lastClient = newRoute.Last(clnt => !(clnt is Depot));
            double penalty = (double)lastClient.VisitTime + lastClient.TimeToUnload + Distances[Depot.Id, lastClient.Id] - Depot.EndAvailable;
            if (penalty > 0)
            {
                dist += 10 * penalty;
            }
        }

        private void ConstructRouteAndComputeDistance(out List<Client> newRoute, out double dist)
        {
            Depot = (Depot)Depot.Clone();
            Depot.VisitTime = EarliestLeaveTime;
            newRoute = this.clientsToAssign.OrderBy(clnt => clnt.Rank).ToList();
            Unassigned = new List<Client>();
            dist = 0.0;
            Client prevClient = (this.assignedClients.Count > 0) ? this.assignedClients[0] : Depot;
            for (int i = 0; i < this.assignedClients.Count; i++)
            {
                ComputeDistanceAndNeedAndMoveToNext(ref dist, ref prevClient, this.assignedClients[i], true);
            }
            for (int i = 0; i < newRoute.Count; i++)
            {
                if (this.Cargo + newRoute[i].Need < 0)
                {
                    newRoute.Insert(i, (Depot)Depot.Clone());
                }
                ComputeDistanceAndNeedAndMoveToNext(ref dist, ref prevClient, newRoute[i]);
                if (!ignoreTimeBounds)
                {
                    if (DoLateExistAndMoveToUnassigned(newRoute, i))
                        break;
                }

            }
            dist += Distances[prevClient.Id
                , Depot.Id
                ];
        }

        private void ComputeDistanceAndNeedAndMoveToNext(ref double dist, ref Client prevClient, Client currentClient, bool assigned = false)
        {
            if (currentClient is Depot)
                this.Cargo = this.Capacity;
            else
                this.Cargo += currentClient.Need;
            double currentDist = Distances[prevClient.Id,
                currentClient.Id
                ];
            dist += currentDist;
            if (!assigned)
                currentClient.VisitTime = Math.Max(EarliestLeaveTime, prevClient.VisitTime + prevClient.TimeToUnload) + (decimal)currentDist;
            prevClient = currentClient;
        }

        private bool DoLateExistAndMoveToUnassigned(List<Client> newRoute, int i)
        {
            bool needsBreak = false;
            int lastId = newRoute[i].Id;
            if (newRoute[i].VisitTime + newRoute[i].TimeToUnload + (decimal)Distances[
                Depot.Id,
                lastId
                ] > Depot.EndAvailable)
            {
                Unassigned.AddRange(newRoute.GetRange(i, newRoute.Count - i));
                newRoute.RemoveRange(i, newRoute.Count - i);
                needsBreak = true;
            }
            return needsBreak;
        }

        private void SetRanks(double[] x)
        {
            int rankIndex = 0;
            var enumClient = this.clientsToAssign.OrderBy(clnt => clnt.Id).GetEnumerator();
            while (enumClient.MoveNext())
            {
                if (!(enumClient.Current is Depot))
                {
                    if (x[rankIndex] > (double)decimal.MaxValue)
                        enumClient.Current.Rank = decimal.MaxValue;
                    else if (x[rankIndex] < (double)decimal.MinValue)
                        enumClient.Current.Rank = decimal.MinValue;
                    else
                        enumClient.Current.Rank = (decimal)x[rankIndex];
                    ++rankIndex;
                    enumClient.Current.VisitTime = EarliestLeaveTime;
                }
            }
        }

        public int Dimension
        {
            get { return this.clientsToAssign != null ? this.clientsToAssign.Where(clnt => !(clnt is Depot)).Count() : 0; }
        }

        public int ValueCount
        {
            get { throw new NotImplementedException(); }
        }


        public double[] Encode()
        {
            return this.clientsToAssign.Where(clnt => !(clnt is Depot)).OrderBy(clnt => clnt.Id).Select(clnt => decimal.ToDouble(clnt.Rank)).ToArray();
        }

        public decimal[] GetSortedRanks()
        {
            return this.clientsToAssign.Where(clnt => !(clnt is Depot)).OrderBy(clnt => clnt.Rank).Select(clnt => clnt.Rank).ToArray();
        }

        public override string ToString()
        {
            return string.Format("{0} [{1}] [{2}]",
                this.Id,
                string.Join(",", this.assignedClients.Select(clnt => clnt.Id)),
                string.Join(",", this.clientsToAssign.OrderBy(clnt=> clnt.Rank).Select(clnt => clnt.Id)));
        }
    }
}
