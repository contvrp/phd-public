﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRouting
{
    public class Client: SpatialPoint, IComparable<Client>, ICloneable
    {
        /// <summary>
        /// Czas w jakim zamówienie staje się znane
        /// </summary>
        public int StartAvailable;
        public int EndAvailable;
        public int TimeToUnload;
        public int Need;
        public decimal Rank;
        /// <summary>
        /// Czas w jakim zamówienie zostanie odwiedzone - na początku ustawiamy na aktualny
        /// </summary>
        public decimal VisitTime;
        public int FakeVehicleId;
        public double[] positionForClientAssignment;

        public int CompareTo(Client other)
        {
            int rankComp = Rank.CompareTo(other.Rank);
            if (rankComp != 0)
                return rankComp;
            else
                return Id.CompareTo(Id);
        }

        public object Clone()
        {
            Client newClient = new Client();
            if (this is Depot)
                newClient = new Depot();
            newClient.EndAvailable = this.EndAvailable;
            newClient.Need = this.Need;
            newClient.Rank = this.Rank;
            newClient.TimeToUnload = this.TimeToUnload;
            newClient.StartAvailable = this.StartAvailable;
            newClient.VisitTime = this.VisitTime;
            newClient.FakeVehicleId = this.FakeVehicleId;
            newClient.Id = this.Id;
            newClient.X = this.X;
            newClient.Y = this.Y;
            if (this.positionForClientAssignment != null)
            {
                newClient.positionForClientAssignment = new double[this.positionForClientAssignment.Length];
                for (int i = 0; i < this.positionForClientAssignment.Length; ++i)
                    newClient.positionForClientAssignment[i] = this.positionForClientAssignment[i];
            }
            return newClient;
        }
    }
}
