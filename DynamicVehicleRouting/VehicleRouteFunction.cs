﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParticleSwarmOptimization;

namespace DynamicVehicleRouting
{
    class VehicleRouteFunction : IFunction
    {
        public List<Client> Route;
        public List<Client> Unassigned;
        private Vehicle vehicle;
        private Depot[] Depots;
        private decimal currentProblemTime;
        private bool ignoreTimeBounds;
        private double[,] distances;

        public VehicleRouteFunction(Vehicle vehicle, Depot[] depots, decimal currentProblemTime, bool ignoreTimeBounds, double[,] distances)
        {
            this.vehicle = vehicle;
            this.Depots = depots;
            this.currentProblemTime = currentProblemTime;
            this.ignoreTimeBounds = ignoreTimeBounds;
            this.distances = distances;
        }

        public double Value(double[] x)
        {
            //x = DVRPInstance.EnhanceWith2Opt(vehicle, this, x, Depots, distances);
            return ValueWithout2OPT(x);
        }

        private Dictionary<string, double> distanceDict = new Dictionary<string,double>();
        private Dictionary<string, List<Client>> routesDict = new Dictionary<string,List<Client>>();
        private Dictionary<string, List<Client>> unassignedDict = new Dictionary<string, List<Client>>();

        public double ValueWithout2OPT(double[] x)
        {
            SetRanks(x);
            /*
            string key = string.Join(",",vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).OrderBy(clnt => clnt.Rank).Select(clnt => clnt.Id));
            if (!distanceDict.ContainsKey(key))
            {
            */
                double dist;
                List<Client> newRoute;
                ConstructRouteAndComputeDistance(out newRoute, out dist);
                //Dodać karę za spóźnienie jeżeli nie usuwamy pojazdów
                if (ignoreTimeBounds && newRoute.Count(clnt => !(clnt is Depot)) > 0)
                    CheckAndAddPenalty(newRoute, ref dist);
            /*
                distanceDict.Add(key, dist);
                routesDict.Add(key, newRoute);
                unassignedDict.Add(key, Unassigned);
            }
            */
            vehicle.Cargo = 0;
            /*
            Route = routesDict[key].Select(clnt => (Client)clnt.Clone()).ToList();

            Unassigned = unassignedDict[key].Select(clnt => (Client)clnt.Clone()).ToList();
            return distanceDict[key];
            */
            Route = newRoute;
            return dist;
        }

        private void CheckAndAddPenalty(List<Client> newRoute, ref double dist)
        {
            if (newRoute.Last(clnt => !(clnt is Depot)).VisitTime + newRoute.Last(clnt => !(clnt is Depot)).TimeToUnload + (decimal)Depots.Min(dpt => distances[
                dpt.Id,
                newRoute.Last(clnt => !(clnt is Depot)).Id
                ]) > Depots.Max(dpt => dpt.EndAvailable))
            {
                double penalty = (double)(newRoute.Last(clnt => !(clnt is Depot)).VisitTime + newRoute.Last(clnt => !(clnt is Depot)).TimeToUnload + (decimal)Depots.Min(dpt => distances[
                dpt.Id,
                newRoute.Last(clnt => !(clnt is Depot)).Id
                ])) - (double)Depots.Max(dpt => dpt.EndAvailable);
                dist += 10 * penalty;
            }
        }

        private void ConstructRouteAndComputeDistance(out List<Client> newRoute, out double dist)
        {
            newRoute = vehicle.clientsToAssign.OrderBy(clnt => clnt.Rank).ToList();
            Unassigned = new List<Client>();
            dist = 0.0;
            Client prevClient = (vehicle.assignedClients.Count > 0) ? vehicle.assignedClients[0] : Depots.First();
            for (int i = 0; i < vehicle.assignedClients.Count; i++)
            {
                ComputeDistanceAndNeedAndMoveToNext(ref dist, ref prevClient, vehicle.assignedClients[i], true);
            }
            for (int i = 0; i < newRoute.Count; i++)
            {
                if (vehicle.Cargo + newRoute[i].Need < 0)
                {
                    newRoute.Insert(i, (Depot)Depots.First().Clone());
                }
                ComputeDistanceAndNeedAndMoveToNext(ref dist, ref prevClient, newRoute[i]);
                if (!ignoreTimeBounds)
                {
                    if (DoLateExistAndMoveToUnassigned(newRoute, i))
                        break;
                }

            }
            dist += distances[prevClient.Id
                , Depots.First().Id
                ];
        }

        private void ComputeDistanceAndNeedAndMoveToNext(ref double dist, ref Client prevClient, Client currentClient, bool assigned = false)
        {
            if (currentClient is Depot)
                vehicle.Cargo = vehicle.Capacity;
            else
                vehicle.Cargo += currentClient.Need;
            double currentDist = distances[prevClient.Id,
                currentClient.Id
                ];
            dist += currentDist;
            if (!assigned)
                currentClient.VisitTime = Math.Max(currentProblemTime, prevClient.VisitTime) + prevClient.TimeToUnload + (decimal)currentDist;
            prevClient = currentClient;
        }

        private bool DoLateExistAndMoveToUnassigned(List<Client> newRoute, int i)
        {
            bool needsBreak = false;
            int lastId = newRoute[i].Id;
            if (newRoute[i].VisitTime + newRoute[i].TimeToUnload + (decimal)Depots.Min(dpt => distances[
                dpt.Id,
                lastId
                ]) > Depots.Max(dpt => dpt.EndAvailable))
            {
                Unassigned.AddRange(newRoute.GetRange(i, newRoute.Count - i));
                newRoute.RemoveRange(i, newRoute.Count - i);
                needsBreak = true;
            }
            return needsBreak;
        }

        private void SetRanks(double[] x)
        {
            int rankIndex = 0;
            for (int i = 0; i < vehicle.clientsToAssign.Count; i++)
            {
                if (!(vehicle.clientsToAssign[i] is Depot))
                {
                    if (x[rankIndex] > (double)decimal.MaxValue)
                        vehicle.clientsToAssign[i].Rank = decimal.MaxValue;
                    else if (x[rankIndex] < (double)decimal.MinValue)
                        vehicle.clientsToAssign[i].Rank = decimal.MinValue;
                    else
                        vehicle.clientsToAssign[i].Rank = (decimal)x[rankIndex];
                    ++rankIndex;
                    vehicle.clientsToAssign[i].VisitTime = currentProblemTime;
                }
            }
        }

        public int Dimension
        {
            get { return vehicle.clientsToAssign != null ? vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Count() : 0; }
        }

        public int ValueCount
        {
            get { throw new NotImplementedException(); }
        }
    }

}
