﻿using ParticleSwarmOptimization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRouting
{
    abstract class MonteCarloWithSolutionProposals
    {
        protected bool penalizeInsufficentVehicles;
        protected decimal cutoffTime;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance">The instance to be solved</param>
        /// <param name="models">The number of propositions</param>
        /// <param name="generateDataFromModel">Generate from oracle?</param>
        public MonteCarloWithSolutionProposals(DVRPInstance instance, int models, int additionalModels, bool generateDataFromModel,
            bool penalizeInsufficentVehicles, decimal bufferSize, decimal cutoffTime)
        {
            this.instance = instance;
            this.generateDataFromModel = generateDataFromModel;
            this.models = models;
            this.additionalModelsCount = additionalModels;
            int known = instance.Clients.Count(clnt => clnt.StartAvailable <= instance.CurrentTime * (decimal)instance.MaxTime);
            if (generateDataFromModel)
                this.countNew = (int)Math.Round(Math.Max((cutoffTime - instance.CurrentTime), 0) * known * bufferSize / (1 - cutoffTime + instance.CurrentTime ));
            else
                this.countNew = instance.Clients.Length - known;
            this.maxX = instance.Clients.Max(clnt => clnt.X);
            this.maxY = instance.Clients.Max(clnt => clnt.Y);
            this.minX = instance.Clients.Min(clnt => clnt.X);
            this.minY = instance.Clients.Min(clnt => clnt.Y);
            this.cutoffTime = cutoffTime;
        }

        protected DVRPInstance instance;
        protected int countNew;
        protected double maxX;
        protected double maxY;
        protected double minX;
        protected double minY;
        protected bool generateDataFromModel;
        protected int models;
        protected int additionalModelsCount;
        protected double C = 1.0;

        protected class ValuedClusterings
        {
            public List<double> values;
            public ClusterFunction cluster;
            public object solution;
            public DVRPInstance instance;
        }

        protected DVRPInstance GenerateFakeRequests()
        {
            DVRPInstance simulationInstance = instance.Clone();
            int originalLength = simulationInstance.Clients.Length;
                simulationInstance.Clients = simulationInstance.Clients.Concat(new Client[this.countNew]).ToArray();
            
                //TODO: how to generate to preserve IDs for caching and reassignment and references for assignedClients?
                //Answer: add fake at the end and ignore the rest

                for (int i = originalLength; i < originalLength + countNew; ++i)
                {
                    Client client = simulationInstance.Clients[i] = new Client();
                    client.Id = i+1;
                    if (generateDataFromModel)
                    {
                        double x = 0.0;
                        double y = 0.0;
                        do
                        {
                            bool cont = false;
                            x = Utils.Instance.random.NextDouble() * (maxX - minX) + minX;
                            y = Utils.Instance.random.NextDouble() * (maxY - minY) + minY;
                            //foreach (Client setClient in simulationInstance.Clients.Where(clnt => clnt != null && clnt.Id <= originalLength && (double)clnt.StartAvailable / simulationInstance.MaxTime <= (double)simulationInstance.CurrentTime))
                            //{
                            //    double dist = 1 - Math.Exp(-0.5 * ((x - setClient.X) * (x - setClient.X) + (y - setClient.Y) * (y - setClient.Y)) / ((maxX - minX) * (maxY - minY) / countNew));
                            //    if (Utils.random.NextDouble() > dist)
                            //    {
                            //        cont = true;
                            //        break;
                            //    }
                            //}
                            if (!cont)
                                break;
                        } while (true);
                        client.X = x;
                        client.Y = y;
                        client.Rank = (decimal)Utils.Instance.random.NextDouble();
                        client.Need = simulationInstance.ClientsToAssign[Utils.Instance.random.Next(simulationInstance.ClientsToAssign.Count)].Need;
                        client.FakeVehicleId = 1000;
                        client.TimeToUnload = simulationInstance.Clients[0].TimeToUnload;
                    }
                    else
                    {
                        client.Rank = simulationInstance.Clients[i - originalLength].Rank;
                        client.X = simulationInstance.Clients[i - originalLength].X;
                        client.Y = simulationInstance.Clients[i - originalLength].Y;
                        client.Need = simulationInstance.Clients[i - originalLength].Need;
                        client.FakeVehicleId = 1000;
                    }
                    client.StartAvailable = (int)Math.Floor(simulationInstance.CurrentTime * (decimal)simulationInstance.MaxTime);
                    client.VisitTime = (int)Math.Floor(simulationInstance.CurrentTime * (decimal)simulationInstance.MaxTime);
                simulationInstance.ClientsToAssign.Add(client);
                }
            simulationInstance.CacheDistances();
            return simulationInstance;

        }

        public ClusterFunction Solve()
        {
            object previousSolution = GetPreviousSolution();
            List<ValuedClusterings> results = new List<ValuedClusterings>();
            for (int i = 0; i < models; ++i)
            {
                DVRPInstance fakeInstance = GenerateFakeRequests();
                ClusterFunction clusterFunction;
                double sum;
                ComputeRoutes(fakeInstance, out clusterFunction, out sum, i == 0, models == 1, previousSolution);
                //Console.WriteLine(sum);
                results.Add(new ValuedClusterings()
                {
                    values = new List<double>() { sum },
                    cluster = clusterFunction,
                    solution = ExtractSolution(clusterFunction),
                    instance = fakeInstance
                });
            }
            for (int i = 0; i < results.Count; ++i)
            {
                for (int j = 0; j < results.Count; ++j)
                {
                    if (i != j)
                    {
                        ClusterFunction clusterFunction;
                        double sum;
                        //Console.WriteLine("{0} {1}",i,j);
                        //Console.WriteLine(string.Join(",", results[j].clients.Select(clnt => clnt.FakeVehicleId).ToArray()));
                        ComputeRoutes(results[i].instance.Clone(), out clusterFunction, out sum, true, false, results[j].solution);
                        results[i].values.Add(sum);
                        //Console.WriteLine(sum);
                    }
                }
            }
            double maxSolutionValue = results.Max(rs => rs.values.Max());
            for (int i = 0; i < additionalModelsCount; i++)
            {
                DVRPInstance fakeInstance = GenerateFakeRequests();
                ValuedClusterings choice = results.OrderBy(rs => rs.values.Average()
                    + C * maxSolutionValue * Math.Sqrt((double)rs.values.Count / Math.Log(results.Sum(rs2 => rs2.values.Count)))).First();
                //Console.WriteLine(string.Join(",", results.Select(rs => (int)(rs.values.Average()
                //    + maxSolutionValue / 3.0 * Math.Sqrt((double)rs.values.Count / Math.Log(results.Sum(rs2 => rs2.values.Count)))))
                //    ));
                ClusterFunction clusterFunction;
                double sum;
                //Console.WriteLine("{0} {1}",i,j);
                //Console.WriteLine(string.Join(",", results[j].clients.Select(clnt => clnt.FakeVehicleId).ToArray()));
                ComputeRoutes(fakeInstance, out clusterFunction, out sum, true, false, choice.solution);
                if (double.IsNaN(sum))
                    sum = maxSolutionValue;
                choice.values.Add(sum);
                //Console.WriteLine(sum);
            }

            return results.OrderBy(rs => rs.values.Average()).ElementAt(0).cluster;
        }

        protected abstract object GetPreviousSolution();

        protected abstract object ExtractSolution(ClusterFunction clusterFunction);

        protected virtual void ComputeRoutes(
            DVRPInstance fakeInstance,
            out ClusterFunction clusterFunction,
            out double sum,
            bool usePartialSolution,
            bool singleModel,
            object solution = null)
        {
            clusterFunction = AssignClients(fakeInstance, (usePartialSolution && !singleModel) ? solution : null);
            sum = TwoOPTRoutes(fakeInstance, clusterFunction);
        }

        protected abstract ClusterFunction AssignClients(DVRPInstance fakeInstance, object solution);

        protected double TwoOPTRoutes(DVRPInstance fakeInstance, ClusterFunction clusterFunction)
        {
            double sum;
            sum = 0.0;
            foreach (Vehicle vehicle in clusterFunction.Vehicles.Where(vhcl => vhcl.clientsToAssign.Count > 0))
            {
                //TODO: This is the same code as in DapsoClusterFunction
                vehicle.ignoreTimeBounds = true;
                vehicle.Distances = fakeInstance.distances;
                double[] center = vehicle.Encode();
                center = vehicle.EnhanceWith2Opt(center, fakeInstance.Depots, fakeInstance.distances);
                sum += vehicle.Value(center);
                vehicle.clientsToAssign.RemoveAll(clnt => !(clnt is Depot) && !instance.ClientsToAssign.Any(clnt2 => clnt2.Id == clnt.Id));
            }
            return sum;
        }

    }
}
