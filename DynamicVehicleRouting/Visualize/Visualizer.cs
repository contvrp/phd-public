﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using DynamicVehicleRouting;

namespace DynamicVehicleRouting.Visualize
{
    public class Visualizer
    {
        static List<Color> colors;

        static Visualizer()
        {
            Random r = new Random();
            colors = new List<Color>();
            for (int i = 0; i < 500; i++)
            {
                int which = r.Next(3);
                colors.Add(Color.FromArgb(r.Next(100) + (which == 0 ? 100 : 0), r.Next(100) + (which == 1 ? 100 : 0), r.Next(100) + (which == 2 ? 100 : 0)));
            }
        }

        public static void DrawVehicleAssignment(DVRPInstance function, Graphics graphics)
        {
            if (function == null)
                return;
            lock (function)
            {
                Graphics g = graphics;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                try
                {
                    if (function.Vehicles.Where(vhcl => vhcl.clientsToAssign.Count + vhcl.assignedClients.Count > 0).ToList().Count == 0)
                        return;
                    double minX = Math.Min(function.Clients.Min(clt => clt.X), function.Depots.Min(dpt => dpt.X));
                    double minY = Math.Min(function.Clients.Min(clt => clt.Y), function.Depots.Min(dpt => dpt.Y));
                    double maxX = Math.Max(function.Clients.Max(clt => clt.X), function.Depots.Max(dpt => dpt.X));
                    double maxY = Math.Max(function.Clients.Max(clt => clt.Y), function.Depots.Max(dpt => dpt.Y));
                    g.Clear(Color.White);
                    foreach (Vehicle v in function.Vehicles)
                    {
                        if (v.clientsToAssign.Count + v.assignedClients.Count == 0)
                            continue;
                        Pen colorPen = new Pen(colors[v.Id - function.Vehicles[0].Id], 2);
                        Pen pen = new Pen(Color.FromArgb((int)(colorPen.Color.R * 1.27M), (int)(colorPen.Color.G * 1.27M), (int)(colorPen.Color.B * 1.27M)), 2);
                        /*
                        g.DrawImage(
                            Properties.Resources.Truck_48,
                            (float)((v.X - minX) * ((graphics.VisibleClipBounds.Width - 40) / (maxX - minX)) - 4),
                                (float)((v.Y - minY) * ((graphics.VisibleClipBounds.Height - 40) / (maxY - minY)) - 4));
                        */
                        List<Client> clients = new List<Client>();
                        clients.AddRange(v.assignedClients);
                        clients.AddRange(v.clientsToAssign);
                        for (int i = 0; i < clients.Count; ++i)
                        {
                            if (i >= v.assignedClients.Count - 1)
                                pen = colorPen;
                            if ((double)clients[i].VisitTime + clients[i].TimeToUnload + ParticleSwarmOptimization.Utils.Instance.EuclideanDistance(new double[] { clients[i].X, clients[i].Y },
                                new double[]{clients[0].X, clients[0].Y}, clients[i].Id, clients[0].Id) > clients[0].EndAvailable)
                            {
                                pen = new Pen(Color.Black, 2);
                            }
                            int j = (i + 1) % clients.Count;
                            if (i == v.assignedClients.Count - 1)
                            {
                                g.DrawEllipse(pen, (float)((clients[i].X - minX) * ((graphics.VisibleClipBounds.Width - 40) / (maxX - minX)) + 20) - 2,
                                    (float)((clients[i].Y - minY) * ((graphics.VisibleClipBounds.Height - 40) / (maxY - minY)) + 20) - 2, 5, 5);
                            }
                            g.DrawString(
                                clients[i].Id.ToString() + ":" + clients[i].VisitTime.ToString("0"),
                                new Font("Calibri", 14),
                                new SolidBrush(pen.Color),
                                (float)((clients[i].X - minX) * ((graphics.VisibleClipBounds.Width - 40) / (maxX - minX)) + 20),
                                (float)((clients[i].Y - minY) * ((graphics.VisibleClipBounds.Height - 40) / (maxY - minY)) + 20));
                            g.DrawLine(
                                pen,
                                (float)((clients[i].X - minX) * ((graphics.VisibleClipBounds.Width - 40) / (maxX - minX)) + 20),
                                (float)((clients[i].Y - minY) * ((graphics.VisibleClipBounds.Height - 40) / (maxY - minY)) + 20),
                                (float)((clients[j].X - minX) * ((graphics.VisibleClipBounds.Width - 40) / (maxX - minX)) + 20),
                                (float)((clients[j].Y - minY) * ((graphics.VisibleClipBounds.Height - 40) / (maxY - minY)) + 20)
                                );
                            //rysowanie powiązań
                            /*
                            g.DrawLine(
                                new Pen(new SolidBrush(Color.DarkGray),3),
                                (float)((clients[i].X - minX) * ((graphics.VisibleClipBounds.Width - 40) / (maxX - minX)) + 20),
                                (float)((clients[i].Y - minY) * ((graphics.VisibleClipBounds.Height - 40) / (maxY - minY)) + 20),
                                (float)((v.X - minX) * ((graphics.VisibleClipBounds.Width - 40) / (maxX - minX)) + 20),
                                (float)((v.Y - minY) * ((graphics.VisibleClipBounds.Height - 40) / (maxY - minY)) + 20)
                                );
                            */

                        }
                        //rysowanie pojazdów
                        for (int i = 0; i < v.X.Length; ++i)
                        {
                            g.FillEllipse(new SolidBrush(Color.Black), (float)((v.X[i] - minX) * ((graphics.VisibleClipBounds.Width - 40) / (maxX - minX)) + 20) - 4,
        (float)((v.Y[i] - minY) * ((graphics.VisibleClipBounds.Height - 40) / (maxY - minY)) + 20) - 4, 9, 9);
                        }
                    }
                }
                catch
                {
                    return;
                }

            }
        }


    }
}
