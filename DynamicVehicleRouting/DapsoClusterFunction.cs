﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRouting
{
    class DapsoClusterFunction: ClusterFunction
    {
        public DapsoClusterFunction(Vehicle[] vehicles, Client[] clients, Depot[] depots, decimal currentProblemTime, double[,] distances,
            double weightPowerParameter, int classesForVehicle, bool fullMulticlustering, bool penalizeInsufficentVehicles, decimal cutoffTime, decimal bufferSize)
            : base(vehicles, clients, depots, currentProblemTime, distances, weightPowerParameter, classesForVehicle, fullMulticlustering, penalizeInsufficentVehicles, cutoffTime, bufferSize)
        {
                CacheRanks(initialCache);
    }
        private Dictionary<string, double> assignmentValues = new Dictionary<string, double>();
        public const bool useCache = false;
        protected Dictionary<int, decimal> clientRanks;
        private double bestValue = double.PositiveInfinity;

        private enum CacheAction
        {
            None,
            Store,
            Random
        }
#if DEBUG
        private CacheAction initialCache = CacheAction.Store;
        private CacheAction afterNewBest = CacheAction.None;
#else
        private CacheAction initialCache = CacheAction.Random;
        private CacheAction afterNewBest = CacheAction.Store;
#endif

        private void CacheRanks(CacheAction action)
        {
            if (action == CacheAction.Random)
            {
                clientRanks = ClientsToAssign
                    .Select((clnt) => new { clnt.Id, clnt.Rank })
                    .ToDictionary(clnt => clnt.Id, clnt => (decimal)ParticleSwarmOptimization.Utils.Instance.random.NextDouble() * 2.0M - 1.0M);
            }
            else if (action == CacheAction.Store)
            {
                clientRanks = ClientsToAssign
                    .Select((clnt) => new { clnt.Id, clnt.Rank })
                    .ToDictionary(clnt => clnt.Id, clnt => clnt.Rank);
            }
                
        }

        protected override double CalculateValue()
        {
            if (clientRanks != null)
            {
                foreach (Client clnt in ClientsToAssign)
                {
                    clnt.Rank = clientRanks[clnt.Id];
                }
            }
            double sum = 0.0;
            foreach (Vehicle vehicle in Vehicles.Where(vhcl => vhcl.clientsToAssign.Count + vhcl.assignedClients.Count > 0))
            {
                if (useCache)
                {
                    string key = string.Join(",", vehicle.clientsToAssign.OrderBy(clnt => clnt.Rank).Select(clnt => clnt.Id));
                    if (!assignmentValues.ContainsKey(key))
                    {
                        vehicle.ignoreTimeBounds = true;
                        vehicle.Distances = distances;
                        var center = vehicle.EnhanceWith2Opt(vehicle.Encode(), Depots, distances);
                        assignmentValues.Add(key, vehicle.Value(center));
                    }
                    sum += assignmentValues[key];
                }
                else
                {
                    vehicle.ignoreTimeBounds = true;
                    vehicle.Distances = distances;
                    var center = vehicle.EnhanceWith2Opt(vehicle.Encode(), Depots, distances);
                    sum += vehicle.Value(center);
                }
            }
            if (sum < bestValue)
            {
                bestValue = sum;
                CacheRanks(afterNewBest);
            }
            return sum;
        }
    }
}
