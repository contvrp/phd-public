﻿using ParticleSwarmOptimization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRouting
{
    class MonteCarloWithTree2OPTSolutionProposals: MonteCarloWithSolutionProposals
    {
        private bool applyDistanceHeuristicInTree;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance">The instance to be solved</param>
        /// <param name="models">The number of propositions</param>
        /// <param name="generateDataFromModel">Generate from oracle?</param>
        public MonteCarloWithTree2OPTSolutionProposals(DVRPInstance instance, int models, int additionalModels, bool applyDistanceHeuristicInTree, bool penalizeInsufficentVehicles, decimal cutoffTime, bool generateDataFromModel = true, decimal bufferSize = 0.5M)
            : base(instance, models, additionalModels, generateDataFromModel, penalizeInsufficentVehicles, bufferSize, cutoffTime)
        {
            this.applyDistanceHeuristicInTree = applyDistanceHeuristicInTree;
        }

        protected override ClusterFunction AssignClients(DVRPInstance fakeInstance, object solution)
        {
            ClusterFunction clusterFunction;
            clusterFunction = new TreeClusterFunction(fakeInstance.Vehicles, fakeInstance.Clients, fakeInstance.Depots,
                fakeInstance.NextTime, fakeInstance.distances, 0.0, 1, 4, applyDistanceHeuristicInTree, penalizeInsufficentVehicles
                , this.cutoffTime, 0M);
            (clusterFunction as TreeClusterFunction).setClients = solution as Client[];
            clusterFunction.Value(new double[0]);
            return clusterFunction;
        }

        protected override object GetPreviousSolution()
        {
            return instance.StartVehicles
                .SelectMany(vhc => vhc.clientsToAssign)
                .Where(clnt => !(clnt is Depot))
                .Select(clnt => (Client)clnt.Clone())
                .ToArray();
        }

        protected override object ExtractSolution(ClusterFunction clusterFunction)
        {
            return clusterFunction.ClientsToAssign.Where(clnt => instance.ClientsToAssign.Any(clnt2 => clnt.Id == clnt2.Id)).ToArray();
        }


    }
}
