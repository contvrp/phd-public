﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParticleSwarmOptimization;
using System.IO;
using System.Diagnostics;

namespace DynamicVehicleRouting
{
    class ClusterFunction : IFunction
    {
        protected decimal currentProblemTime;
        public Client[] ClientsToAssign;
        protected Depot[] Depots;

        public Vehicle[] Vehicles;
        protected Vehicle[] StartingVehicles;
        protected double[,] distances;
        protected double weightPowerParameter;
        private int classesForVehicle;
        private bool fullMulticlustering;
        protected bool penalizeInsufficentVehicles;
        protected decimal cutoffTime;
        protected decimal bufferSize;

        public ClusterFunction(Vehicle[] vehicles, Client[] clients, Depot[] depots, decimal currentProblemTime, double[,] distances, double weightPowerParameter, int classesForVehicle, bool fullMulticlustering, bool penalizeInsufficentVehicles,
            decimal cutoffTime, decimal bufferSize)
        {
            this.weightPowerParameter = weightPowerParameter;
            StartingVehicles = vehicles;
            Vehicles = new Vehicle[vehicles.Length];
            for (int i = 0; i < StartingVehicles.Length; i++)
            {
                Vehicles[i] = (Vehicle)StartingVehicles[i].Clone();
            }
            ClientsToAssign = clients;
            Depots = depots;
            this.currentProblemTime = currentProblemTime;
            this.distances = distances;
            this.classesForVehicle = classesForVehicle;
            this.fullMulticlustering = fullMulticlustering;
            this.penalizeInsufficentVehicles = penalizeInsufficentVehicles;
            this.cutoffTime = cutoffTime;
            this.bufferSize = bufferSize;
        }

        protected struct ClientVehicleEdge : IComparable<ClientVehicleEdge>
        {
            public Client client;
            public Vehicle vehicle;
            private double distance;
            private double weightPowerParameter;
            public double Distance
            {
                get
                {
                    return distance;
                }
            }

            //smaller is more important
            /*
            private int Rank
            {
                get
                {
                    double normalizedNeed = -(double)this.client.Need / (double)this.vehicle.Capacity;
                    if (normalizedNeed > 0.5)
                        return 0;
                    if (normalizedNeed > 0.25)
                        return 1;
                    if (normalizedNeed > 0.125)
                        return 2;
                    return 3;
                }
            }
            */

            /// <summary>
            /// 
            /// </summary>
            /// <remarks>Deprecated</remarks>
            private double WeightedDistance
            {
                get
                {
                    return distance * Math.Pow(((double)vehicle.Capacity / (double)(-client.Need)), weightPowerParameter);
                }
            }

            public ClientVehicleEdge(Client client, Vehicle vehicle, double weightPowerParameter, int component)
            {
                this.weightPowerParameter = weightPowerParameter;
                this.client = client;
                this.vehicle = vehicle;
                distance = Utils.Instance.EuclideanDistance(
                        new double[] { client.X, client.Y },
                        new double[] { vehicle.X[component], vehicle.Y[component] }
                        );
            }

            public int CompareTo(ClientVehicleEdge other)
            {
                int comp = this.distance.CompareTo(other.distance);
                if (comp == 0)
                    comp = this.distance.CompareTo(other.distance);
                if (comp == 0)
                    comp = this.vehicle.Id.CompareTo(other.vehicle.Id);
                if (comp == 0)
                    comp = this.client.Id.CompareTo(other.client.Id);
                return comp;
            }

            public override bool Equals(object obj)
            {
                if (obj is ClientVehicleEdge)
                    return this.CompareTo((ClientVehicleEdge)obj) == 0;
                return false;
            }

            public override int GetHashCode()
            {
                return (int)distance;
            }
        }

        protected virtual void AssignClientsToVehicles(double[] x)
        {
            List<ClientVehicleEdge> edges = new List<ClientVehicleEdge>();
            int k = 0;
            for (int i = 0; i < classesForVehicle * Vehicles.Length; ++i)
            {
                if (Vehicles[i % Vehicles.Length].Available || Vehicles.Count(vhcl => vhcl.Available) == 0)
                {
                    Vehicles[i % Vehicles.Length].X[i / Vehicles.Length] = x[2 * k];
                    Vehicles[i % Vehicles.Length].Y[i / Vehicles.Length] = x[2 * k + 1];
                    ++k;
                    for (int j = 0; j < ClientsToAssign.Length; ++j)
                    {
                        if (fullMulticlustering)
                            edges.Add(new ClientVehicleEdge(ClientsToAssign[j], Vehicles[i % Vehicles.Length], this.weightPowerParameter, i / Vehicles.Length));
                        //for backward compatibility
                        else
                            edges.Add(new ClientVehicleEdge(ClientsToAssign[j], Vehicles[i % Vehicles.Length], this.weightPowerParameter, 0));
                    }
                }
            }
            var edgeEnum = new List<ClientVehicleEdge>(edges).OrderBy(edg => edg.Distance).GetEnumerator();
            List<int> clientsToOmit = new List<int>();
            while (edgeEnum.MoveNext())
            {
                Client clientToRemove = edgeEnum.Current.client;
                if (!clientsToOmit.Contains(edgeEnum.Current.client.Id))
                {
                    edgeEnum.Current.vehicle.clientsToAssign.Add(edgeEnum.Current.client);
                    edgeEnum.Current.client.FakeVehicleId = edgeEnum.Current.vehicle.Id;
                    edgeEnum.Current.client.VisitTime = currentProblemTime;
                    clientsToOmit.Add(edgeEnum.Current.client.Id);
                    //edgesSet.RemoveAll(edg => edg.client == clientToRemove);
                    if (clientsToOmit.Count == ClientsToAssign.Length)
                        break;
                }
                //i--;
            }
            edgeEnum.Dispose();
            edges.Clear();
        }

        public double Value(double[] x)
        {
//            DateTime start = DateTime.Now;
            CleanVehiclesRequests();
            PrepareData(ref x);
            AssignClientsToVehicles(x);
            double value = CalculateValue();// + x.Sum(xx => Math.Min(100,1/Math.Abs(xx/100)));
            if (penalizeInsufficentVehicles && currentProblemTime / Depots[0].EndAvailable < cutoffTime)
            {
                var currentTotalLoad = Vehicles.Sum(vhcl => -vhcl.Need);
                var possibleTotalLoad = Vehicles.Sum(vhcl => (vhcl.clientsToAssign.Any(clnt => !(clnt is Depot)) || vhcl.assignedClients.Any(clnt => !(clnt is Depot))) ? vhcl.Capacity : 0);
                var estimatedLoad = (decimal.ToDouble(bufferSize) * currentTotalLoad) / (double)Math.Min(1.0M, (1.0M - cutoffTime + (currentProblemTime / Depots[0].EndAvailable)));
                var activeVehicles = Vehicles.Count(vhcl => (vhcl.clientsToAssign.Any(clnt => !(clnt is Depot)) || vhcl.assignedClients.Any(clnt => !(clnt is Depot))));
                var lowerBoundOnPossibleAdditionalVehicles = Math.Ceiling((double)(Math.Abs(estimatedLoad) / Vehicles[0].Capacity)) - Math.Ceiling((double)(Math.Abs(possibleTotalLoad) / Vehicles[0].Capacity)) + 1;
                if (estimatedLoad > possibleTotalLoad)
                {
                    value += 1*(value / activeVehicles) * lowerBoundOnPossibleAdditionalVehicles;
                }
            }
//            File.AppendAllLines(string.Format("ExecutionTime-{0}-{1}.csv",Process.GetCurrentProcess().Id,System.Threading.Thread.CurrentThread.ManagedThreadId), new string[] { string.Format("{0}\t{1}", this.GetType(), (DateTime.Now - start).TotalMilliseconds)});
            return value;
        }

        protected void CleanVehiclesRequests()
        {
            for (int i = 0; i < Vehicles.Length; i++)
            {
                Vehicles[i].clientsToAssign = new List<Client>();
                //Vehicles[i].clientsToAssign.AddRange(StartingVehicles[i].clientsToAssign);
            }
        }

        protected virtual void PrepareData(ref double[] x)
        {
        }


        protected virtual double CalculateValue()
        {
            double sum = 0.0;
            foreach (Vehicle vehicle in Vehicles)
            {
                double dist = 0.0;
                decimal time = 0.0M;
                if (vehicle.assignedClients.Count > 0)
                {
                    for (int i = 1; i < vehicle.assignedClients.Count; ++i)
                    {
                        if (vehicle.assignedClients[i - 1] is Depot)
                            vehicle.Cargo = vehicle.Capacity;
                        dist += distances[vehicle.assignedClients[i].Id,
                            vehicle.assignedClients[i - 1].Id
                            ];
                        vehicle.Cargo += vehicle.assignedClients[i].Need;
                        time = vehicle.assignedClients[i].VisitTime + vehicle.assignedClients[i].TimeToUnload;
                    }
                    dist +=
                        Utils.Instance.EuclideanDistance(
                            new double[] { vehicle.X[0], vehicle.Y[0] },
                            new double[] { vehicle.assignedClients[vehicle.assignedClients.Count - 1].X, vehicle.assignedClients[vehicle.assignedClients.Count - 1].Y }
                            );
                    try
                    {
                        time += (decimal)Utils.Instance.EuclideanDistance(
                          new double[] { vehicle.X[0], vehicle.Y[0] },
                          new double[] { vehicle.assignedClients[vehicle.assignedClients.Count - 1].X, vehicle.assignedClients[vehicle.assignedClients.Count - 1].Y }
                          ) / 2;
                    }
                    catch (System.OverflowException) { time = decimal.MaxValue; }

                }
                else
                {
                    if (vehicle.clientsToAssign.Count > 0)
                    {
                        dist += 2 * Depots.Min(dpt =>
                            Utils.Instance.EuclideanDistance(
                                new double[] { vehicle.X[0], vehicle.Y[0] },
                                new double[] { dpt.X, dpt.Y }
                                ));
                        time = vehicle.clientsToAssign.Last().VisitTime + vehicle.clientsToAssign.Last().TimeToUnload;
                        try
                        {
                            time += (decimal)Depots.Min(dpt =>
                                Utils.Instance.EuclideanDistance(
                                    new double[] { vehicle.X[0], vehicle.Y[0] },
                                    new double[] { dpt.X, dpt.Y }
                                    )) / 2;
                        }
                        catch (System.OverflowException)
                        {
                            time = decimal.MaxValue;
                        }
                    }
                    vehicle.Cargo = vehicle.Capacity;
                }
                foreach (Client client in vehicle.clientsToAssign)
                {
                    if (vehicle.Cargo + client.Need < 0)
                    {
                        try
                        {
                            dist += 4 * Depots.Min(dpt =>
                                Utils.Instance.EuclideanDistance(
                                    new double[] { vehicle.X[0], vehicle.Y[0] },
                                    new double[] { dpt.X, dpt.Y }
                                    ));
                        }
                        catch (OverflowException)
                        {
                            dist = double.MaxValue;
                        }
                        try
                        {
                            time += (decimal)Depots.Min(dpt =>
                                Utils.Instance.EuclideanDistance(
                                    new double[] { vehicle.X[0], vehicle.Y[0] },
                                    new double[] { dpt.X, dpt.Y }
                                    ));
                        }
                        catch (OverflowException)
                        {
                            time = decimal.MaxValue;
                        }
                    }
                    try
                    {
                        dist += Utils.Instance.EuclideanDistance(
                            new double[] { vehicle.X[0], vehicle.Y[0] },
                            new double[] { client.X, client.Y }
                            );
                    }
                    catch (OverflowException)
                    {
                        dist = double.MaxValue;
                    }
                    try
                    {
                        time += (decimal)Utils.Instance.EuclideanDistance(
                            new double[] { vehicle.X[0], vehicle.Y[0] },
                            new double[] { client.X, client.Y }
                            ) / 2;
                    }
                    catch (System.OverflowException)
                    {
                        time = decimal.MaxValue;
                    }
                    vehicle.Cargo += client.Need;
                    try { time += client.TimeToUnload; }
                    catch (System.OverflowException) { time = decimal.MaxValue; }
                }
                if (vehicle.clientsToAssign.Count + vehicle.assignedClients.Count > 0)
                    dist += 2 * Depots.Min(dpt =>
                    Utils.Instance.EuclideanDistance(
                        new double[] { vehicle.X[0], vehicle.Y[0] },
                        new double[] { dpt.X, dpt.Y }
                        ));
                vehicle.Cargo = 0;
                sum += dist;
                /*
                if (time > Depots.Max(dpt => dpt.EndAvailable))
                    sum += ((double)time - Depots.Max(dpt => dpt.EndAvailable)) * ((double)time - Depots.Max(dpt => dpt.EndAvailable));
                */
            }
            return sum;
        }

        public virtual int Dimension
        {
            get { return classesForVehicle * 2 * (StartingVehicles != null ? StartingVehicles.Where(vhcl => vhcl.Available || StartingVehicles.Count(vhcl2 => vhcl2.Available) == 0).Count() : 0); }
        }

        public int ValueCount
        {
            get { throw new NotImplementedException(); }
        }

        public static double[] GetFlatCentersVector(IEnumerable<Vehicle> vehicles, int classesForVehicle, bool randomize, double radius)
        {
            var noAvailableVehicles = !vehicles.Any(vhcl => vhcl.Available);
            var center = new double[0];
            for (int i = 0; i < classesForVehicle; ++i)
            {
                center = center
                    .Concat(vehicles.OrderBy(vhcl => vhcl.Id)
                    .Where(vhcl => vhcl.Available || noAvailableVehicles)
                    .SelectMany(vhcl => new double[] {
                        !randomize ? vhcl.X[i] : Utils.Instance.random.NextDouble() * radius - 0.5 * radius,
                        !randomize ? vhcl.Y[i] : Utils.Instance.random.NextDouble() * radius - 0.5 * radius}
                        )).ToArray();
            }
            return center;
        }

        public static double[] GetFlatCentersVectorFromDiscrete(IEnumerable<Vehicle> vehicles, int classesForVehicle)
        {
            var noAvailableVehicles = !vehicles.Any(vhcl => vhcl.Available);
            var center = new double[0];
            for (int i = 0; i < classesForVehicle; ++i)
            {
                center = center
                    .Concat(vehicles.OrderBy(vhcl => vhcl.Id)
                    .Where(vhcl => vhcl.Available || noAvailableVehicles)
                    .SelectMany(vhcl => new double[] {
                        (vhcl.clientsToAssign.Any() ? vhcl.clientsToAssign.Average(clnt => clnt.X) : vhcl.Depot.X) + 0.02 * Utils.Instance.random.NextDouble() - 0.01,
                        (vhcl.clientsToAssign.Any() ? vhcl.clientsToAssign.Average(clnt => clnt.Y) : vhcl.Depot.Y) + 0.02 * Utils.Instance.random.NextDouble() - 0.01,
                    }))
                    .ToArray();
            }
            return center;
        }
    }

}
