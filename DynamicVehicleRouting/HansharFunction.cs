﻿using ParticleSwarmOptimization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRouting
{
    class HansharFunction : DapsoClusterFunction, DiscreteParticleSwarmOptimization.IFunction
    {
        public HansharFunction(Vehicle[] vehicles, Client[] clients, Depot[] depots, decimal currentProblemTime, double[,] distances, double weightPowerParameter, int classesForVehicle, bool fullMulticlustering,
            bool penalizeInsufficentVehicles, decimal cutoffTime, decimal bufferSize)
            : base(vehicles, clients, depots, currentProblemTime, distances, weightPowerParameter, classesForVehicle, fullMulticlustering, penalizeInsufficentVehicles, cutoffTime, bufferSize)
        {
        }

        public double Value(int[] x)
        {
            CleanVehiclesRequests();
            AssignClients(x);
            double value = CalculateValue();
            return value;
        }

        private void SetRoughContinuousEncoding()
        {
            decimal rank = 0.0M;
            foreach (var vehicle in Vehicles.Where(vhcl => vhcl.clientsToAssign.Any()))
            {
                foreach (var client in vehicle.clientsToAssign)
                {
                    client.Rank = rank / ClientsToAssign.Length;
                    ClientsToAssign.FirstOrDefault(clnt => clnt.Id == client.Id).Rank = rank / ClientsToAssign.Length;
                    rank += 1.0M;
                }
                for (int i = 0; i < vehicle.X.Length; ++i)
                {
                    vehicle.X[i] = vehicle.clientsToAssign.Average(clnt => clnt.X) + Utils.Instance.random.NextDouble() / 10 - 0.05;
                    vehicle.Y[i] = vehicle.clientsToAssign.Average(clnt => clnt.Y) + Utils.Instance.random.NextDouble() / 10 - 0.05;
                }
            }
        }

        public double Value(int[] x, out List<int[]> chosenClients)
        {
            var value = Value(x);
            if (Vehicles.Any(vhcl => vhcl.clientsToAssign.Any()))
            {
                chosenClients = Vehicles
                    .Where(vhcl => vhcl.clientsToAssign.Any())
                    .Select(vhcl => vhcl
                    .clientsToAssign
                    .Where(clnt => !(clnt is Depot))
                    .Select(clnt => clnt.Id)
                    .ToArray())
                    .ToList();
            }
            else
            {
                chosenClients = new List<int[]>() { new int[0] };
            }
            return value;
        }

        public List<Vehicle> ReassignClients(int[] x, int[] y)
        {
#warning this is improper - I am repacking vehicles, I should only assign and recompute by addition
            x = x.Where(xx => !y.Contains(xx)).ToArray();
            Value(x);
            Depot depot = Depots[0];
            foreach (var clientId in y)
            {
#warning definitely some copied code with the case of initialization
                Vehicle chosen = null;
                double minCost = double.PositiveInfinity;
                Client insertBefore = null;
                Client client = ClientsToAssign.FirstOrDefault(clnt => clnt.Id == clientId);
                foreach (Vehicle vehicle in Vehicles)
                {
                    Client start = vehicle.assignedClients.Any() ? vehicle.assignedClients.Last() : depot;
                    if (-vehicle.Need - client.Need <= vehicle.Capacity)
                    {
                        double insertCost;
                        foreach (Client end in vehicle.clientsToAssign)
                        {
#warning TryInsert should be moved to vehicle and vehicle's earliest leave time should be properly set
                            insertCost = TryInsert(ref chosen, ref minCost, ref insertBefore, client, vehicle, start, end);
                            start = end;
                        }
                        insertCost = TryInsert(ref chosen, ref minCost, ref insertBefore, client, vehicle, start, depot);
                    }
                }
                int insertIndex = -1;
                if (chosen != null)
                {
                    client.FakeVehicleId = chosen.Id;
                    insertIndex = chosen.clientsToAssign.IndexOf(insertBefore);
                }
                else
                {
                    chosen = Vehicles.OrderBy(vhcl => vhcl.FinishTime).FirstOrDefault();
                }
                if (insertIndex > -1)
                {
                    client.Rank = insertBefore.Rank - 0.5M / ClientsToAssign.Length;
                    chosen.clientsToAssign.Insert(insertIndex, client);
                    for (int i = insertIndex + 1; i < chosen.clientsToAssign.Count; ++i)
                    {
                        chosen.clientsToAssign[i].VisitTime += client.TimeToUnload + (decimal)minCost;
                    }
                }
                else
                {
                    client.Rank = 1.1M;
                    if (chosen.clientsToAssign.Any())
                        client.Rank = chosen.clientsToAssign.Max(clnt => clnt.Rank) + 0.5M / ClientsToAssign.Length;
                    chosen.clientsToAssign.Add(client);
                    if (chosen.assignedClients.Any())
                    {
                        client.VisitTime = Math.Max(chosen.assignedClients.Last().VisitTime + chosen.assignedClients.Last().TimeToUnload, currentProblemTime)
                            + (decimal)Utils.Instance.EuclideanDistance(new double[] { chosen.assignedClients.Last().X, chosen.assignedClients.Last().Y }, new double[]{
                            client.X,client.Y});
                    }
                    else
                    {
                        client.VisitTime = currentProblemTime
                            + (decimal)Utils.Instance.EuclideanDistance(new double[] { Depots[0].X, Depots[0].Y }, new double[]{
                            client.X,client.Y});
                    }
                }
            }
            return GetNonEmptyVehicles();
        }

        public static int[] GetHansharEncoding(IEnumerable<Vehicle> vehicles)
        {
            return vehicles
                        .OrderBy(vhcl => vhcl.Need)
                            .SelectMany(vhcl => (vhcl.assignedClients.Any() ? new int[] { -vhcl.Id } : new int[] { })
                                .Concat(vhcl.clientsToAssign
                                .Where(clnt => !(clnt is Depot))
                                .OrderBy(clnt => clnt.Rank)
                                .Select(clnt => clnt.Id))).ToArray(); ;
        }

        private double TryInsert(ref Vehicle chosen, ref double minCost, ref Client insertBefore, Client client, Vehicle vehicle, Client start, Client end)
        {
            double insertCost;
            insertCost =
                distances[start.Id, client.Id]
                + distances[end.Id, client.Id]
                - distances[start.Id, end.Id];
            if (insertCost < minCost && Math.Max(vehicle.FinishTime,currentProblemTime) + (decimal)insertCost + client.TimeToUnload <= Depots[0].EndAvailable)
            {
                chosen = vehicle;
                minCost = insertCost;
                insertBefore = end;
            }
            return insertCost;
        }

        private void AssignClients(int[] x)
        {
            Vehicle current = null;
            Client start = null;
            decimal vehicleTime = 0.0M;
            for (int i = 0; i < x.Length; i++)
            {
                if (x[i] < 0)
                {
                    current = Vehicles.FirstOrDefault(vhcl => vhcl.Id == -x[i]);
                    if (current.assignedClients.Any())
                    {
                        start = current.assignedClients.Last();
                        vehicleTime = Math.Max(currentProblemTime, start.VisitTime + start.TimeToUnload);
                    }
                    else
                    {
                        start = Depots[0];
                        vehicleTime = currentProblemTime;
                    }
                }
                if (current != null && x[i] > 0)
                {
                    if (-current.Need - ClientsToAssign.FirstOrDefault(clnt => clnt.Id == x[i]).Need > current.Capacity)
                    {
                        current = null;
                    }
                    Client currentClient = ClientsToAssign.FirstOrDefault(clnt => clnt.Id == x[i]);
                    if (vehicleTime + (decimal)Utils.Instance.EuclideanDistance(new double[] { start.X, start.Y }, new double[] { currentClient.X, currentClient.Y })
                        + currentClient.TimeToUnload + (decimal)Utils.Instance.EuclideanDistance(new double[] { Depots[0].X, Depots[0].Y }, new double[] { currentClient.X, currentClient.Y })
                        > Depots[0].EndAvailable)
                        current = null;
                }
                if (current == null)
                {
                    current = Vehicles.OrderBy(vhcl => vhcl.assignedClients.Count + vhcl.clientsToAssign.Count).FirstOrDefault();
                    if (current.clientsToAssign.Any())
                    {
                        start = current.clientsToAssign.Last();
                        vehicleTime = Math.Max(currentProblemTime, start.VisitTime + start.TimeToUnload);
                    }
                    else if (current.assignedClients.Any())
                    {
                        start = current.assignedClients.Last();
                        vehicleTime = Math.Max(currentProblemTime, start.VisitTime + start.TimeToUnload);
                    }
                    else
                    {
                        start = Depots[0];
                        vehicleTime = currentProblemTime;
                    }
                }
                if (x[i] > 0)
                {
                    Client currentClient = ClientsToAssign.FirstOrDefault(clnt => clnt.Id == x[i]);
                    vehicleTime += (decimal)Utils.Instance.EuclideanDistance(new double[] { start.X, start.Y }, new double[] { currentClient.X, currentClient.Y });
                    currentClient.VisitTime = vehicleTime;
                    vehicleTime += currentClient.TimeToUnload;
                    current.clientsToAssign.Add(currentClient);
                    currentClient.FakeVehicleId = current.Id;
                    currentClient.Rank = clientRanks[x[i]] = (decimal)i / x.Length;
                    start = currentClient;
                }
            }
            SetRoughContinuousEncoding();
        }

        public List<Vehicle> GetGreedyAssignmentForOrderedRequests(int[] requestsIds)
        {
            CleanVehiclesRequests();
            AssignClients(requestsIds);
            return GetNonEmptyVehicles();
        }

        private List<Vehicle> GetNonEmptyVehicles()
        {
            return Vehicles.Where(vhcl => vhcl.assignedClients.Any() || vhcl.clientsToAssign.Any())
                .Select(vhcl => (Vehicle)vhcl.Clone())
                .ToList();
        }

        public int DiscreteDimension
        {
            get { return Vehicles.Length + ClientsToAssign.Length; }
        }
    }
}
