﻿using System;
namespace ParticleSwarmOptimization
{
    public interface IOptimizer
    {
        double[] Best { get; }
        double BestValue { get; }
        void Step();
    }
}
