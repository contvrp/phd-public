﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParticleSwarmOptimization
{
    public class RandomClustererVNS: Optimizer
    {
        private double[] radiuses;
        private int maxFailCount;
        private int[] moves;
        private double attractinFactor;

        public RandomClustererVNS(int populationSize, IFunction objectiveFunction, double radius, Dictionary<double[], int> seedsDictionary,
            int maxFailCount = 10, double attractinFactor = 1.4)
            : base(populationSize, objectiveFunction, radius, seedsDictionary)
        {
            this.maxFailCount = maxFailCount;
            this.attractinFactor = attractinFactor;
            this.moves = new int[_populationSize];
            radiuses = new double[_populationSize];
            for (int dimIdx = 0; dimIdx < radiuses.Length; dimIdx++)
            {
                radiuses[dimIdx] = attractinFactor;
                this.moves[dimIdx] = 1;
            }
        }

        protected override double[] MoveSpecimen(int specimenIndex)
        {
            if (failCounter[specimenIndex] > maxFailCount)
            {
                ++moves[specimenIndex];
                failCounter[specimenIndex] = 0;
            }
            double[] move = new double[_objectiveFunction.Dimension];
            for (int dimIdx = 0; dimIdx < _objectiveFunction.Dimension; dimIdx++)
            {
                move[dimIdx] = _population[specimenIndex][dimIdx];
            }
            for (int i = 0; i < moves[specimenIndex]; i++)
            {

                HashSet<int> centersSet = new HashSet<int>();
                while (centersSet.Count < 2)
                {
                    centersSet.Add(Utils.Instance.random.Next(_objectiveFunction.Dimension / 2));
                }
                int[] centers = centersSet.ToArray();
                double vector = Utils.Instance.random.NextDouble() * radiuses[specimenIndex];
                move[centers[0] * 2] += vector * (move[centers[1] * 2] - move[centers[0] * 2]);
                move[centers[0] * 2 + 1] += vector * (move[centers[1] * 2 + 1] - move[centers[0] * 2 + 1]);
            }
            return move;
        }
    }
}
