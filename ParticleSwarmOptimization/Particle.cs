﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParticleSwarmOptimization
{
    public class Inventor : Particle
    {
        public Inventor(IFunction function)
            :base(function, 0.0, 2.2, 0.6, 0.63, 0.0, 0)
        {
        }
    }

    public class Particle
    {
        public double[] x;
        public double[] v;
        public IList<Particle> neighbours = new List<Particle>();
        public double neighbourBestAttractionFactor;
        public double neighbourRandomRepulsionFactor;
        public double localBestAttractionFactor;
        public double inertiaFactor;
        public double randomFactor;
        public byte constantFactors;
        public double bestValue = Double.PositiveInfinity;
        public double[] best;
        public IFunction function;
        private Utils utils = new Utils();

        public Particle (
            IFunction function,
            double neighbourBestAttractionFactor = 0.6,
            double localBestAttractionFactor = 2.2,
            double neighbourRandomRepulsionFactor = 0.0,
            double inertiaFactor = 0.63,
            double randomFactor = 0.0,
            byte constantFactors = 0)
        {
            this.function = function;
            this.neighbourBestAttractionFactor = neighbourBestAttractionFactor;
            this.neighbourRandomRepulsionFactor = neighbourRandomRepulsionFactor;
            this.localBestAttractionFactor = localBestAttractionFactor;
            this.inertiaFactor = inertiaFactor;
            this.randomFactor = randomFactor;
            this.constantFactors = constantFactors;
        }

        public virtual void InitializePosition(double[] center, double radius)
        {
            x = utils.GetRandomInZeroBasedNSphere(center.Length, radius);
            for (int i = 0; i < center.Length; i++)
            {
                x[i] += center[i];
            }
            v = new double[center.Length];
            best = new double[center.Length];
            UpdateBest();
        }

        public virtual void InitializeVelocity()
        {
            if (neighbours.Count > 0)
            {
                int index = Utils.Instance.random.Next(neighbours.Count);
                for (int i = 0; i < x.Length; i++)
                {
                    v[i] = (neighbours[index].x[i] - x[i]) / 2.0;
                }
            }
            else
            {
                for (int i = 0; i < x.Length; i++)
                {
                    v[i] = Utils.Instance.random.NextDouble();
                }
            }
            neighbours.Add(this);
        }

        public virtual void UpdateVelocity()
        {
            //Random r = new Random();
            double[] neighbourBest = neighbours.Count > 0  ? this.neighbours.First(particle => particle.bestValue == this.neighbours.Min(particle2 => particle2.bestValue)).best : new double[x.Length];
            double[] neighbourRandom = neighbours.Count > 0 ? this.neighbours[Utils.Instance.random.Next(neighbours.Count)].x : new double[x.Length];
            for (int i = 0; i < v.Length; i++)
            {
                double v_i = inertiaFactor * v[i];
                double v_i_global = neighbours.Count > 0 ? neighbourBestAttractionFactor * Math.Max(Utils.Instance.random.NextDouble(), constantFactors) * (neighbourBest[i] - x[i]) : 0;
                double v_i_repulse = neighbours.Count > 0 ? neighbourRandomRepulsionFactor * Math.Max(Utils.Instance.random.NextDouble(), constantFactors) * (x[i] - neighbourRandom[i]) : 0;
                double v_i_local = localBestAttractionFactor * Math.Max(Utils.Instance.random.NextDouble(), constantFactors) * (best[i] - x[i]);
                v[i] =
                    v_i +
                    v_i_global + 
                    v_i_repulse +
                    v_i_local +
                    randomFactor * Utils.Instance.random.NextDouble();
            }
        }

        public virtual void Move()
        {
            for (int i = 0; i < x.Length; i++)
            {
                x[i] += v[i];
            }
            //UpdateBest();
        }

        public virtual void UpdateBest()
        {
            double valueToBe;
            if ((valueToBe = function.Value(this.x)) < this.bestValue)
            {
                for (int i = 0; i < x.Length; ++i)
                    best[i] = x[i];
                bestValue = valueToBe;
            }
        }
    }
}
