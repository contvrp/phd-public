﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParticleSwarmOptimization
{
    public class RandomWalkVNS : Optimizer
    {
        private double[] radiuses;
        private int maxFailCount;
        private double radiusFactor;

        public RandomWalkVNS(int populationSize, IFunction objectiveFunction, double radius, Dictionary<double[], int> seedsDictionary,
            int maxFailCount = 10, double radiusFactor = 0.5)
            : base(populationSize, objectiveFunction, radius, seedsDictionary)
        {
            this.maxFailCount = maxFailCount;
            this.radiusFactor = radiusFactor;
            radiuses = new double[_populationSize];
            for (int dimIdx = 0; dimIdx < radiuses.Length; dimIdx++)
            {
                radiuses[dimIdx] = radius;
            }
        }

        protected override double[] MoveSpecimen(int specimenIndex)
        {
            if (failCounter[specimenIndex] > maxFailCount)
            {
                radiuses[specimenIndex] *= radiusFactor;
                failCounter[specimenIndex] = 0;
            }
            double[] move = Utils.Instance.GetRandomInZeroBasedNSphere(
                        n: _objectiveFunction.Dimension,
                        radius: radiuses[specimenIndex]);
            for (int dimIdx = 0; dimIdx < _objectiveFunction.Dimension; dimIdx++)
            {
                move[dimIdx] += _population[specimenIndex][dimIdx];
            }
            return move;
        }
    }
}
