﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiscreteParticleSwarmOptimization
{
    public interface IFunction
    {
        double Value(int[] x);
        int DiscreteDimension { get; }
        int ValueCount { get; }
    }
}
