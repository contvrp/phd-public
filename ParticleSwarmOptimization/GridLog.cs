﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ParticleSwarmOptimization
{
    public class GridLog: Optimizer
    {
        int _gridSize;
        int _x;
        int _y;
#if DEBUG
        static int FIRST_SECTION = 0;
        static int SECOND_SECTION = 1;
#else
        static int FIRST_SECTION = Utils.Instance.random.Next();
        static int SECOND_SECTION = Utils.Instance.random.Next();
#endif
        readonly double _initXLoc, _initYloc;
        double _xLoc, _yLoc;
        double _stepX, _stepY;
        double[] position;

        public GridLog(decimal time, int gridSize, int populationSize, IFunction objectiveFunction, double radius, Dictionary<double[], int> seedsDictionary,
            double minX, double maxX, double minY, double maxY)
            : base(populationSize, new FunctionWrapper(objectiveFunction, time), radius, seedsDictionary)
        {
            _gridSize = gridSize;
            _x = 0;
            _y = 0;
            _stepX = 1.5 * ((FIRST_SECTION % 2 == 0) ? ((maxX - minX) / gridSize) : ((maxY - minY) / gridSize));
            _stepY = 1.5 * ((SECOND_SECTION % 2 == 0) ? ((maxX - minX) / gridSize) : ((maxY - minY) / gridSize));
            _initXLoc = _xLoc = (FIRST_SECTION % 2 == 0) ? (minX - _stepX - (maxX - minX) / 4) : (minY - _stepY - (maxY - minY) / 4);
            _initYloc = _yLoc = (SECOND_SECTION % 2 == 0) ? (minX - _stepX - (maxX - minX) / 4) : (minY - _stepY - (maxY - minY) / 4);
            position = new double[objectiveFunction.Dimension];
            for (int dim = 0; dim < position.Length; dim++)
            {
                position[dim] = this.Best[dim];
            }
        }

        protected override double[] MoveSpecimen(int specimenIndex)
        {
                while (_x < _gridSize)
                {
                    while (_y < _gridSize)
                    {
                        ++_y;
                        _yLoc += _stepY;
                        goto test;
                    }
                    ++_x;
                    _y = 0;
                    _yLoc = _initYloc;
                    _xLoc += _stepX;
                    goto test;
                }
                _x = 0;
                _xLoc = _initXLoc;
            test:
                position[FIRST_SECTION % position.Length] = _xLoc;
                position[SECOND_SECTION % position.Length] = _yLoc;
            return position;
        }

        class FunctionWrapper : IFunction
        {
            IFunction _innerFunction;
            decimal _time;

            public FunctionWrapper(IFunction innerFunction, decimal time)
            {
                _innerFunction = innerFunction;
                _time = time;
            }

            public double Value(double[] x)
            {
                double value = _innerFunction.Value(x);
#if DEBUG
                File.AppendAllText("values.csv", string.Format("{0}\t{1}\t{2}\t{3}\n", x[FIRST_SECTION % x.Length], x[SECOND_SECTION % x.Length], value, _time));
#endif
                return value;
            }

            public int Dimension
            {
                get { return _innerFunction.Dimension; }
            }

            public int ValueCount
            {
                get { return _innerFunction.ValueCount; }
            }
        }

    }
}
