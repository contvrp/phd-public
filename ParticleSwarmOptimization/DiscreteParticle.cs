﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiscreteParticleSwarmOptimization
{
    public class Inventor : Particle
    {
        public Inventor(IFunction function)
            :base(function, 0.0, 1.4, 1.4, 0.8, 0.0, 0)
        {
        }
    }

    public class Particle
    {
        public int[] x;
        public int[] v;
        public IList<Particle> neighbours = new List<Particle>();
        public double neighbourBestAttractionFactor;
        public double neighbourRandomRepulsionFactor;
        public double localBestAttractionFactor;
        public double inertiaFactor;
        public double randomFactor;
        public byte constantFactors;
        public double bestValue = Double.PositiveInfinity;
        public int[] best;
        public IFunction function;
        private ParticleSwarmOptimization.Utils utils = new ParticleSwarmOptimization.Utils();

        public Particle (
            IFunction function,
            double neighbourBestAttractionFactor = 0.75,
            double localBestAttractionFactor = 0.5,
            double neighbourRandomRepulsionFactor = 0.0,
            double inertiaFactor = 1.0,
            double randomFactor = 0.0,
            byte constantFactors = 0)
        {
            this.function = function;
            this.neighbourBestAttractionFactor = neighbourBestAttractionFactor;
            this.neighbourRandomRepulsionFactor = neighbourRandomRepulsionFactor;
            this.localBestAttractionFactor = localBestAttractionFactor;
            this.inertiaFactor = inertiaFactor;
            this.randomFactor = randomFactor;
            this.constantFactors = constantFactors;
        }

        public virtual void InitializePosition(int[] center, int radius)
        {
            x = new int[center.Length];
            neighbours.Clear();
            for (int i = 0; i < center.Length; i++)
            {
                x[i] += center[i] + 2 * ParticleSwarmOptimization.Utils.Instance.random.Next(radius) - (radius - 1);
            }
            v = new int[center.Length];
            best = new int[center.Length];
            UpdateBest();
        }

        public virtual void InitializeVelocity()
        {
            if (neighbours.Count > 0)
            {
                int index = ParticleSwarmOptimization.Utils.Instance.random.Next(neighbours.Count);
                for (int i = 0; i < x.Length; i++)
                {
                    v[i] = (neighbours[index].x[i] - x[i]) / 2;
                }
            }
            else
            {
                for (int i = 0; i < x.Length; i++)
                {
                    v[i] = ParticleSwarmOptimization.Utils.Instance.random.Next();
                }
            }
            neighbours.Add(this);
        }

        public virtual void UpdateVelocity()
        {
            //Random r = new Random();
            int[] neighbourBest = neighbours.Count > 0  ? this.neighbours.First(particle => particle.bestValue == this.neighbours.Min(particle2 => particle2.bestValue)).best : new int[x.Length];
            int[] neighbourRandom = neighbours.Count > 0 ? this.neighbours[ParticleSwarmOptimization.Utils.Instance.random.Next(neighbours.Count)].x : new int[x.Length];
            for (int i = 0; i < v.Length; i++)
            {
                double v_i = inertiaFactor * v[i];
                double v_i_global = neighbours.Count > 0 ? neighbourBestAttractionFactor * Math.Max(ParticleSwarmOptimization.Utils.Instance.random.NextDouble(), constantFactors) * (neighbourBest[i] - x[i]) : 0;
                double v_i_repulse = neighbours.Count > 0 ? neighbourRandomRepulsionFactor * Math.Max(ParticleSwarmOptimization.Utils.Instance.random.NextDouble(), constantFactors) * (x[i] - neighbourRandom[i]) : 0;
                double v_i_local = localBestAttractionFactor * Math.Max(ParticleSwarmOptimization.Utils.Instance.random.NextDouble(), constantFactors) * (best[i] - x[i]);
                v[i] =
                    (int)Math.Round(
                    v_i +
                    v_i_global + 
                    v_i_repulse +
                    v_i_local +
                    randomFactor * ParticleSwarmOptimization.Utils.Instance.random.NextDouble()
                    );
            }
        }

        public virtual void Move()
        {
            for (int i = 0; i < x.Length; i++)
            {
                x[i] += v[i];
            }
            UpdateBest();
        }

        public virtual void UpdateBest()
        {
            double valueToBe;
            if ((valueToBe = function.Value(this.x)) < this.bestValue)
            {
                for (int i = 0; i < x.Length; ++i)
                    best[i] = x[i];
                bestValue = valueToBe;
            }
        }
    }
}
